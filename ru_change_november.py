 # coding: utf-8 
import os
import operator
from num2words import num2words #i'm not sure that it works with ru-version
import gc
import sys
from transliterate import translit, get_available_language_codes
import itertools
INPUT_PATH = sys.path[0]
DATA_INPUT_PATH = 'kaggle/russian/ru_with_types/'
DATA_INPUT_PATHH = 'kaggle/russian/ruu_with_types/'
SUBM_PATH = INPUT_PATH

SUB = str.maketrans("₀₁₂₃₄₅₆₇₈₉", "0123456789")
SUP = str.maketrans("⁰¹²³⁴⁵⁶⁷⁸⁹", "0123456789")
OTH = str.maketrans("፬", "4")

print('Train start...')

file = "ru_train.csv"
train = open(os.path.join(INPUT_PATH, "ru_train.csv"), encoding='UTF8')
line = train.readline()
res = dict()
latin=dict()
newspaper_dict=dict()
decimal_dict=dict()
cardinal5_dict = dict()
cardinal3_dict = dict()
roman5_dict=dict()
year_len2_dict=dict()
year_len3_dict=dict()
year_len4_dict=dict()
minus_dict = dict()
measuring_symbols_dict = dict()
fem=0
do=0
total = 0
not_same = 0
latin_number =0
gen=0
acc=0
roman=0
nom=0
digit=0
yes=0
no=0
notin=0
change4=0
gengengen=0
ipip=0
gotcha=0
seasonss=0
oyeeee=0
nana=0
powers_dict={}
powers_dict['³']='в кубе'
powers_dict['²']='в квадрате'

latin_letters = ["A", "B", "C", "D", "E", "F", "H", "I", "K", "O", "P", "Q", "R", "T", "U", "V", "X", "Y","a", "b", "c", "d", "e", "f","h", "i", "k", "l", "o", "p", "q", "r","t", "u", "v",  "x", "y"]
latin_dict={}
latin_dict['A']='a'
latin_dict['B']='b'
latin_dict['C']='c'
latin_dict['D']='d'
latin_dict['E']='e'
latin_dict['F']='f'
#latin_dict['G']='g'
latin_dict['H']='h'
latin_dict['I']='i'
#latin_dict['J']='j'
latin_dict['K']='k'
latin_dict['L']='l'
#latin_dict['M']='m'
#latin_dict['N']='n'
latin_dict['O']='o'
latin_dict['P']='p'
latin_dict['Q']='q'
latin_dict['R']='r'
#latin_dict['S']='s'
latin_dict['T']='t'
latin_dict['U']='u'
latin_dict['V']='v'
#latin_dict['W']='w'
latin_dict['X']='x'
latin_dict['Y']='y'
#latin_dict['Z']='z'
latin_dict['a']='a'
latin_dict['b']='b'
latin_dict['c']='c'
latin_dict['d']='d'
latin_dict['e']='e'
latin_dict['f']='f'
#latin_dict['g']='g'
latin_dict['h']='h'
latin_dict['i']='i'
#latin_dict['j']='j'
latin_dict['k']='k'
latin_dict['l']='l'
#latin_dict['m']='m'
#latin_dict['n']='n'
latin_dict['o']='o'
latin_dict['p']='p'
latin_dict['q']='q'
latin_dict['r']='r'
#latin_dict['s']='s'
latin_dict['t']='t'
latin_dict['u']='u'
latin_dict['v']='v'
#latin_dict['w']='w'
latin_dict['x']='x'
latin_dict['y']='y'
#latin_dict['z']='z'

russian_dict={}
russian_dict['А']='а'
russian_dict['Б']='б'
russian_dict['В']='в'
russian_dict['Г']='г'
russian_dict['Д']='д'
russian_dict['Е']='е'
russian_dict['Ё']='ё'
russian_dict['Ж']='ж'
russian_dict['З']='з'
russian_dict['И']='и'
russian_dict['Й']='й'
russian_dict['К']='к'
russian_dict['Л']='л'
russian_dict['М']='м'
russian_dict['Н']='н'
russian_dict['О']='о'
russian_dict['Р']='р'
russian_dict['П']='п'
russian_dict['С']='с'
russian_dict['Т']='т'
russian_dict['У']='у'
russian_dict['Ф']='ф'
russian_dict['Х']='х'
russian_dict['Ц']='ц'
russian_dict['Ч']='ч'
russian_dict['Ш']='ш'
russian_dict['Щ']='щ'
russian_dict['Ъ']='ъ'
russian_dict['Ы']='ы'
russian_dict['Ь']='ь'
russian_dict['Э']='э'
russian_dict['Ю']='ю'
russian_dict['Я']='я'
russian_dict['а']='a'
russian_dict['б']='a'
russian_dict['в']='a'
russian_dict['г']='a'
russian_dict['д']='a'
russian_dict['е']='a'
russian_dict['ё']='a'
russian_dict['ж']='a'
russian_dict['з']='a'
russian_dict['и']='a'
russian_dict['й']='a'
russian_dict['к']='a'
russian_dict['л']='a'
russian_dict['м']='a'
russian_dict['н']='a'
russian_dict['о']='a'
russian_dict['р']='a'
russian_dict['п']='a'
russian_dict['с']='a'
russian_dict['т']='a'
russian_dict['у']='a'
russian_dict['ф']='a'
russian_dict['х']='a'
russian_dict['ц']='a'
russian_dict['ч']='a'
russian_dict['ш']='a'
russian_dict['щ']='a'
russian_dict['ъ']='a'
russian_dict['ы']='a'
russian_dict['ь']='a'
russian_dict['э']='a'
russian_dict['ю']='a'
russian_dict['я']='a'

russian_capital_letters=['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ь','Э','Ю','Я']
russian_dict['А']='а'
russian_dict['Б']='б'
russian_dict['В']='в'
russian_dict['Г']='г'
russian_dict['Д']='д'
russian_dict['Е']='е'
russian_dict['Ё']='ё'
russian_dict['Ж']='ж'
russian_dict['З']='з'
russian_dict['И']='и'
russian_dict['Й']='й'
russian_dict['К']='к'
russian_dict['Л']='л'
russian_dict['М']='м'
russian_dict['Н']='н'
russian_dict['О']='о'
russian_dict['Р']='р'
russian_dict['П']='п'
russian_dict['С']='с'
russian_dict['Т']='т'
russian_dict['У']='у'
russian_dict['Ф']='ф'
russian_dict['Х']='х'
russian_dict['Ц']='ц'
russian_dict['Ч']='ч'
russian_dict['Ш']='ш'
russian_dict['Щ']='щ'
russian_dict['Ъ']='ъ'
russian_dict['Ы']='ы'
russian_dict['Ь']='ь'
russian_dict['Э']='э'
russian_dict['Ю']='ю'
russian_dict['Я']='я'
russian_dict['а']='a'
russian_dict['б']='a'
russian_dict['в']='a'
russian_dict['г']='a'
russian_dict['д']='a'
russian_dict['е']='a'
russian_dict['ё']='a'
russian_dict['ж']='a'
russian_dict['з']='a'
russian_dict['и']='a'
russian_dict['й']='a'
russian_dict['к']='a'
russian_dict['л']='a'
russian_dict['м']='a'
russian_dict['н']='a'
russian_dict['о']='a'
russian_dict['р']='a'
russian_dict['п']='a'
russian_dict['с']='a'
russian_dict['т']='a'
russian_dict['у']='a'
russian_dict['ф']='a'
russian_dict['х']='a'
russian_dict['ц']='a'
russian_dict['ч']='a'
russian_dict['ш']='a'
russian_dict['щ']='a'
russian_dict['ъ']='a'
russian_dict['ы']='a'
russian_dict['ь']='a'
russian_dict['э']='a'
russian_dict['ю']='a'
russian_dict['я']='a'

combined_dict={}
combined_dict['A']='a'
combined_dict['B']='b'
combined_dict['C']='c'
combined_dict['D']='d'
combined_dict['E']='e'
combined_dict['F']='f'
combined_dict['G']='g'
combined_dict['H']='h'
combined_dict['I']='i'
combined_dict['J']='j'
combined_dict['K']='k'
combined_dict['L']='l'
combined_dict['M']='m'
combined_dict['N']='n'
combined_dict['O']='o'
combined_dict['P']='p'
combined_dict['Q']='q'
combined_dict['R']='r'
combined_dict['S']='s'
combined_dict['T']='t'
combined_dict['U']='u'
combined_dict['V']='v'
combined_dict['W']='w'
combined_dict['X']='x'
combined_dict['Y']='y'
combined_dict['Z']='z'
combined_dict['А']='а'
combined_dict['Б']='б'
combined_dict['В']='в'
combined_dict['Г']='г'
combined_dict['Д']='д'
combined_dict['Е']='е'
combined_dict['Ё']='ё'
combined_dict['Ж']='ж'
combined_dict['З']='з'
combined_dict['И']='и'
combined_dict['Й']='й'
combined_dict['К']='к'
combined_dict['Л']='л'
combined_dict['М']='м'
combined_dict['Н']='н'
combined_dict['О']='о'
combined_dict['Р']='р'
combined_dict['П']='п'
combined_dict['С']='с'
combined_dict['Т']='т'
combined_dict['У']='у'
combined_dict['Ф']='ф'
combined_dict['Х']='х'
combined_dict['Ц']='ц'
combined_dict['Ч']='ч'
combined_dict['Ш']='ш'
combined_dict['Щ']='щ'
combined_dict['Ъ']='ъ'
combined_dict['Ы']='ы'
combined_dict['Ь']='ь'
combined_dict['Э']='э'
combined_dict['Ю']='ю'
combined_dict['Я']='я'
combined_dict['0']='0'
combined_dict['1']='1'
combined_dict['2']='2'
combined_dict['3']='3'
combined_dict['4']='4'
combined_dict['5']='5'
combined_dict['6']='6'
combined_dict['7']='7'
combined_dict['8']='8'
combined_dict['9']='9'
lowercase_letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
roman_numerals=['II','I I','III','I I I','IV','I V','VI','V I','VII','V I I','VIII','V I I I','IX','I X','XI','X I','XII','X I I','XIII','X I I I','XIV','X I V','XV','X V','XVI','X V I','XVII','X V I I','XVIII','X V I I I','XIX','X I X','XX','X X','XXI','X X I','XXII','X X I I','ХХI', 'XXIII', 'X X I I I','XXIV', 'X X I V','XXV','X X V', 'XXVI', 'X X V I', 'XXVII', 'X X V I I', 'XXVIII', 'X X V I I I', 'XXIX', 'X X I X','XXX', 'X X X', 'XXXI', 'X X X I', 'XXXII', 'X X X I I ', 'XXXIII', 'X X X I I', 'XXXIV', 'X X X I V', 'XXXV', 'X X X V', 'XXXVI', 'X X X V I', 'XXXVII', 'X X X V I I', 'XXXVIII','X X X V I I I','XXXIX','X X X I X', 'XXXX', 'X X X X']
roman_numeral_exceptions=['I','V', 'X']
roman_numerals_full=['I','V','X','II','I I','III','I I I','IV','I V','VI','V I','VII','V I I','VIII','V I I I','IX','I X','XI','X I','XII','X I I','XIII','X I I I','XIV','X I V','XV','X V','XVI','X V I','XVII','X V I I','XVIII','X V I I I','XIX','X I X','XX','X X','XXI','X X I','XXII','X X I I','ХХI', 'XXIII', 'X X I I I','XXIV', 'X X I V','XXV','X X V', 'XXVI', 'X X V I', 'XXVII', 'X X V I I', 'XXVIII', 'X X V I I I', 'XXIX', 'X X I X','XXX', 'X X X', 'XXXI', 'X X X I', 'XXXII', 'X X X I I ', 'XXXIII', 'X X X I I', 'XXXIV', 'X X X I V', 'XXXV', 'X X X V', 'XXXVI', 'X X X V I', 'XXXVII', 'X X X V I I', 'XXXVIII','X X X V I I I','XXXIX','X X X I X', 'XXXX', 'X X X X']
numbers=['0','1','2','3','4','5','6','7','8','9']
time_marks=[':']
time_zones = ['UTC', 'GMT', 'PM', 'p.m.', 'AM', 'a.m.']
punc_marks = ['.','/']
telephone_marks = ['-','—']
measurement_marks=['—','-']
full_clause_marks = ['(', '.', ',', ')', '-', '—', '»', '«', ';', ':', '!', '?']
some_symbols = ['%', '$', '=']
clause_marks = ['.',',', '(']
new_clause = ['(', ',']
directions = ['юг','юж','север','запад','ост']
months = ['Январь', 'январь', 'Февраль', 'февраль', 'март', 'Март', 'Апрель', 'апрель', 'Май', 'май', 'Июнь', 'июнь', 'Июль', 'июль', 'август', 'Август', 'Сентябрь', 'сентябрь', 'Октябрь', 'октябрь', 'Ноябрь', 'ноябрь', 'Декабрь', 'декабрь' ]
month_abbreviations={}
month_abbreviations['авг.']='августа'
month_abbreviations['дек.']='декабря'
month_abbreviations['фев.']='февраля'
gen_months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря', 'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']
gen_months_upper=['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']
prep_months = ['январе', 'феврале', 'марте', 'апреле', 'мае', 'июне', 'июле', 'августе', 'сентябре', 'октябре', 'ноябре', 'декабре', 'Январе', 'Феврале', 'Марте', 'Апреле', 'Мае', 'Июне', 'Июле', 'Августе', 'Сентябре', 'Октябре', 'Ноябре', 'Декабре']
prep_months_upper=['Январе', 'Феврале', 'Марте', 'Апреле', 'Мае', 'Июне', 'Июле', 'Августе', 'Сентябре', 'Октябре', 'Ноябре', 'Декабре']
dat_months = ['январю', 'февралю', 'марту', 'апрелю', 'маю', 'июню', 'июлю', 'августу', 'сентябрю', 'октябрю', 'ноябрю', 'декабрю']
inst_months = ['январем', 'февралем', 'мартом', 'апрелем', 'маем', 'июнем', 'июлем', 'августом', 'сентябрем', 'октябрем', 'ноябрем', 'декабрем']
seasons = [ 'Весна', 'весна', 'Лето', 'лето', 'Осень', 'осень', 'зима', 'Зима', 'Весной', 'весной', 'Летом', 'летом', 'Осенью', 'осенью','зимой', 'Зимой' ]
of_our_lord=['до н. э.','год до н. э.','гг. до н. э.','г. до н. э.','году до н. э','годах до н. э','года до н. э','годами до н. э.']

dating = ['Медиа','Telegram','Newsweek', 'ГАЗЕТА','Газета', 'Sport', 'Правда', 'daily','Daily', 'YouTube','Report', 'Телекритика', 'Свобода', '»', 'Государства', 'Sports.ru', 'Звезды','Коммерсантъ',
	  'Telegraph','UEFA','Hurriyet', 'Известия','сайт','Москва','System','Reporter','BBC','AVN','Компьютерра','ведомости','неформат','Courier','Ru','Ру','NME','ни','CNN','Zeit','Взгляд','Телесемь']
websites_oye=['Утро.ru', 'UzReport','f1news.ru','SportaCentrs.com','GameSpot','Lenta.ru','lenta.ru','Афиша','Чемпионат.com','Bigmir.net','SecurityLab.ru','Moviesushi','AVN.com','Inc','sovsport.ru/news',
	      'NEWSru','NEWSmusic.ru','tribuna.md','RFS.com','Deadline.com','Delfi.lv']
two_step=['газета', 'Российская','York','Times','Новости','РИА']

neutral_endings = ['ое', 'ие']
feminine_endings = ['я', 'ть', 'а', 'ой']

trigger_endings_b4_1=['ы','и']
trigger_endings_b4_2=['же','ии','ке','ля']
trigger_endings_a4_1=[]
trigger_endings_a4_2=['ях','ия','их']

trigger_prep_a4=['ии']
trigger_gen_a4=['ия']
def transify(word):
 a=[]
 word=translit(word, 'ru')
 for i,v in enumerate(word):
    #print(i,v)
    b = word[i]+'_trans'+' '
    a.append(b)
    #print(''.join(a))
 return ''.join(a)

 

genitive_prepositions=['более','Более','с', 'С','до','До','от','От','после','После' , 'без','Без','вне','Вне', 'вместо', 'Вместо', 'вокруг','Вокруг','около','Около', 'для','Для', 'из', 'Из', 'изо','Изо', 'из-за','Из-за',  'из-под','Из-под', 'кроме','Кроме', 'мимо','Мимо', 'ото','Ото','у','У', 'посреди','Посреди', 'среди','Среди','напротив', 'Напротив', 'против','Против', 'близ','Близ',  
            'вдоль','Вдоль' 'возле', 'Возле','внутри','Внутри','ради','Ради', 'накануне', 'Накануне', 'меньше', 'Меньше','менее','Менее',  'начала', 'Начала', 'Начало', 'начало', 'конца', 'Конца', 'конце','Конце','первоисточника', 'Первоисточника', 'утра', 'Утра', 'вечера', 'Вечера', 'году', 'Году','выше','Выше','население','Население','начале','Начале','среднем','войны','примерно','уровне','вес','сумму','свыше']

accusative_prepositions=['По', 'по']# 'на', 'На']
po=['По', 'по']
nominative_prepositions=['на', 'На', 'за', 'За', 'Через', 'через']
prepositional_prepositions=['в', 'В', 'во', 'Во', 'О', 'о', 'при', 'При', 'об', 'Об', 'Обо', 'обо']
dative_prepositions=['К', 'к', 'ко', 'Ко', 'согласно','Согласно','назло', 'Назло', 'благодаря', 'Благодаря']
instrumental_prepositions=['между','Между','над','Над','перед','Перед','Под','под','Со', 'со']
number_accusative=['на', 'На']
crossover=['с', 'С','Со', 'со',]
years_abbreviations = ['год', 'Год', 'года','Года','году','Году', 'гг', 'ГГ', 'ГГ.', 'гг.', 'годами','Годами','годом','Годом','годы', 'Годы', 'годах', 'Годах', 'г.', 'Г.']
years_abbreviations_ab=[ 'гг', 'ГГ', 'ГГ.', 'гг.', 'г.', 'Г.', 'года','Года']

full_prepositions = ['По', 'по', 'за', 'За', 'Через', 'через','на', 'На','в', 'В', 'во', 'Во', 'О', 'о', 'при', 'При', 'об', 'Об', 'Обо', 'обо','К', 'к', 'ко', 'Ко', 'согласно','Согласно','назло', 'Назло', 'благодаря', 'Благодаря','между','Между','над','Над','перед','Перед','Под','под','Со', 'со','более','Более','с', 'С','до','До','от','От','после','После' , 'без','Без','вне','Вне',  'вместо', 'Вместо', 'вокруг','Вокруг', 'около','Около', 'для','Для', 'из', 'Из', 'изо','Изо', 'из-за','Из-за',  'из-под','Из-под', 'кроме','Кроме', 'мимо','Мимо', 'ото','Ото','у','У', 'посреди','Посреди', 'среди','Среди','напротив', 'Напротив', 'против','Против', 'близ','Близ', 'вдоль','Вдоль' 'возле', 'Возле','внутри','Внутри','ради','Ради', 'накануне', 'Накануне', 'меньше', 'Меньше','менее','Менее',  'начала', 'Начала', 'Начало', 'начало', 'конца', 'Конца', 'конце','Конце','первоисточника', 'Первоисточника', 'утра', 'Утра', 'вечера', 'Вечера', 'году', 'Году','выше','Выше','население','Население','начале','Начале','вес','сумму','свыше']


royals= ['Густав','Николай','Александр','Константин','Бернар','Петр','Вильгельм','Эдуард','Карл','Яков','Павел','Георгий','Георг','Алексий','Адольф','Иоанн','Генрих','Леопольд','Гильом',
	 'Роберт','Оттон','Гуго','Конрад','Амори','Давид','Болеслав','Иоганн','Иосиф','Василий','Санчо','Луи','Гвидо','Людвиг','Мехмед', 'Иван', 'Фредерик', 'Наполеон', 'Генрих','Кристиан', 'Владимир', 'Юстиниан',
	 'Сигизмунд', 'Рено', 'Иннокентий', 'Август', 'Филипп', 'Ричард', 'Роже', 'Бенедикт', 'Алексей', 'Урбан', 'Виллем', 'Ираклий', 'Ашот', 'Фердинанд','Андроник', 'Козимо', 'Владислав', 'Рудольф', 'Григорий',
	 'Казимир', 'Санш', 'Жоффруа','Энрике','Тутмос','Аршак','Суворов','Абасс','Гордиан','Раймунд','Сикст']
gen_royals=['Густава','Николая','Александра','Константина','Бернара','Петра','Вильгельма','Эдуарда','Карла','Якова','Павла','Георгия','Георга','Алексия','Адольфа','Иоанна','Генриха','Юлия','Леопольда','Гильома',
	    'Роберта','Оттона','Конрада','Давида','Болеслава','Иоганна','Иосифа','Василия','Кристиана','короля','Короля','Людвига','Мехмеда', 'Льва','Ивана', 'Фредерика', 'Наполеона', 'Генриха', 'Владимира', 'Юстиниана','Сигизмунда', 'Иннокентия', 'Августа', 'Лотаря', 'Филиппа','Ричарда', 'Бенедикта', 'Алексея', 'Урбана', 'Виллема', 'Ираклия', 'Ашота', 'Фердинанда', 'Андроника', 'Владислава', 'Рамы',
	    'Рудольфа', 'Григория', 'Казимира', 'Санша','Тутмоса','Аршака','Суворова','Белы','Абасса','Гордиана','Раймунда','Сикста']
royals_female=['Екатерина', 'Елизавета', 'половина', 'Слава', 'Анна']
gen_royals_female=['Екатерины', 'Елизаветы', 'половины', 'Славы', 'Екатериной', 'Екатерине', 'Анны']
royalties = ['князь','Князь','Король','король','император','Император']

prep_royals=['Николае']
inst_royals=['Николаем', 'Павлом', 'Иоанном', 'Людовиком', 'Генрихом', 'Петром', 'Иваном', 'Григорием','Густавом','Александром','Константином','Бернаром','Петром','Вильгельмом','Эдуардом','Карлом','Яковом','Павелом','Георгием','Георгом','Алексием','Адольфом','Иоанном','Генрихом','Леопольдом','Гильомом',
	 'Робертом','Оттоном','Гугоом','Конрадом','Аморим','Давидом','Болеславом','Иоганном','Иосифом','Василием','Санчом','Луиом','Гвидоом','Людвигом','Мехмедом', 'Иваном', 'Фредериком', 'Наполеоном', 'Генрихом','Кристианом', 'Владимиром', 'Юстинианом','Сигизмундом', 'Реноом', 'Иннокентий', 'Августом', 'Филиппом', 'Ричардом', 'Роже', 'Бенедиктом', 'Алексем', 'Урбаном', 'Виллемом', 'Ираклием', 'Ашотом', 'Фердинандом','Андроником', 'Козимом', 'Владиславом', 'Рудольфом', 'Григорием',
	 'Казимиром', 'Саншом']
dat_royals=['Фридриху', 'Александру', 'Ивану', 'князю', 'Генриху', 'Карлу','Николау', 'Павлу', 'Иоанну', 'Людовику', 'Генриху', 'Петру', 'Ивану', 'Григориу','Густаву','Александру','Константину','Бернару','Вильгельму','Эдуарду','Карлу','Якову','Павелу','Георгиу','Георгу','Алексиу','Адольфу','Иоанну','Генриху','Леопольду','Гильому',
	 'Роберту','Оттону','Гугоу','Конраду','Амориу','Давиду','Болеславу','Иоганну','Иосифу','Василиу','Санчу','Луиу','Гвидоу','Людвигу','Мехмеду', 'Ивану', 'Фредерику', 'Наполеону', 'Генриху','Кристиану', 'Владимиру', 'Юстиниану','Сигизмунду', 'Реноу', 'Иннокенту', 'Августу', 'Филиппу', 'Ричарду', 'Рожу', 'Бенедикту', 'Алексу', 'Урбану', 'Виллему', 'Ираклиу', 'Ашоту', 'Фердинанду','Андронику', 'Козиму', 'Владиславу', 'Рудольфу', 'Григориу','Казимиру', 'Саншу']




#if (any(x in 'до') for x in genitive_prepositions):
#  print('poo')
prep_ord_dict={}
prep_ord_dict['ноль']='нулевом'
prep_ord_dict['один']='первом'
prep_ord_dict['два']='втором'
prep_ord_dict['две']='втором'
prep_ord_dict['три']='третьем'
prep_ord_dict['четыре']='четвертом'
prep_ord_dict['одна']='первом'
prep_ord_dict['одно']='первом'
prep_ord_dict['одни']='первом'
prep_ord_dict['пять']='пятом'
prep_ord_dict['шесть']='шестом'
prep_ord_dict['семь']='седьмом'
prep_ord_dict['восемь']='восьмом'
prep_ord_dict['девять']='девятом'
prep_ord_dict['десять']='десятом'
prep_ord_dict['одиннадцать']='одиннадцатом'
prep_ord_dict['двенадцать']='двенадцатом'
prep_ord_dict['тринадцать']='тринадцатом'
prep_ord_dict['четырнадцать']='четырнадцатом'
prep_ord_dict['пятнадцать']='пятнадцатом'
prep_ord_dict['шестнадцать']='шестнадцатом'
prep_ord_dict['семнадцать']='семнадцатом'
prep_ord_dict['восемнадцать']='восемнадцатом'
prep_ord_dict['девятнадцать']='девятнадцатом'
prep_ord_dict['двадцать']='двадцатом'
prep_ord_dict['тридцать']='тридцатом'
prep_ord_dict['сорок']='сороковом'
prep_ord_dict['пятьдесят']='пятидесятом'
prep_ord_dict['шестьдесят']='шестидесятом'
prep_ord_dict['семьдесят']='семидесятом'
prep_ord_dict['восемьдесят']='восьмидесятом'
prep_ord_dict['девяносто']='девяностом'
prep_ord_dict['сто']='сотом'
prep_ord_dict['двести']='двухсотом'
prep_ord_dict['триста']='трехсотом'
prep_ord_dict['четыреста']='четырехсотом'
prep_ord_dict['пятьсот']='пятисотом'
prep_ord_dict['шестьсот']='шестисотом'
prep_ord_dict['семьсот']='семисотом'
prep_ord_dict['восемьсот']='восьмисотом'
prep_ord_dict['девятьсот']='девятисотом'
prep_ord_dict['девятсот']='девятисотом'
prep_ord_dict['тысяча']='тысячном'
prep_ord_dict['тысячи']='тысячном'
prep_ord_dict['тысяч']='тысячном'
prep_ord_dict['миллион'] = 'миллионом'
prep_ord_dict['миллиона'] = 'миллионом'
prep_ord_dict['миллионов'] ='миллионом'

prep_car_dict={}
prep_car_dict['ноль']='ноле'
prep_car_dict['один']='одном'
prep_car_dict['два']='двух'
prep_car_dict['две']='двух'
prep_car_dict['три']='трех'
prep_car_dict['четыре']='четырех'
prep_car_dict['одна']='одном'
prep_car_dict['одно']='одном'
prep_car_dict['одни']='одном'
prep_car_dict['пять']='пяти'
prep_car_dict['шесть']='шести'
prep_car_dict['семь']='семи'
prep_car_dict['восемь']='восьми'
prep_car_dict['девять']='девяти'
prep_car_dict['десять']='десяти'
prep_car_dict['одиннадцать']='одиннадцати'
prep_car_dict['двенадцать']='двенадцати'
prep_car_dict['тринадцать']='тринадцати'
prep_car_dict['четырнадцать']='четырнадцати'
prep_car_dict['пятнадцать']='пятнадцати'
prep_car_dict['шестнадцать']='шестнадцати'
prep_car_dict['семнадцать']='семнадцати'
prep_car_dict['восемнадцать']='восемнадцати'
prep_car_dict['девятнадцать']='девятнадцати'
prep_car_dict['двадцать']='двадцати'
prep_car_dict['тридцать']='тридцати'
prep_car_dict['сорок']='сорока'
prep_car_dict['пятьдесят']='пятидесяти'
prep_car_dict['шестьдесят']='шестидесяти'
prep_car_dict['семьдесят']='семидесяти'
prep_car_dict['восемьдесят']='восьмидесяти'
prep_car_dict['девяносто']='девяноста'
prep_car_dict['сто']='ста'
prep_car_dict['двести']='двухстах'
prep_car_dict['триста']='трехстах'
prep_car_dict['четыреста']='четырехстах'
prep_car_dict['пятьсот']='пятистах'
prep_car_dict['шестьсот']='шестистах'
prep_car_dict['семьсот']='семистах'
prep_car_dict['восемьсот']='восьмистах'
prep_car_dict['девятьсот']='девятистах'
prep_car_dict['девятсот']='девятистах'
prep_car_dict['тысяча']='тысячью'
prep_car_dict['тысячи']='тысячах'
prep_car_dict['тысяч']='тысячах'
prep_car_dict['миллион'] = 'миллионе'
prep_car_dict['миллиона'] = 'миллионах'
prep_car_dict['миллионов'] ='миллионах'

inst_ord_dict={}
inst_ord_dict['ноль']='нулевым'
inst_ord_dict['один']='первым'
inst_ord_dict['два']='вторым'
inst_ord_dict['две']='вторым'
inst_ord_dict['три']='третьим'
inst_ord_dict['четыре']='четвертым'
inst_ord_dict['одна']='первым'
inst_ord_dict['одно']='первым'
inst_ord_dict['одни']='первым'
inst_ord_dict['пять']='пятым'
inst_ord_dict['шесть']='шестым'
inst_ord_dict['семь']='седьмым'
inst_ord_dict['восемь']='восьмым'
inst_ord_dict['девять']='девятым'
inst_ord_dict['десять']='десятым'
inst_ord_dict['одиннадцать']='одиннадцатым'
inst_ord_dict['двенадцать']='двенадцатым'
inst_ord_dict['тринадцать']='тринадцатым'
inst_ord_dict['четырнадцать']='четырнадцатым'
inst_ord_dict['пятнадцать']='пятнадцатым'
inst_ord_dict['шестнадцать']='шестнадцатым'
inst_ord_dict['семнадцать']='семнадцатым'
inst_ord_dict['восемнадцать']='восемнадцатым'
inst_ord_dict['девятнадцать']='девятнадцатым'
inst_ord_dict['двадцать']='двадцатым'
inst_ord_dict['тридцать']='тридцатым'
inst_ord_dict['сорок']='сороковым'
inst_ord_dict['пятьдесят']='пятидесятым'
inst_ord_dict['шестьдесят']='шестидесятым'
inst_ord_dict['семьдесят']='семидесятым'
inst_ord_dict['восемьдесят']='восьмидесятым'
inst_ord_dict['девяносто']='девяностым'
inst_ord_dict['сто']='сотым'
inst_ord_dict['двести']='двухсотым'
inst_ord_dict['триста']='трехсотым'
inst_ord_dict['четыреста']='четырехсотым'
inst_ord_dict['пятьсот']='пятисотым'
inst_ord_dict['шестьсот']='шестисотым'
inst_ord_dict['семьсот']='семисотым'
inst_ord_dict['восемьсот']='восьмисотым'
inst_ord_dict['девятьсот']='девятисотым'
inst_ord_dict['девятсот']='девятисотым'
inst_ord_dict['тысяча']='тысячным'
inst_ord_dict['тысячи']='тысячным'
inst_ord_dict['тысяч']='тысячным'
inst_ord_dict['миллион'] = 'миллионым'
inst_ord_dict['миллиона'] = 'миллионым'
inst_ord_dict['миллионов'] ='миллионым'

inst_car_dict={}
inst_car_dict['ноль']='нулем'
inst_car_dict['один']='одним'
inst_car_dict['два']='двумя'
inst_car_dict['две']='двумя'
inst_car_dict['три']='тремя'
inst_car_dict['четыре']='четырьмя'
inst_car_dict['одна']='одним'
inst_car_dict['одно']='одним'
inst_car_dict['одни']='одним'
inst_car_dict['пять']='пятью'
inst_car_dict['шесть']='шестью'
inst_car_dict['семь']='семью'
inst_car_dict['восемь']='восьмью'
inst_car_dict['девять']='девятью'
inst_car_dict['десять']='десятью'
inst_car_dict['одиннадцать']='одиннадцатью'
inst_car_dict['двенадцать']='двенадцатью'
inst_car_dict['тринадцать']='тринадцатью'
inst_car_dict['четырнадцать']='четырнадцатью'
inst_car_dict['пятнадцать']='пятнадцатью'
inst_car_dict['шестнадцать']='шестнадцатью'
inst_car_dict['семнадцать']='семнадцатью'
inst_car_dict['восемнадцать']='восемнадцатью'
inst_car_dict['девятнадцать']='девятнадцатью'
inst_car_dict['двадцать']='двадцатью'
inst_car_dict['тридцать']='тридцатью'
inst_car_dict['сорок']='сорока'
inst_car_dict['пятьдесят']='пятьюдесятью'
inst_car_dict['шестьдесят']='шестьюдесятью'
inst_car_dict['семьдесят']='семьюдесятью'
inst_car_dict['восемьдесят']='восьмьюдесятью'
inst_car_dict['девяносто']='девяноста'
inst_car_dict['сто']='ста'
inst_car_dict['двести']='двумястами'
inst_car_dict['триста']='тремястами'
inst_car_dict['четыреста']='четырьмястами'
inst_car_dict['пятьсот']='пятьюстами'
inst_car_dict['шестьсот']='шестьюстами'
inst_car_dict['семьсот']='семьюстами'
inst_car_dict['восемьсот']='восьмьюстами'
inst_car_dict['девятьсот']='девятьюстами'
inst_car_dict['девятсот']='девятьюстами'
inst_car_dict['тысяча']='тысячей'
inst_car_dict['тысячи']='тысячами'
inst_car_dict['тысяч']='тысячами'
inst_car_dict['миллион'] = 'миллионом'
inst_car_dict['миллиона'] = 'миллионами'
inst_car_dict['миллионов'] ='миллионами'


dat_ord_dict={}
dat_ord_dict['ноль']='нулевому'
dat_ord_dict['один']='первому'
dat_ord_dict['два']='второму'
dat_ord_dict['две']='второму'
dat_ord_dict['три']='третьему'
dat_ord_dict['четыре']='четвертому'
dat_ord_dict['одна']='первому'
dat_ord_dict['одно']='первому'
dat_ord_dict['одни']='первом'
dat_ord_dict['пять']='пятому'
dat_ord_dict['шесть']='шестому'
dat_ord_dict['семь']='седьмому'
dat_ord_dict['восемь']='восьмому'
dat_ord_dict['девять']='девятому'
dat_ord_dict['десять']='десятому'
dat_ord_dict['одиннадцать']='одиннадцатому'
dat_ord_dict['двенадцать']='двенадцатому'
dat_ord_dict['тринадцать']='тринадцатому'
dat_ord_dict['четырнадцать']='четырнадцатому'
dat_ord_dict['пятнадцать']='пятнадцатому'
dat_ord_dict['шестнадцать']='шестнадцатому'
dat_ord_dict['семнадцать']='семнадцатому'
dat_ord_dict['восемнадцать']='восемнадцатому'
dat_ord_dict['девятнадцать']='девятнадцатому'
dat_ord_dict['двадцать']='двадцатому'
dat_ord_dict['тридцать']='тридцатому'
dat_ord_dict['сорок']='сороковому'
dat_ord_dict['пятьдесят']='пятидесятому'
dat_ord_dict['шестьдесят']='шестидесятому'
dat_ord_dict['семьдесят']='семидесятому'
dat_ord_dict['восемьдесят']='восьмидесятому'
dat_ord_dict['девяносто']='девяностому'
dat_ord_dict['сто']='сотому'
dat_ord_dict['двести']='двухсотому'
dat_ord_dict['триста']='трехсотому'
dat_ord_dict['четыреста']='четырехсотому'
dat_ord_dict['пятьсот']='пятисотому'
dat_ord_dict['шестьсот']='шестисотому'
dat_ord_dict['семьсот']='семисотому'
dat_ord_dict['восемьсот']='восьмисотому'
dat_ord_dict['девятьсот']='девятисотому'
dat_ord_dict['девятсот']='девятисотому'
dat_ord_dict['тысяча']='тысячному'
dat_ord_dict['тысячи']='тысячному'
dat_ord_dict['тысяч']='тысячному'
dat_ord_dict['миллион'] = 'миллионому'
dat_ord_dict['миллиона'] = 'миллионому'
dat_ord_dict['миллионов'] ='миллионому'

dat_car_dict={}
dat_car_dict['ноль']='нулю'
dat_car_dict['один']='одному'
dat_car_dict['два']='двум'
dat_car_dict['две']='двум'
dat_car_dict['три']='трем'
dat_car_dict['четыре']='четырем'
dat_car_dict['одна']='одному'
dat_car_dict['одно']='одному'
dat_car_dict['одни']='одному'
dat_car_dict['пять']='пяти'
dat_car_dict['шесть']='шести'
dat_car_dict['семь']='семи'
dat_car_dict['восемь']='восьми'
dat_car_dict['девять']='девяти'
dat_car_dict['десять']='десяти'
dat_car_dict['одиннадцать']='одиннадцати'
dat_car_dict['двенадцать']='двенадцати'
dat_car_dict['тринадцать']='тринадцати'
dat_car_dict['четырнадцать']='четырнадцати'
dat_car_dict['пятнадцать']='пятнадцати'
dat_car_dict['шестнадцать']='шестнадцати'
dat_car_dict['семнадцать']='семнадцати'
dat_car_dict['восемнадцать']='восемнадцати'
dat_car_dict['девятнадцать']='девятнадцати'
dat_car_dict['двадцать']='двадцати'
dat_car_dict['тридцать']='тридцати'
dat_car_dict['сорок']='сорока'
dat_car_dict['пятьдесят']='пятидесяти'
dat_car_dict['шестьдесят']='шестидесяти'
dat_car_dict['семьдесят']='семидесяти'
dat_car_dict['восемьдесят']='восьмидесяти'
dat_car_dict['девяносто']='девяноста'
dat_car_dict['сто']='ста'
dat_car_dict['двести']='двумстам'
dat_car_dict['триста']='тремстам'
dat_car_dict['четыреста']='четыремястам'
dat_car_dict['пятьсот']='пятистам'
dat_car_dict['шестьсот']='шестистам'
dat_car_dict['семьсот']='семистам'
dat_car_dict['восемьсот']='восьмистам'
dat_car_dict['девятьсот']='девятистам'
dat_car_dict['девятсот']='девятистам'
dat_car_dict['тысяча']='тысяче'
dat_car_dict['тысячи']='тысячам'
dat_car_dict['тысяч']='тысячам'
dat_car_dict['миллион'] = 'миллиону'
dat_car_dict['миллиона'] = 'миллиону'
dat_car_dict['миллионов'] ='миллиону'
gen_ord_dict={}
#gen_dict['нуль']=''
gen_ord_dict['ноль']='нулевого'
gen_ord_dict['один']='первого'
gen_ord_dict['i']='первого'
gen_ord_dict['I']='первого'
gen_ord_dict['два']='второго'
gen_ord_dict['ii']='второго'
gen_ord_dict['i i']='второго'
gen_ord_dict['II']='второго'
gen_ord_dict['I I']='второго'
gen_ord_dict['две']='второго'
gen_ord_dict['три']='третьего'
gen_ord_dict['iii']='третьего'
gen_ord_dict['i i i']='третьего'
gen_ord_dict['III']='третьего'
gen_ord_dict['I I I']='третьего'
gen_ord_dict['четыре']='четвертого'
gen_ord_dict['iv']='четвертого'
gen_ord_dict['i v']='четвертого'
gen_ord_dict['IV']='четвертого'
gen_ord_dict['I V']='четвертого'
gen_ord_dict['одна']='первого'
gen_ord_dict['одно']='первого'
gen_ord_dict['одни']='первого'
gen_ord_dict['пять']='пятого'
gen_ord_dict['v']='пятого'
gen_ord_dict['V']='пятого'
gen_ord_dict['шесть']='шестого'
gen_ord_dict['vi']='шестого'
gen_ord_dict['v i']='шестого'
gen_ord_dict['VI']='шестого'
gen_ord_dict['V I']='шестого'
gen_ord_dict['семь']='седьмого'
gen_ord_dict['vii']='седьмого'
gen_ord_dict['v i i']='седьмого'
gen_ord_dict['VII']='седьмого'
gen_ord_dict['V I I']='седьмого'
gen_ord_dict['восемь']='восьмого'
gen_ord_dict['viii']='восьмого'
gen_ord_dict['v i i i']='восьмого'
gen_ord_dict['VIII']='восьмого'
gen_ord_dict['V I I I']='восьмого'
gen_ord_dict['девять']='девятого'
gen_ord_dict['ix']='девятого'
gen_ord_dict['i x']='девятого'
gen_ord_dict['IX']='девятого'
gen_ord_dict['I X']='девятого'
gen_ord_dict['десять']='десятого'
gen_ord_dict['x']='десятого'
gen_ord_dict['X']='десятого'
gen_ord_dict['одиннадцать']='одиннадцатого'
gen_ord_dict['xi']='одиннадцатого'
gen_ord_dict['x i']='одиннадцатого'
gen_ord_dict['XI']='одиннадцатого'
gen_ord_dict['X I']='одиннадцатого'
gen_ord_dict['двенадцать']='двенадцатого'
gen_ord_dict['xii']='двенадцатого'
gen_ord_dict['x i i']='двенадцатого'
gen_ord_dict['XII']='двенадцатого'
gen_ord_dict['X I I']='двенадцатого'
gen_ord_dict['тринадцать']='тринадцатого'
gen_ord_dict['xiii']='тринадцатого'
gen_ord_dict['x i i i']='тринадцатого'
gen_ord_dict['XIII']='тринадцатого'
gen_ord_dict['X I I I']='тринадцатого'
gen_ord_dict['четырнадцать']='четырнадцатого'
gen_ord_dict['xiv']='четырнадцатого'
gen_ord_dict['x i v']='четырнадцатого'
gen_ord_dict['XIV']='четырнадцатого'
gen_ord_dict['X I V']='четырнадцатого'
gen_ord_dict['пятнадцать']='пятнадцатого'
gen_ord_dict['xv']='пятнадцатого'
gen_ord_dict['x v']='пятнадцатого'
gen_ord_dict['XV']='пятнадцатого'
gen_ord_dict['X V']='пятнадцатого'
gen_ord_dict['шестнадцать']='шестнадцатого'
gen_ord_dict['xvi']='шестнадцатого'
gen_ord_dict['x v i']='шестнадцатого'
gen_ord_dict['XVI']='шестнадцатого'
gen_ord_dict['X V I']='шестнадцатого'
gen_ord_dict['семнадцать']='семнадцатого'
gen_ord_dict['xvii']='семнадцатого'
gen_ord_dict['x v i i']='семнадцатого'
gen_ord_dict['XVII']='семнадцатого'
gen_ord_dict['X V I I']='семнадцатого'
gen_ord_dict['восемнадцать']='восемнадцатого'
gen_ord_dict['xviii']='восемнадцатого'
gen_ord_dict['x v i i i']='восемнадцатого'
gen_ord_dict['XVIII']='восемнадцатого'
gen_ord_dict['X V I I I']='восемнадцатого'
gen_ord_dict['девятнадцать']='девятнадцатого'
gen_ord_dict['xix']='девятнадцатого'
gen_ord_dict['x i x']='девятнадцатого'
gen_ord_dict['XIX']='девятнадцатого'
gen_ord_dict['X I X']='девятнадцатого'
gen_ord_dict['двадцать']='двадцатого'
gen_ord_dict['xx']='двадцатого'
gen_ord_dict['x x']='двадцатого'
gen_ord_dict['XX']='двадцатого'
gen_ord_dict['X X']='двадцатого'
gen_ord_dict['xxi']='двадцать первого'
gen_ord_dict['x x i']='двадцать первого'
gen_ord_dict['XXI']='двадцать первого'
gen_ord_dict['X X I']='двадцать первого'
gen_ord_dict['xxii']='двадцать второго'
gen_ord_dict['x x i i']='двадцать второго'
gen_ord_dict['XXII']='двадцать второго'
gen_ord_dict['X X I I']='двадцать второго'
gen_ord_dict['тридцать']='тридцатого'
gen_ord_dict['сорок']='сорокового'
gen_ord_dict['пятьдесят']='пятидесятого'
gen_ord_dict['шестьдесят']='шестидесятого'
gen_ord_dict['семьдесят']='семидесятого'
gen_ord_dict['восемьдесят']='восьмидесятого'
gen_ord_dict['девяносто']='девяностого'
gen_ord_dict['сто']='сотого'
gen_ord_dict['двести']='двухсотого '
gen_ord_dict['триста']='трехсотого'
gen_ord_dict['четыреста']='четырехсотого'
gen_ord_dict['пятьсот']='пятисотого'
gen_ord_dict['шестьсот']='шестисотого'
gen_ord_dict['семьсот']='семисотого'
gen_ord_dict['восемьсот']='восьмисотого'
gen_ord_dict['девятьсот']='девятисотого'
gen_ord_dict['девятсот']='девятисотого'
gen_ord_dict['тысяча']='тысячного'
gen_ord_dict['тысячи']='тысячного'
gen_ord_dict['тысяч']='тысячного'
gen_ord_dict['миллион'] = 'миллионого'
gen_ord_dict['миллиона'] = 'миллионого'
gen_ord_dict['миллионов'] ='миллионого'

gen_car_dict={}
gen_car_dict['ноль']='нуля'
gen_car_dict['один']='одного'
gen_car_dict['два']='двух'
gen_car_dict['две']='двух'
gen_car_dict['три']='трех'
gen_car_dict['четыре']='четырех'
gen_car_dict['одна']='одного'
gen_car_dict['одно']='одного'
gen_car_dict['одни']='одного'
gen_car_dict['пять']='пяти'
gen_car_dict['шесть']='шести'
gen_car_dict['семь']='семи'
gen_car_dict['восемь']='восьми'
gen_car_dict['девять']='девяти'
gen_car_dict['десять']='десяти'
gen_car_dict['одиннадцать']='одиннадцати'
gen_car_dict['двенадцать']='двенадцати'
gen_car_dict['тринадцать']='тринадцати'
gen_car_dict['четырнадцать']='четырнадцати'
gen_car_dict['пятнадцать']='пятнадцати'
gen_car_dict['шестнадцать']='шестнадцати'
gen_car_dict['семнадцать']='семнадцати'
gen_car_dict['восемнадцать']='восемнадцати'
gen_car_dict['девятнадцать']='девятнадцати'
gen_car_dict['двадцать']='двадцати'
gen_car_dict['тридцать']='тридцати'
gen_car_dict['сорок']='сорока'
gen_car_dict['пятьдесят']='пятидесяти'
gen_car_dict['шестьдесят']='шестидесяти'
gen_car_dict['семьдесят']='семидесяти'
gen_car_dict['восемьдесят']='восьмидесяти'
gen_car_dict['девяносто']='девяноста'
gen_car_dict['сто']='ста'
gen_car_dict['двести']='двухсот'
gen_car_dict['триста']='трехсот'
gen_car_dict['четыреста']='четырехсот'
gen_car_dict['пятьсот']='пятисот'
gen_car_dict['шестьсот']='шестисот'
gen_car_dict['семьсот']='семисот'
gen_car_dict['восемьсот']='восьмисот'
gen_car_dict['девятьсот']='девятисот'
gen_car_dict['девятсот']='девятисот'
gen_car_dict['тысяча']='тысячи'
gen_car_dict['тысячи']='тысяч'
gen_car_dict['тысяч']='тысяч'
gen_car_dict['миллион'] = 'миллиона'
gen_car_dict['миллиона'] = 'миллиона'
gen_car_dict['миллионов'] ='миллиона'


acc_ord_dict={}
#acc_dict['нуль']=''
acc_ord_dict['ноль']='нулевое'
acc_ord_dict['один']='первый'
acc_ord_dict['два']='второй'
acc_ord_dict['две']='второй'
acc_ord_dict['три']='третий'
acc_ord_dict['четыре']='четвертый'
acc_ord_dict['одна']='первый'
acc_ord_dict['одно']='первый'
acc_ord_dict['одни']='первый'
acc_ord_dict['пять']='пятый'
acc_ord_dict['шесть']='шестой'
acc_ord_dict['семь']='седьмой'
acc_ord_dict['восемь']='восьмой'
acc_ord_dict['девять']='девятый'
acc_ord_dict['десять']='десятый'
acc_ord_dict['одиннадцать']='одиннадцатый'
acc_ord_dict['двенадцать']='двенадцатый'
acc_ord_dict['тринадцать']='тринадцатый'
acc_ord_dict['четырнадцать']='четырнадцатый'
acc_ord_dict['пятнадцать']='пятнадцатый'
acc_ord_dict['шестнадцать']='шестнадцатый'
acc_ord_dict['семнадцать']='семнадцатый'
acc_ord_dict['восемнадцать']='восемнадцатый'
acc_ord_dict['девятнадцать']='девятнадцатый'
acc_ord_dict['двадцать']='двадцатый'
acc_ord_dict['тридцать']='тридцатый'
acc_ord_dict['сорок']='сороковой'
acc_ord_dict['пятьдесят']='пятидесятый'
acc_ord_dict['шестьдесят']='шестидесятый'
acc_ord_dict['семьдесят']='семидесятый'
acc_ord_dict['восемьдесят']='восьмидесятый'
acc_ord_dict['девяносто']='девяностый'
acc_ord_dict['сто']='сотый'
acc_ord_dict['двести']='двухсотый'
acc_ord_dict['триста']='трехсотый'
acc_ord_dict['четыреста']='четырехсотый'
acc_ord_dict['пятьсот']='пятисотый'
acc_ord_dict['шестьсот']='шестисотый'
acc_ord_dict['семьсот']='семисотый'
acc_ord_dict['восемьсот']='восьмисотый'
acc_ord_dict['девятьсот']='девятисотый'
acc_ord_dict['девятсот']='девятисотый'
acc_ord_dict['тысяча']='тысячный'
acc_ord_dict['тысячи']='тысячный'
acc_ord_dict['тысяч']='тысячный'
acc_ord_dict['миллион'] = 'миллионный'
acc_ord_dict['миллиона'] = 'миллионный'
acc_ord_dict['миллионов'] ='миллионный'

acc_car_dict={}
acc_car_dict['ноль']='нуля'
acc_car_dict['один']='одного'
acc_car_dict['два']='двух'
acc_car_dict['две']='двух'
acc_car_dict['три']='трех'
acc_car_dict['четыре']='четырех'
acc_car_dict['одна']='одного'
acc_car_dict['одно']='одного'
acc_car_dict['одни']='одного'
acc_car_dict['пять']='пяти'
acc_car_dict['шесть']='шести'
acc_car_dict['семь']='семи'
acc_car_dict['восемь']='восьми'
acc_car_dict['девять']='девяти'
acc_car_dict['десять']='десяти'
acc_car_dict['одиннадцать']='одиннадцати'
acc_car_dict['двенадцать']='двенадцати'
acc_car_dict['тринадцать']='тринадцати'
acc_car_dict['четырнадцать']='четырнадцати'
acc_car_dict['пятнадцать']='пятнадцати'
acc_car_dict['шестнадцать']='шестнадцати'
acc_car_dict['семнадцать']='семнадцати'
acc_car_dict['восемнадцать']='восемнадцати'
acc_car_dict['девятнадцать']='девятнадцати'
acc_car_dict['двадцать']='двадцати'
acc_car_dict['тридцать']='тридцати'
acc_car_dict['сорок']='сорока'
acc_car_dict['пятьдесят']='пятидесяти'
acc_car_dict['шестьдесят']='шестидесяти'
acc_car_dict['семьдесят']='семидесяти'
acc_car_dict['восемьдесят']='восьмидесяти'
acc_car_dict['девяносто']='девяноста'
acc_car_dict['сто']='ста'
acc_car_dict['двести']='двухсот'
acc_car_dict['триста']='трехсот'
acc_car_dict['четыреста']='четырехсот'
acc_car_dict['пятьсот']='пятисот'
acc_car_dict['шестьсот']='шестисот'
acc_car_dict['семьсот']='семисот'
acc_car_dict['восемьсот']='восьмисот'
acc_car_dict['девятьсот']='девятисот'
acc_car_dict['девятсот']='девятисот'
acc_car_dict['тысяча']='тысячу'
acc_car_dict['тысячи']='тысячи'
acc_car_dict['тысяч']='тысячи'
acc_car_dict['миллион'] = 'миллиона'
acc_car_dict['миллиона'] = 'миллиона'
acc_car_dict['миллионов'] ='миллиона'

uyu_ord_dict={}
uyu_ord_dict['ноль']='нулевую'
uyu_ord_dict['один']='первую'
uyu_ord_dict['два']='вторую'
uyu_ord_dict['две']='вторую'
uyu_ord_dict['три']='третью'
uyu_ord_dict['четыре']='четвертую'
uyu_ord_dict['одна']='первую'
uyu_ord_dict['одно']='первую'
uyu_ord_dict['одни']='первую'
uyu_ord_dict['пять']='пятую'
uyu_ord_dict['шесть']='шестую'
uyu_ord_dict['семь']='седьмую'
uyu_ord_dict['восемь']='восьмую'
uyu_ord_dict['девять']='девятую'
uyu_ord_dict['десять']='десятую'
uyu_ord_dict['одиннадцать']='одиннадцатую'
uyu_ord_dict['двенадцать']='двенадцатую'
uyu_ord_dict['тринадцать']='тринадцатую'
uyu_ord_dict['четырнадцать']='четырнадцатую'
uyu_ord_dict['пятнадцать']='пятнадцатую'
uyu_ord_dict['шестнадцать']='шестнадцатую'
uyu_ord_dict['семнадцать']='семнадцатую'
uyu_ord_dict['восемнадцать']='восемнадцатую'
uyu_ord_dict['девятнадцать']='девятнадцатую'
uyu_ord_dict['двадцать']='двадцатую'
uyu_ord_dict['тридцать']='тридцатую'
uyu_ord_dict['сорок']='сороковую'
uyu_ord_dict['пятьдесят']='пятидесятую'
uyu_ord_dict['шестьдесят']='шестидесятую'
uyu_ord_dict['семьдесят']='семидесятую'
uyu_ord_dict['восемьдесят']='восьмидесятую'
uyu_ord_dict['девяносто']='девяностую'
uyu_ord_dict['сто']='сотую'
uyu_ord_dict['двести']='двухсотую'
uyu_ord_dict['триста']='трехсотую'
uyu_ord_dict['четыреста']='четырехсотую'
uyu_ord_dict['пятьсот']='пятисотую'
uyu_ord_dict['шестьсот']='шестисотую'
uyu_ord_dict['семьсот']='семисотую'
uyu_ord_dict['восемьсот']='восьмисотую'
uyu_ord_dict['девятьсот']='девятисотую'
uyu_ord_dict['девятсот']='девятисотую'
uyu_ord_dict['тысяча']='тысячную'
uyu_ord_dict['тысячи']='тысячную'
uyu_ord_dict['тысяч']='тысячную'
uyu_ord_dict['миллион'] = 'миллионную'
uyu_ord_dict['миллиона'] = 'миллионную'
uyu_ord_dict['миллионов'] ='миллионную'

uye_ord_dict={}
uye_ord_dict['ноль']='нулевые'
uye_ord_dict['один']='первые'
uye_ord_dict['два']='вторые'
uye_ord_dict['две']='вторые'
uye_ord_dict['три']='третье'
uye_ord_dict['четыре']='четвертые'
uye_ord_dict['одна']='первые'
uye_ord_dict['одно']='первые'
uye_ord_dict['одни']='первые'
uye_ord_dict['пять']='пятые'
uye_ord_dict['шесть']='шестые'
uye_ord_dict['семь']='седьмые'
uye_ord_dict['восемь']='восьмые'
uye_ord_dict['девять']='девятые'
uye_ord_dict['десять']='десятые'
uye_ord_dict['одиннадцать']='одиннадцатые'
uye_ord_dict['двенадцать']='двенадцатые'
uye_ord_dict['тринадцать']='тринадцатые'
uye_ord_dict['четырнадцать']='четырнадцатые'
uye_ord_dict['пятнадцать']='пятнадцатые'
uye_ord_dict['шестнадцать']='шестнадцатые'
uye_ord_dict['семнадцать']='семнадцатые'
uye_ord_dict['восемнадцать']='восемнадцатые'
uye_ord_dict['девятнадцать']='девятнадцатые'
uye_ord_dict['двадцать']='двадцатые'
uye_ord_dict['тридцать']='тридцатые'
uye_ord_dict['сорок']='сороковые'
uye_ord_dict['пятьдесят']='пятидесятые'
uye_ord_dict['шестьдесят']='шестидесятые'
uye_ord_dict['семьдесят']='семидесятые'
uye_ord_dict['восемьдесят']='восьмидесятые'
uye_ord_dict['девяносто']='девяностые'
uye_ord_dict['сто']='сотые'
uye_ord_dict['двести']='двухсотые'
uye_ord_dict['триста']='трехсотые'
uye_ord_dict['четыреста']='четырехсотые'
uye_ord_dict['пятьсот']='пятисотые'
uye_ord_dict['шестьсот']='шестисотые'
uye_ord_dict['семьсот']='семисотые'
uye_ord_dict['восемьсот']='восьмисотые'
uye_ord_dict['девятьсот']='девятисотые'
uye_ord_dict['девятсот']='девятисотые'
uye_ord_dict['тысяча']='тысячные'
uye_ord_dict['тысячи']='тысячные'
uye_ord_dict['тысяч']='тысячные'
uye_ord_dict['миллион'] = 'миллионные'
uye_ord_dict['миллиона'] = 'миллионные'
uye_ord_dict['миллионов'] ='миллионные'

oye_ord_dict={}
oye_ord_dict['ноль']='нулевое'
oye_ord_dict['один']='первое'
oye_ord_dict['два']='второе'
oye_ord_dict['две']='второе'
oye_ord_dict['три']='третье'
oye_ord_dict['четыре']='четвертое'
oye_ord_dict['одна']='первое'
oye_ord_dict['одно']='первое'
oye_ord_dict['одни']='первое'
oye_ord_dict['пять']='пятое'
oye_ord_dict['шесть']='шестое'
oye_ord_dict['семь']='седьмое'
oye_ord_dict['восемь']='восьмое'
oye_ord_dict['девять']='девятое'
oye_ord_dict['десять']='десятое'
oye_ord_dict['одиннадцать']='одиннадцатое'
oye_ord_dict['двенадцать']='двенадцатое'
oye_ord_dict['тринадцать']='тринадцатое'
oye_ord_dict['четырнадцать']='четырнадцатое'
oye_ord_dict['пятнадцать']='пятнадцатое'
oye_ord_dict['шестнадцать']='шестнадцатое'
oye_ord_dict['семнадцать']='семнадцатое'
oye_ord_dict['восемнадцать']='восемнадцатое'
oye_ord_dict['девятнадцать']='девятнадцатое'
oye_ord_dict['двадцать']='двадцатое'
oye_ord_dict['тридцать']='тридцатое'
oye_ord_dict['сорок']='сороковое'
oye_ord_dict['пятьдесят']='пятидесятое'
oye_ord_dict['шестьдесят']='шестидесятое'
oye_ord_dict['семьдесят']='семидесятое'
oye_ord_dict['восемьдесят']='восьмидесятое'
oye_ord_dict['девяносто']='девяностое'
oye_ord_dict['сто']='сотое'
oye_ord_dict['двести']='двухсотое'
oye_ord_dict['триста']='трехсотое'
oye_ord_dict['четыреста']='четырехсотое'
oye_ord_dict['пятьсот']='пятисотое'
oye_ord_dict['шестьсот']='шестисотое'
oye_ord_dict['семьсот']='семисотое'
oye_ord_dict['восемьсот']='восьмисотое'
oye_ord_dict['девятьсот']='девятисотое'
oye_ord_dict['девятсот']='девятисотое'
oye_ord_dict['тысяча']='тысячное'
oye_ord_dict['тысячи']='тысячное'
oye_ord_dict['тысяч']='тысячное'
oye_ord_dict['миллион'] = 'миллионное'
oye_ord_dict['миллиона'] = 'миллионное'
oye_ord_dict['миллионов'] ='миллионное'

oy_ord_dict={}
oy_ord_dict['ноль']='нулевое'
oy_ord_dict['один']='первой'
oy_ord_dict['два']='второй'
oy_ord_dict['две']='второй'
oy_ord_dict['три']='третьей'
oy_ord_dict['четыре']='четвертой'
oy_ord_dict['одна']='первой'
oy_ord_dict['одно']='первой'
oy_ord_dict['одни']='первой'
oy_ord_dict['пять']='пятой'
oy_ord_dict['шесть']='шестой'
oy_ord_dict['семь']='седьмой'
oy_ord_dict['восемь']='восьмой'
oy_ord_dict['девять']='девятой'
oy_ord_dict['десять']='десятой'
oy_ord_dict['одиннадцать']='одиннадцатой'
oy_ord_dict['двенадцать']='двенадцатой'
oy_ord_dict['тринадцать']='тринадцатой'
oy_ord_dict['четырнадцать']='четырнадцатой'
oy_ord_dict['пятнадцать']='пятнадцатой'
oy_ord_dict['шестнадцать']='шестнадцатой'
oy_ord_dict['семнадцать']='семнадцатой'
oy_ord_dict['восемнадцать']='восемнадцатой'
oy_ord_dict['девятнадцать']='девятнадцатой'
oy_ord_dict['двадцать']='двадцатой'
oy_ord_dict['тридцать']='тридцатой'
oy_ord_dict['сорок']='сороковой'
oy_ord_dict['пятьдесят']='пятидесятой'
oy_ord_dict['шестьдесят']='шестидесятой'
oy_ord_dict['семьдесят']='семидесятой'
oy_ord_dict['восемьдесят']='восьмидесятой'
oy_ord_dict['девяносто']='девяностой'
oy_ord_dict['сто']='сотой'
oy_ord_dict['двести']='двухсотой'
oy_ord_dict['триста']='трехсотой'
oy_ord_dict['четыреста']='четырехсотой'
oy_ord_dict['пятьсот']='пятисотой'
oy_ord_dict['шестьсот']='шестисотой'
oy_ord_dict['семьсот']='семисотой'
oy_ord_dict['восемьсот']='восьмисотой'
oy_ord_dict['девятьсот']='девятисотой'
oy_ord_dict['девятсот']='девятисотой'
oy_ord_dict['тысяча']='тысячной'
oy_ord_dict['тысячи']='тысячной'
oy_ord_dict['тысяч']='тысячной'
oy_ord_dict['миллион'] = 'миллионной'
oy_ord_dict['миллиона'] = 'миллионной'
oy_ord_dict['миллионов'] ='миллионной'

ukh_ord_dict={}
ukh_ord_dict['ноль']='нулевых'
ukh_ord_dict['один']='первых'
ukh_ord_dict['два']='вторых'
ukh_ord_dict['две']='вторых'
ukh_ord_dict['три']='третьих'
ukh_ord_dict['четыре']='четвертых'
ukh_ord_dict['одна']='первых'
ukh_ord_dict['одно']='первых'
ukh_ord_dict['одни']='первых'
ukh_ord_dict['пять']='пятых'
ukh_ord_dict['шесть']='шестых'
ukh_ord_dict['семь']='седьмых'
ukh_ord_dict['восемь']='восьмых'
ukh_ord_dict['девять']='девятых'
ukh_ord_dict['десять']='десятых'
ukh_ord_dict['одиннадцать']='одиннадцатых'
ukh_ord_dict['двенадцать']='двенадцатых'
ukh_ord_dict['тринадцать']='тринадцатых'
ukh_ord_dict['четырнадцать']='четырнадцатых'
ukh_ord_dict['пятнадцать']='пятнадцатых'
ukh_ord_dict['шестнадцать']='шестнадцатых'
ukh_ord_dict['семнадцать']='семнадцатых'
ukh_ord_dict['восемнадцать']='восемнадцатых'
ukh_ord_dict['девятнадцать']='девятнадцатых'
ukh_ord_dict['двадцать']='двадцатых'
ukh_ord_dict['тридцать']='тридцатых'
ukh_ord_dict['сорок']='сороковых'
ukh_ord_dict['пятьдесят']='пятидесятых'
ukh_ord_dict['шестьдесят']='шестидесятых'
ukh_ord_dict['семьдесят']='семидесятых'
ukh_ord_dict['восемьдесят']='восьмидесятых'
ukh_ord_dict['девяносто']='девяностых'
ukh_ord_dict['сто']='сотых'
ukh_ord_dict['двести']='двухсотых'
ukh_ord_dict['триста']='трехсотых'
ukh_ord_dict['четыреста']='четырехсотых'
ukh_ord_dict['пятьсот']='пятисотых'
ukh_ord_dict['шестьсот']='шестисотых'
ukh_ord_dict['семьсот']='семисотых'
ukh_ord_dict['восемьсот']='восьмисотых'
ukh_ord_dict['девятьсот']='девятисотых'
ukh_ord_dict['девятсот']='девятисотых'
ukh_ord_dict['тысяча']='тысячных'
ukh_ord_dict['тысячи']='тысячных'
ukh_ord_dict['тысяч']='тысячных'
ukh_ord_dict['миллион'] = 'миллионных'
ukh_ord_dict['миллиона'] = 'миллионных'
ukh_ord_dict['миллионов'] ='миллионных'

aya_ord_dict={}
aya_ord_dict['ноль']='нулевая'
aya_ord_dict['один']='первая'
aya_ord_dict['два']='вторая'
aya_ord_dict['две']='вторая'
aya_ord_dict['три']='третья'
aya_ord_dict['четыре']='четвертая'
aya_ord_dict['одна']='первая'
aya_ord_dict['одно']='первая'
aya_ord_dict['одни']='первая'
aya_ord_dict['пять']='пятая'
aya_ord_dict['шесть']='шестая'
aya_ord_dict['семь']='седьмая'
aya_ord_dict['восемь']='восьмая'
aya_ord_dict['девять']='девятая'
aya_ord_dict['десять']='десятая'
aya_ord_dict['одиннадцать']='одиннадцатая'
aya_ord_dict['двенадцать']='двенадцатая'
aya_ord_dict['тринадцать']='тринадцатая'
aya_ord_dict['четырнадцать']='четырнадцатая'
aya_ord_dict['пятнадцать']='пятнадцатая'
aya_ord_dict['шестнадцать']='шестнадцатая'
aya_ord_dict['семнадцать']='семнадцатая'
aya_ord_dict['восемнадцать']='восемнадцатая'
aya_ord_dict['девятнадцать']='девятнадцатая'
aya_ord_dict['двадцать']='двадцатая'
aya_ord_dict['тридцать']='тридцатая'
aya_ord_dict['сорок']='сороковая'
aya_ord_dict['пятьдесят']='пятидесятая'
aya_ord_dict['шестьдесят']='шестидесятая'
aya_ord_dict['семьдесят']='семидесятая'
aya_ord_dict['восемьдесят']='восьмидесятая'
aya_ord_dict['девяносто']='девяностая'
aya_ord_dict['сто']='сотая'
aya_ord_dict['двести']='двухсотая'
aya_ord_dict['триста']='трехсотая'
aya_ord_dict['четыреста']='четырехсотая'
aya_ord_dict['пятьсот']='пятисотая'
aya_ord_dict['шестьсот']='шестисотая'
aya_ord_dict['семьсот']='семисотая'
aya_ord_dict['восемьсот']='восьмисотая'
aya_ord_dict['девятьсот']='девятисотая'
aya_ord_dict['девятсот']='девятисотая'
aya_ord_dict['тысяча']='тысячная'
aya_ord_dict['тысячи']='тысячная'
aya_ord_dict['тысяч']='тысячная'
aya_ord_dict['миллион'] = 'миллионная'
aya_ord_dict['миллиона'] = 'миллионная'
aya_ord_dict['миллионов'] ='миллионная'
#gen_dict[]=''
def geni(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=gen_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
  
  splitted=' '.join(splitted)
  if str(number)=='2000':
    splitted = 'двух тысячного'
 
  return splitted  
def insti(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=inst_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
  splitted=' '.join(splitted)
  
  if str(number)=='2000':
    splitted = 'двумя тысячами'

  return splitted  
def acci(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=acc_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
  splitted=' '.join(splitted)
  if str(number)=='2000':
    splitted = 'двух тысячный'
 
  return splitted 
def nomi(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=oye_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
    
  splitted=' '.join(splitted)
  if str(number)=='2000':
    splitted = 'двух тысячный'
   
  return splitted 
def prepi(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=prep_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
  splitted=' '.join(splitted)
  if str(number)=='2000':
    splitted = 'двух тысячном'
  
  return splitted 
def dati(number):
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  splitted=a.split()
  splitted[-1]=dat_ord_dict[splitted[-1]]
  length_of_number = len(str(number))
  if length_of_number==4 and str(number)[0]=='1':
    splitted = splitted[1:]
  splitted=' '.join(splitted)
  if str(number)=='2000':
    splitted = 'двух тысячному'
  
  return splitted 
def genitivise(number):
  
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  if number =='²':
    line = 'второго'
    return line
  if number == '³':
    line = 'третьего'
    return line 
  elif float(number) ==2000.0:
    line = 'двух тысячного'
    return line
  
  else:
    
    a=num2words(int(number), lang='ru')
    
    
    how_many_words=len(a.split())
    length_of_number = len(str(number))
    splitted=a.split()
   
    #b = splitted[:-2]
    
    
    
    if length_of_number==455:
      splitted[-1]=gen_ord_dict[splitted[-1]]
      #splitted.append('года')
      if str(number)[0]=='1':
        splitted = splitted[1:]
        
    else:
      ii=0
      while ii < how_many_words:
        splitted[ii]=gen_car_dict[splitted[ii]]
        ii+=1
        #splitted[-1]=gen_car_dict[splitted[-1]]
        #print(splitted)
    
    if 'ноль' in splitted:
      print('YO WHAT THE FUCK')
    if len(number)==4 and str(number)[0]=='1':
      splitted=splitted[1:]
    splitted=' '.join(splitted)
    if length_of_number>5 and str(number)[-4]=='1':
      splitted.replace('одного тысячи','одной тысячи')
    
    
    #print(splitted)
    return splitted
  
def instrumentalise(number):
  #print('genitivising!')
  
  #print(number)
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  if number =='²':
    line = 'второго'
    return line
  if number == '³':
    line = 'третьего'
    return line 
  elif int(number) ==2000:
    line = 'двух тысячного'
    return line
  
  else:
    
    a=num2words(int(number), lang='ru')
    #print(a)
    how_many_words=len(a.split())
    length_of_number = len(str(number))
    splitted=a.split()
   
    #b = splitted[:-2]
    #print(b)
    
    if length_of_number==455:
      splitted[-1]=inst_ord_dict[splitted[-1]]
      #splitted.append('года')
      if str(number)[0]=='1':
        splitted = splitted[1:]
        
    else:
      ii=0
      while ii < how_many_words:
        splitted[ii]=inst_car_dict[splitted[ii]]
        ii+=1
        #splitted[-1]=gen_car_dict[splitted[-1]]
        #print(splitted)
    if 'ноль' in splitted:
      print('YO WHAT THE FUCK')
    if len(number)==4 and str(number)[0]=='1':
      splitted=splitted[1:]
    splitted=' '.join(splitted)
    
    #print(splitted)
    return splitted

def prepositionalise(number):
  #print('prepositionalising')
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  a=num2words(int(number), lang='ru')
  #print(a)
  how_many_words=len(a.split())
  length_of_number = len(str(number))
  splitted=a.split()
  if length_of_number==455:
      splitted=a.split()
      splitted[-1]=prep_ord_dict[splitted[-1]]
      #splitted.append('года')
      if str(number)[0]=='1':
        splitted = splitted[1:]
        
  else:
    
    
    ii=0
    while ii < how_many_words:
        splitted[ii]=prep_car_dict[splitted[ii]]
        ii+=1
        #splitted[-1]=gen_car_dict[splitted[-1]]
        #print(splitted)
  if 'ноль' in splitted:
    splitted = a.split()
  if len(number)==4 and str(number)[0]=='1':
      splitted = splitted[1:]
  splitted=' '.join(splitted)  
  #print(splitted)
  return splitted
def accusativise(number):
  #print('genitivising!')
  
  #print(number)
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  #if number =='²':
  #  line = 'второго'
  #  return line
  #if number == '³':
  #  line = 'третьего'
  #  return line 
  #elif float(number) ==2000.0:
  #  line = 'двухтысячного'
  #  return line
  
  #else:
    
  a=num2words(int(number), lang='ru')
  #print(a)
  how_many_words=len(a.split())
  length_of_number = len(str(number))
  splitted=a.split()
   
    #b = splitted[:-2]
    #print(b)
    
  if length_of_number==455:
    splitted[-1]=acc_ord_dict[splitted[-1]]
    #splitted.append('года')
    if str(number)[0]=='1':
      splitted = splitted[1:]
  #else: print('')
  #else:
    #splitted[-1]=acc_dict[splitted[-1]]
  #if 'ноль' in splitted:
  #  print('YO WHAT THE FUCK')
  if len(number)==4 and str(number)[0]=='1':
      splitted=splitted[1:]
  elif len(number)==7 and str(number)[0]=='1':
      splitted=splitted[1:]
  splitted=' '.join(splitted)
    

  return splitted
def dativise(number):
  #print('genitivising!')
  
  #print(number)
  number = number.translate(SUB)
  number = number.translate(SUP)
  number= number.translate(OTH)
  if number =='²':
    line = 'второго'
    return line
  if number == '³':
    line = 'третьего'
    return line 
  elif int(number) ==2000:
    line = 'двух тысячного'
    return line
  
  else:
    
    a=num2words(int(number), lang='ru')
    #print(a)
    how_many_words=len(a.split())
    length_of_number = len(str(number))
    splitted=a.split()
   
    #b = splitted[:-2]
    #print(b)
    
    if length_of_number==455:
      splitted[-1]=dat_ord_dict[splitted[-1]]
      #splitted.append('года')
      if str(number)[0]=='1':
        splitted = splitted[1:]
        
    else:
      ii=0
      while ii < how_many_words:
        splitted[ii]=dat_car_dict[splitted[ii]]
        ii+=1
        #splitted[-1]=gen_car_dict[splitted[-1]]
        #print(splitted)
    if 'ноль' in splitted:
      print('YO WHAT THE FUCK')
    if len(number)==4 and str(number)[0]=='1':
      splitted = splitted[1:]
    splitted=' '.join(splitted)
    
    #print(splitted)
    return splitted


def telephonise(string_of_numbers):
  #print('begin tele')
  #print(string_of_numbers)
  hyphen_count = string_of_numbers.count('-')
  #print(hyphen_count)
  first_hyphen_place=string_of_numbers.find('-')
  second_hyphen_place = string_of_numbers.find('-', first_hyphen_place+1)
  third_hyphen_place = string_of_numbers.find('-', second_hyphen_place+1)
  fourth_hyphen_place = string_of_numbers.find('-', third_hyphen_place+1)
  fifth_hyphen_place = string_of_numbers.find('-', fourth_hyphen_place+1)
  sixth_hyphen_place = string_of_numbers.find('-', fifth_hyphen_place+1)
  if hyphen_count==1:
    second_hyphen_place=len(string_of_numbers)
    first_string = string_of_numbers[:first_hyphen_place]
    second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
  elif hyphen_count==2:
    third_hyphen_place=len(string_of_numbers)
    first_string = string_of_numbers[:first_hyphen_place]
    second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
    third_string = string_of_numbers[second_hyphen_place+1:third_hyphen_place]
    
  elif hyphen_count==3:
    fourth_hyphen_place=len(string_of_numbers)
    first_string = string_of_numbers[:first_hyphen_place]
    second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
    third_string = string_of_numbers[second_hyphen_place+1:third_hyphen_place]
    fourth_string= string_of_numbers[third_hyphen_place+1:fourth_hyphen_place]
  elif hyphen_count==4:
    fifth_hyphen_place=len(string_of_numbers)
    first_string = string_of_numbers[:first_hyphen_place]
    second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
    third_string = string_of_numbers[second_hyphen_place+1:third_hyphen_place]
    fourth_string= string_of_numbers[third_hyphen_place+1:fourth_hyphen_place]
    fifth_string=string_of_numbers[fourth_hyphen_place+1:fifth_hyphen_place]
  elif hyphen_count==5:
    sixth_hyphen_place=len(string_of_numbers)
    first_string = string_of_numbers[:first_hyphen_place]
    second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
    third_string = string_of_numbers[second_hyphen_place+1:third_hyphen_place]
    fourth_string= string_of_numbers[third_hyphen_place+1:fourth_hyphen_place]
    fifth_string=string_of_numbers[fourth_hyphen_place+1:fifth_hyphen_place]
    sixth_string = string_of_numbers[fifth_hyphen_place+1:sixth_hyphen_place]
  else:
    print('BIG HYPHEN LOL')
    first_string = '0'
    second_string = '0'
    third_string = '0'
    fourth_string= '0'
    fifth_string='0'
    sixth_string ='0'
  
  ##FIRST STRING##
  if len(first_string)==1:
    first_string=num2words(int(first_string), lang='ru')
  elif len(first_string)==2:
    if first_string[0]=='0':
      first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')
    else:
      first_string = num2words(int(first_string), lang='ru')
  elif len(first_string)==3:
    if first_string[0]=='0':
      if first_string[1]=='0':
        first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')
      else:
        first_string = num2words(int(first_string[0]), lang='ru') +' ' + num2words(int(first_string[1:]), lang='ru')
    elif first_string[1]=='0':
      if first_string[2]=='0':
        first_string = num2words(int(first_string), lang='ru')
      else:
        first_string = num2words(int(first_string), lang='ru')
    else:
      first_string = num2words(int(first_string), lang='ru')
  elif len(first_string)==4:
    if first_string[0]=='0':
      if first_string[1]=='0':
        if first_string[2]=='0':
          first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')+ ' '+num2words(int(first_string[3]), lang='ru')
        else:
          first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2:]), lang='ru')
      else:
       first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1:]), lang='ru')
    else:
      first_string = num2words(int(first_string[:2]), lang='ru') + ' '+num2words(int(first_string[2:]), lang='ru')
  else:
    print('long string!')
  #print(first_string)
 ##SECOND STRING##
  if len(second_string)==1:
    second_string=num2words(int(second_string), lang='ru')
  elif len(second_string)==2:
    if second_string[0]=='0':
      second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')
    else:
      second_string = num2words(int(second_string), lang='ru')
  elif len(second_string)==3:
    if second_string[0]=='0':
      if second_string[1]=='0':
        second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')
      else:
        second_string = num2words(int(second_string[0]), lang='ru') +' ' + num2words(int(second_string[1:]), lang='ru')
    elif second_string[1]=='0':
      if second_string[2]=='0':
        second_string = num2words(int(second_string), lang='ru')
      else:
        second_string = num2words(int(second_string), lang='ru')
    else:
      second_string = num2words(int(second_string), lang='ru')
  elif len(second_string)==4:
    if second_string[0]=='0':
      if second_string[1]=='0':
        if second_string[2]=='0':
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')
        else:
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
      else:
       second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:]), lang='ru')
    else:
      second_string = num2words(int(second_string[:2]), lang='ru') + ' '+num2words(int(second_string[2:]), lang='ru')
  elif len(second_string)==5:
    if second_string[0]=='0':
      if second_string[1]=='0':
        if second_string[2]=='0':
          if second_string[3]=='0':
            second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')
          else:
            second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3:]), lang='ru')
        else:
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
      else:
        second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:3]), lang='ru')+ ' ' + num2words(int(second_string[3:]), lang='ru')
    else:
      if second_string[3]=='0':
        second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
      else:
        second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3:]), lang='ru')
  elif len(second_string)==6:
    if second_string[0]=='0':
      if second_string[1]=='0':
        if second_string[2]=='0':
          if second_string[3]=='0':
            if second_string[4]=='0':
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')+ ' ' + num2words(int(second_string[5]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')+ ' '+num2words(int(second_string[4:]), lang='ru')
          else:
            second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3:]), lang='ru')
        else:
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:4]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
      else:
        if second_string[4]=='0':
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:4]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')+ ' ' + num2words(int(second_string[5:]), lang='ru')
        else:
          second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:4]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
    else:
      if second_string[3]=='0':
        if second_string[4]=='0':
          second_string = num2words(int(second_string[0:3]), lang='ru') + ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')+ ' ' + num2words(int(second_string[5:]), lang='ru')
        else:
          second_string = num2words(int(second_string[0:3]), lang='ru') + ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
      else:
        second_string = num2words(int(second_string[0:3]), lang='ru') + ' '+num2words(int(second_string[3:]), lang='ru')
          
  else:
    print('long second string')
  #print(second_string)

 ##THIRD STRING##
  if len(third_string)==1:
    third_string=num2words(int(third_string), lang='ru')
  elif len(third_string)==2:
    if third_string[0]=='0':
      third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')
    else:
      third_string = num2words(int(third_string), lang='ru')
  elif len(third_string)==3:
    if third_string[0]=='0':
      if third_string[1]=='0':
        third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')
      else:
        third_string = num2words(int(third_string[0]), lang='ru') +' ' + num2words(int(third_string[1:]), lang='ru')
    elif third_string[1]=='0':
      if third_string[2]=='0':
        third_string = num2words(int(third_string), lang='ru')
      else:
        third_string = num2words(int(third_string), lang='ru')
    else:
      third_string = num2words(int(third_string), lang='ru')
  elif len(third_string)==4:
    if third_string[0]=='0':
      if third_string[1]=='0':
        if third_string[2]=='0':
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3]), lang='ru')
        else:
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2:]), lang='ru')
      else:
       third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1:]), lang='ru')
    else:
      third_string = num2words(int(third_string[:2]), lang='ru') + ' '+num2words(int(third_string[2:]), lang='ru')
  elif len(third_string)==5:
    if third_string[0]=='0':
      if third_string[1]=='0':
        if third_string[2]=='0':
          if third_string[3]=='0':
            third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3]), lang='ru')+ ' ' + num2words(int(third_string[4]), lang='ru')
          else:
            third_tring = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3:]), lang='ru')
        else:
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2:]), lang='ru')
      else:
        third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1:3]), lang='ru')+ ' ' + num2words(int(third_string[3:]), lang='ru')
    else:
      if third_string[3]=='0':
        third_string = num2words(int(third_string[:3]), lang='ru') + ' '+num2words(int(third_string[3]), lang='ru')+ ' ' + num2words(int(third_string[4:]), lang='ru')
      else:
        third_string = num2words(int(third_string[:3]), lang='ru') + ' '+num2words(int(third_string[3:]), lang='ru')
  

  elif len(third_string)==6:
    if third_string[0]=='0':
      if third_string[1]=='0':
        if third_string[2]=='0':
          if third_string[3]=='0':
            if third_string[4]=='0':
              third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3]), lang='ru')+ ' ' + num2words(int(third_string[4]), lang='ru')+ ' ' + num2words(int(third_string[5]), lang='ru')
            else:
              third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3]), lang='ru')+ ' '+num2words(int(third_string[4:]), lang='ru')
          else:
            third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2]), lang='ru')+ ' '+num2words(int(third_string[3:]), lang='ru')
        else:
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1]), lang='ru')+ ' ' + num2words(int(third_string[2:4]), lang='ru')+ ' ' + num2words(int(third_string[4:]), lang='ru')
      else:
        if third_string[4]=='0':
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1:4]), lang='ru')+ ' ' + num2words(int(third_string[4]), lang='ru')+ ' ' + num2words(int(third_string[5:]), lang='ru')
        else:
          third_string = num2words(int(third_string[0]), lang='ru') + ' '+num2words(int(third_string[1:4]), lang='ru')+ ' ' + num2words(int(third_string[4:]), lang='ru')
    else:
      if third_string[3]=='0':
        if third_string[4]=='0':
          third_string = num2words(int(third_string[0:3]), lang='ru') + ' '+num2words(int(third_string[3]), lang='ru')+ ' ' + num2words(int(third_string[4]), lang='ru')+ ' ' + num2words(int(third_string[5:]), lang='ru')
        else:
          third_string = num2words(int(third_string[0:3]), lang='ru') + ' '+num2words(int(third_string[3]), lang='ru')+ ' ' + num2words(int(third_string[4:]), lang='ru')
      else:
        third_string = num2words(int(third_string[0:3]), lang='ru') + ' '+num2words(int(third_string[3:]), lang='ru')
  else:
    print('long third string')
  #print(third_string)
  if hyphen_count>=3:
    if len(fourth_string)==1:
      fourth_string=num2words(int(fourth_string), lang='ru')
    elif len(fourth_string)==2:
      if fourth_string[0]=='0':
        fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')
      else:
        fourth_string = num2words(int(fourth_string), lang='ru')
    elif len(fourth_string)==3:
      if fourth_string[0]=='0':
        if fourth_string[1]=='0':
          fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')
        else:
          fourth_string = num2words(int(fourth_string[0]), lang='ru') +' ' + num2words(int(fourth_string[1:]), lang='ru')
      elif fourth_string[1]=='0':
        if fourth_string[2]=='0':
          fourth_string = num2words(int(fourth_string), lang='ru')
        else:
          fourth_string = num2words(int(fourth_string),lang='ru')
      else:
        fourth_string = num2words(int(fourth_string), lang='ru')
    elif len(fourth_string)==4:
      if fourth_string[0]=='0':
        if fourth_string[1]=='0':
          if fourth_string[2]=='0':
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3]), lang='ru')
          else:
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2:]), lang='ru')
        else:
         fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1:]), lang='ru')
      else:
        fourth_string = num2words(int(fourth_string[:2]), lang='ru') + ' '+num2words(int(fourth_string[2:]), lang='ru')
    elif len(fourth_string)==5:
      if fourth_string[0]=='0':
        if fourth_string[1]=='0':
          if fourth_string[2]=='0':
            if fourth_string[3]=='0':
              fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3]), lang='ru')+ ' ' + num2words(int(fourth_string[4]), lang='ru')
            else:
              fourth_tring = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3:]), lang='ru')
          else:
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2:]), lang='ru')
        else:
          fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1:3]), lang='ru')+ ' ' + num2words(int(fourth_string[3:]), lang='ru')
      else:
        if fourth_string[3]=='0':
          fourth_string = num2words(int(fourth_string[:3]), lang='ru') + ' '+num2words(int(fourth_string[3]), lang='ru')+ ' ' + num2words(int(fourth_string[4:]), lang='ru')
        else:
          fourth_string = num2words(int(fourth_string[:3]), lang='ru') + ' '+num2words(int(fourth_string[3:]), lang='ru')
  

    elif len(fourth_string)==6:
      if fourth_string[0]=='0':
        if fourth_string[1]=='0':
          if fourth_string[2]=='0':
            if fourth_string[3]=='0':
              if fourth_string[4]=='0':
                fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3]), lang='ru')+ ' ' + num2words(int(fourth_string[4]), lang='ru')+ ' ' + num2words(int(fourth_string[5]), lang='ru')
              else:
                fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3]), lang='ru')+ ' '+num2words(int(fourth_string[4:]), lang='ru')
            else:
              fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2]), lang='ru')+ ' '+num2words(int(fourth_string[3:]), lang='ru')
          else:
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1]), lang='ru')+ ' ' + num2words(int(fourth_string[2:4]), lang='ru')+ ' ' + num2words(int(fourth_string[4:]), lang='ru')
        else:
          if fourth_string[4]=='0':
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1:4]), lang='ru')+ ' ' + num2words(int(fourth_string[4]), lang='ru')+ ' ' + num2words(int(fourth_string[5:]), lang='ru')
          else:
            fourth_string = num2words(int(fourth_string[0]), lang='ru') + ' '+num2words(int(fourth_string[1:4]), lang='ru')+ ' ' + num2words(int(fourth_string[4:]), lang='ru')
      else:
        if fourth_string[3]=='0':
          if fourth_string[4]=='0':
            fourth_string = num2words(int(fourth_string[0:3]), lang='ru') + ' '+num2words(int(fourth_string[3]), lang='ru')+ ' ' + num2words(int(fourth_string[4]), lang='ru')+ ' ' + num2words(int(fourth_string[5:]), lang='ru')
          else:
            fourth_string = num2words(int(fourth_string[0:3]), lang='ru') + ' '+num2words(int(fourth_string[3]), lang='ru')+ ' ' + num2words(int(fourth_string[4:]), lang='ru')
        else:
          fourth_string = num2words(int(fourth_string[0:3]), lang='ru') + ' '+num2words(int(fourth_string[3:]), lang='ru')
    else:
      print('long fourth string')
    #print(fourth_string)
  if hyphen_count>=4:
    if len(fifth_string)==1:
      fifth_string=num2words(int(fifth_string), lang='ru')
    elif len(fifth_string)==2:
      if fifth_string[0]=='0':
        fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')
      else:
        fifth_string = num2words(int(fifth_string), lang='ru')
    elif len(fifth_string)==3:
      if fifth_string[0]=='0':
        if fifth_string[1]=='0':
          fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')
        else:
          fifth_string = num2words(int(fifth_string[0]), lang='ru') +' ' + num2words(int(fifth_string[1:]), lang='ru')
      elif fifth_string[1]=='0':
        if fifth_string[2]=='0':
          fifth_string = num2words(int(fifth_string), lang='ru')
        else:
          fifth_string = num2words(int(fifth_string), lang='ru')
      else:
        fifth_string = num2words(int(fifth_string), lang='ru')
    elif len(fifth_string)==4:
      if fifth_string[0]=='0':
        if fifth_string[1]=='0':
          if fifth_string[2]=='0':
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3]), lang='ru')
          else:
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2:]), lang='ru')
        else:
         fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1:]), lang='ru')
      else:
        fifth_string = num2words(int(fifth_string[:2]), lang='ru') + ' '+num2words(int(fifth_string[2:]), lang='ru')
    elif len(fifth_string)==5:
      if fifth_string[0]=='0':
        if fifth_string[1]=='0':
          if fifth_string[2]=='0':
            if fifth_string[3]=='0':
              fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3]), lang='ru')+ ' ' + num2words(int(fifth_string[4]), lang='ru')
            else:
              fifth_tring = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3:]), lang='ru')
          else:
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2:]), lang='ru')
        else:
          fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1:3]), lang='ru')+ ' ' + num2words(int(fifth_string[3:]), lang='ru')
      else:
        if fifth_string[3]=='0':
          fifth_string = num2words(int(fifth_string[:3]), lang='ru') + ' '+num2words(int(fifth_string[3]), lang='ru')+ ' ' + num2words(int(fifth_string[4:]), lang='ru')
        else:
          fifth_string = num2words(int(fifth_string[:3]), lang='ru') + ' '+num2words(int(fifth_string[3:]), lang='ru')
  

    elif len(fifth_string)==6:
      if fifth_string[0]=='0':
        if fifth_string[1]=='0':
          if fifth_string[2]=='0':
            if fifth_string[3]=='0':
              if fifth_string[4]=='0':
                fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3]), lang='ru')+ ' ' + num2words(int(fifth_string[4]), lang='ru')+ ' ' + num2words(int(fifth_string[5]), lang='ru')
              else:
                fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3]), lang='ru')+ ' '+num2words(int(fifth_string[4:]), lang='ru')
            else:
              fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2]), lang='ru')+ ' '+num2words(int(fifth_string[3:]), lang='ru')
          else:
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1]), lang='ru')+ ' ' + num2words(int(fifth_string[2:4]), lang='ru')+ ' ' + num2words(int(fifth_string[4:]), lang='ru')
        else:
          if fifth_string[4]=='0':
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1:4]), lang='ru')+ ' ' + num2words(int(fifth_string[4]), lang='ru')+ ' ' + num2words(int(fifth_string[5:]), lang='ru')
          else:
            fifth_string = num2words(int(fifth_string[0]), lang='ru') + ' '+num2words(int(fifth_string[1:4]), lang='ru')+ ' ' + num2words(int(fifth_string[4:]), lang='ru')
      else:
        if fifth_string[3]=='0':
          if fifth_string[4]=='0':
            fifth_string = num2words(int(fifth_string[0:3]), lang='ru') + ' '+num2words(int(fifth_string[3]), lang='ru')+ ' ' + num2words(int(fifth_string[4]), lang='ru')+ ' ' + num2words(int(fifth_string[5:]), lang='ru')
          else:
            fifth_string = num2words(int(fifth_string[0:3]), lang='ru') + ' '+num2words(int(fifth_string[3]), lang='ru')+ ' ' + num2words(int(fifth_string[4:]), lang='ru')
        else:
          fifth_string = num2words(int(fifth_string[0:3]), lang='ru') + ' '+num2words(int(fifth_string[3:]), lang='ru')
    else:
      print('long fourth string')
    #print(fifth_string)
  if hyphen_count>=5:
    if len(sixth_string)==1:
      sixth_string=num2words(int(sixth_string), lang='ru')
    elif len(sixth_string)==2:
      if sixth_string[0]=='0':
        sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')
      else:
        sixth_string = num2words(int(sixth_string), lang='ru')
    elif len(sixth_string)==3:
      if sixth_string[0]=='0':
        if sixth_string[1]=='0':
          sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')
        else:
          sixth_string = num2words(int(sixth_string[0]), lang='ru') +' ' + num2words(int(sixth_string[1:]), lang='ru')
      elif sixth_string[1]=='0':
        if sixth_string[2]=='0':
          sixth_string = num2words(int(sixth_string), lang='ru')
        else:
          sixth_string = num2words(int(sixth_string), lang='ru')
      else:
        sixth_string = num2words(int(sixth_string), lang='ru')
    elif len(sixth_string)==4:
      if sixth_string[0]=='0':
        if sixth_string[1]=='0':
          if sixth_string[2]=='0':
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3]), lang='ru')
          else:
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2:]), lang='ru')
        else:
         sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1:]), lang='ru')
      else:
        sixth_string = num2words(int(sixth_string[:3]), lang='ru') + ' '+num2words(int(sixth_string[3]), lang='ru')
    elif len(sixth_string)==5:
      if sixth_string[0]=='0':
        if sixth_string[1]=='0':
          if sixth_string[2]=='0':
            if sixth_string[3]=='0':
              sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3]), lang='ru')+ ' ' + num2words(int(sixth_string[4]), lang='ru')
            else:
              sixth_tring = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3:]), lang='ru')
          else:
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2:]), lang='ru')
        else:
          sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1:4]), lang='ru')+ ' ' + num2words(int(sixth_string[4:]), lang='ru')
      else:
        if sixth_string[3]=='0':
          sixth_string = num2words(int(sixth_string[:3]), lang='ru') + ' '+num2words(int(sixth_string[3]), lang='ru')+ ' ' + num2words(int(sixth_string[4:]), lang='ru')
        else:
          sixth_string = num2words(int(sixth_string[:3]), lang='ru') + ' '+num2words(int(sixth_string[3:]), lang='ru')
  

    elif len(sixth_string)==6:
      if sixth_string[0]=='0':
        if sixth_string[1]=='0':
          if sixth_string[2]=='0':
            if sixth_string[3]=='0':
              if sixth_string[4]=='0':
                sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3]), lang='ru')+ ' ' + num2words(int(sixth_string[4]), lang='ru')+ ' ' + num2words(int(sixth_string[5]), lang='ru')
              else:
                sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3]), lang='ru')+ ' '+num2words(int(sixth_string[4:]), lang='ru')
            else:
              sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2]), lang='ru')+ ' '+num2words(int(sixth_string[3:]), lang='ru')
          else:
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1]), lang='ru')+ ' ' + num2words(int(sixth_string[2:4]), lang='ru')+ ' ' + num2words(int(sixth_string[4:]), lang='ru')
        else:
          if sixth_string[4]=='0':
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1:4]), lang='ru')+ ' ' + num2words(int(sixth_string[4]), lang='ru')+ ' ' + num2words(int(sixth_string[5:]), lang='ru')
          else:
            sixth_string = num2words(int(sixth_string[0]), lang='ru') + ' '+num2words(int(sixth_string[1:4]), lang='ru')+ ' ' + num2words(int(sixth_string[4:]), lang='ru')
      else:
        if sixth_string[3]=='0':
          if sixth_string[4]=='0':
            sixth_string = num2words(int(sixth_string[0:3]), lang='ru') + ' '+num2words(int(sixth_string[3]), lang='ru')+ ' ' + num2words(int(sixth_string[4]), lang='ru')+ ' ' + num2words(int(sixth_string[5:]), lang='ru')
          else:
            sixth_string = num2words(int(sixth_string[0:3]), lang='ru') + ' '+num2words(int(sixth_string[3]), lang='ru')+ ' ' + num2words(int(sixth_string[4:]), lang='ru')
        else:
          sixth_string = num2words(int(sixth_string[0:3]), lang='ru') + ' '+num2words(int(sixth_string[3:]), lang='ru')
    else:
      print('long fourth string')
    #print(sixth_string)
  s = 'sil'
  if hyphen_count==1:
    a = first_string + ' ' + s + ' '  + second_string
    
  elif hyphen_count==2:
    a = first_string + ' ' + s + ' ' + second_string + ' ' + s + ' ' + third_string
    
  elif hyphen_count==3:
    a = first_string+' ' + s + ' '  + second_string + ' ' + s + ' ' + third_string + ' ' + s + ' '+ fourth_string 
  
  elif hyphen_count==4:
    a = first_string+' ' + s + ' ' +second_string + ' ' + s + ' ' +third_string + ' ' + s + ' ' +fourth_string + ' ' + s + ' ' + fifth_string
    
  elif hyphen_count==5:
    a = first_string+' ' + s + ' ' + second_string+ ' ' + s + ' ' + third_string + ' ' + s + ' ' + fourth_string + ' ' + s + ' ' + fifth_string + ' ' + s + ' ' + sixth_string
  else:
    a = '000000000000000000'
 #print(a)
  return a

def website(string):
  end = 'точка к_trans о_trans м_trans'
  line=0
  if string in res:
    srtd = sorted(res[string].items(), key=operator.itemgetter(1), reverse=True)
    line=srtd[0][0]
  elif string[:-4] in res:
    srtd = sorted(res[string[:-4]].items(), key=operator.itemgetter(1), reverse=True)
    a=srtd[0][0]
    line = a + ' ' + end
    
  else:
    string = string[:-4]
    no=len(string)
    j=0
    while j<no:
      if string[:j] in res and string[j:] in res:
        srtd = sorted(res[string[:j]].items(), key=operator.itemgetter(1), reverse=True)
        a = srtd[0][0]
        srtd = sorted(res[string[j:]].items(), key=operator.itemgetter(1), reverse=True)
        b=srtd[0][0]
        line = a + ' ' + b + ' ' + end
        
        
      j+=1
    if line==0:
      line='1'
      
  return line
def decimal(line):
      number_string=line
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = num2words(int(before_decimal),lang='ru')
      after = num2words(int(after_decimal),lang='ru')
      comma = 'целых и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целая и'
        split = before.split(' ')
        split[-1]='одна'
        before = ' '.join(split)
      elif before_decimal[-1]=='2' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        split= before.split(' ')
        split[-1]='две'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятых'
        if after_decimal=='1':
          frac='десятая'
          split= after.split(' ')
          split[-1]='одна'
          after = ' '.join(split)
        elif after_decimal=='2':
          split= after.split(' ')
          split[-1]='две'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотых'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотая'
          split= after.split(' ')
          split[-1]='одна'
          after = ' '.join(split)
        elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
          split= after.split(' ')
          split[-1]='две'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячных'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячная'
          split= after.split(' ')
          split[-1]='одна'
          after = ' '.join(split)
        elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
          split= after.split(' ')
          split[-1]='две'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десятая тысячная'
         split= after.split(' ')
         split[-1]='одна'
         after = ' '.join(split)
      else:
        frac = 'LOL BIG FRACCO M88'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac
      if 'целых и ноль десятых ' in line:
          line=line.replace('целых и ноль десятых ','')
      if int(after_decimal)==0:
        line = before
      return line
def decimal_genitive(line):
      number_string=line
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = genitivise(before_decimal)
      after = genitivise(after_decimal)
      comma = 'целых и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятых'
        if after_decimal=='1':
          frac='десятой'
          after='одной'
      elif len(after_decimal)==2:
        frac = 'сотых'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячных'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячных'
      else:
         frac='десяти тысячных'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac
      if 'целых и ноль десятых ' in line:
          line=line.replace('целых и ноль десятых ','')
      if int(after_decimal)==0:
        line = before
      return line

def decimal_prepositional(line):
      number_string=line
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = accusativise(before_decimal)
      after = accusativise(after_decimal)
      comma = 'целых и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целую и'
        split = before.split(' ')
        split[-1]='одну'
        before = ' '.join(split)
      elif before_decimal[-1]=='2' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        split = before.split(' ')
        split[-1]='две'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятых'
        if after_decimal=='1':
          frac='десятую'
          split= after.split(' ')
          split[-1]='одну'
          after = ' '.join(split)
        elif after_decimal=='2':
          split = after.split(' ')
          split[-1]=='две'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотых'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотую'
          split= after.split(' ')
          split[-1]='одну'
          after = ' '.join(split)
        elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          split= after.split(' ')
          split[-1]=='две'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячных'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячную'
          split= after.split(' ')
          split[-1]='одну'
          after = ' '.join(split)
        elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
       
          split= after.split(' ')
          split[-1]='две'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячных'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac
      if 'целых и ноль десятых ' in line:
          line=line.replace('целых и ноль десятых','')
      if int(after_decimal)==0:
        line = before
      return line
def decimal_instrumental(line):
      number_string=line
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = instrumentalise(before_decimal)
      after = instrumentalise(after_decimal)
      comma = 'целыми и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятыми'
        if after_decimal=='1':
          frac='десятой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотыми'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячными'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячными'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac
      if 'целых и ноль десятыми ' in line:
          line=line.replace('целых и ноль десятыми','')
      if int(after_decimal)==0:
        line = before
      return line
def decimal_dative(line):
      number_string=line
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = dativise(before_decimal)
      after = dativise(after_decimal)
      comma = 'целым и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятым'
        if after_decimal=='1':
          frac='десятой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотым'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячным'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячным'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac
      if 'целых и ноль десятым' in line:
          line=line.replace('целых и ноль десятым','')
      if int(after_decimal)==0:
        line = before
      return line
    
    
def thousandise(line):
      number = line.split(' ')[0]
      if ',' in number:
        tys = 'тысячи'
        place = number.find(',')
        before_decimal = number[:place]
        after_decimal = number[place+1:]
        
        if any(x==prev_line for x in genitive_prepositions):  
          before= genitivise(before_decimal)
          after= genitivise(after_decimal)
          comma = 'целых и'
          if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            comma = 'целой и'
            split = before.split(' ')
            split[-1]='одной'
            before = ' '.join(split)
          if len(after_decimal)==1:
            frac = 'десятых'
            if after_decimal=='1':
              frac='десятой'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==2:
            frac = 'сотых'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'сотой'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==3:
            frac = 'тысячных'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'тысячной'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==4:
             frac = 'десяти тысячных'
        else:  
          before = num2words(int(before_decimal),lang='ru')
          after = num2words(int(after_decimal),lang='ru')
          comma = 'целых и'
          if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            comma = 'целая и'
            split = before.split(' ')
            split[-1]='одна'
            before = ' '.join(split)
          elif before_decimal[-1]=='2' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            split= before.split(' ')
            split[-1]='две'
            before = ' '.join(split)
          if len(after_decimal)==1:
            frac = 'десятых'
            if after_decimal=='1':
              frac='десятая'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal=='2':
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==2:
            frac = 'сотых'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'сотая'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==3:
            frac = 'тысячных'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'тысячная'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==4:
             frac = 'десятая тысячная'
        line = before + ' ' + comma + ' ' +after + ' ' + frac + ' ' + tys
              
      else:
        if any(x==prev_line for x in genitive_prepositions):
          num = genitivise(number)
          if (number[-1]=='1'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='1'):
            a = num.split(' ')
            a[-1] = 'одной'
            num = ' '.join(a)
            tys = 'тысячи'
          else:
            tys = 'тысяч'
        else:
          num = num2words(int(number),lang='ru')
          if (number[-1]=='1'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='1'):
            a = num.split(' ')
            a[-1] = 'одну'
            tys = 'тысячу'
          elif (number[-1]=='2'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='2'):
            tys = 'тысячи'
            a = num.split(' ')
            a[-1] = 'две'
          elif (number[-1]=='3'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='3'): 
            tys = 'тысячи'
          elif (number[-1]=='4'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='4'): 
            tys = 'тысячи'
          else:
            tys = 'тысяч'
          if len(number)==4 and number[0]=='1':
            a = num.split(' ')
            a = a[1:]
            num = ' '.join(a)
        line = num + ' ' +tys
      return line
def millionise(line):
      number = line.split(' ')[0]
      if ',' in number:
        tys = 'миллиона'
        place = number.find(',')
        before_decimal = number[:place]
        after_decimal = number[place+1:]
        
        if any(x==prev_line for x in genitive_prepositions):  
          before= genitivise(before_decimal)
          after= genitivise(after_decimal)
          comma = 'целых и'
          if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            comma = 'целой и'
            split = before.split(' ')
            split[-1]='одной'
            before = ' '.join(split)
          if len(after_decimal)==1:
            frac = 'десятых'
            if after_decimal=='1':
              frac='десятой'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==2:
            frac = 'сотых'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'сотой'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==3:
            frac = 'тысячных'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'тысячной'
              split= after.split(' ')
              split[-1]='одной'
              after = ' '.join(split)
          elif len(after_decimal)==4:
             frac = 'десяти тысячных'
        else:  
          before = num2words(int(before_decimal),lang='ru')
          after = num2words(int(after_decimal),lang='ru')
          comma = 'целых и'
          if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            comma = 'целая и'
            split = before.split(' ')
            split[-1]='одна'
            before = ' '.join(split)
          elif before_decimal[-1]=='2' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
            split= before.split(' ')
            split[-1]='две'
            before = ' '.join(split)
          if len(after_decimal)==1:
            frac = 'десятых'
            if after_decimal=='1':
              frac='десятая'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal=='2':
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==2:
            frac = 'сотых'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'сотая'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==3:
            frac = 'тысячных'
            if after_decimal[-1]=='1' and after_decimal[-2]!='1':
              frac = 'тысячная'
              split= after.split(' ')
              split[-1]='одна'
              after = ' '.join(split)
            elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
              split= after.split(' ')
              split[-1]='две'
              after = ' '.join(split)
          elif len(after_decimal)==4:
             frac = 'десятая тысячная'
        line = before + ' ' + comma + ' ' +after + ' ' + frac + ' ' + tys
              
      else:
        if any(x==prev_line for x in genitive_prepositions):
          num = genitivise(number)
          if (number[-1]=='1'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='1'):
            a = num.split(' ')
            a[-1] = 'одного'
            num = ' '.join(a)
            tys = 'миллиона'
          else:
            tys = 'миллионов'
        else:
          num = num2words(int(number),lang='ru')
          if (number[-1]=='1'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='1'):
            a = num.split(' ')
            a[-1] = 'один'
            tys = 'миллион'
          elif (number[-1]=='2'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='2'):
            tys = 'миллиона'
            a = num.split(' ')
            
          elif (number[-1]=='3'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='3'): 
            tys = 'миллиона'
          elif (number[-1]=='4'and len(number)==1) or (len(number)>1 and number[-2]!='1' and number[-1]=='4'): 
            tys = 'миллиона'
          else:
            tys = 'миллионов'
          if len(number)==4 and number[0]=='1':
            a = num.split(' ')
            a = a[1:]
            num = ' '.join(a)
        line = num + ' ' +tys
      return line
        
    
while 1:
    
    line = train.readline().strip()
    if line == '':
        break
    total += 1
    pos = line.find('","')
    text = line[pos + 2:]
    if text[:3] == '","':
        continue
    text = text[1:-1]
    arr = text.split('","')
    if arr[0] != arr[1]:
      not_same += 1
      #for letter in latin_letters:
       # if letter in arr[0]:
        #  latin[arr[0]]=dict()
         # latin[arr[0]][arr[1]]=1
          #latin_number+=1
          
    if arr[0] not in res:
        res[arr[0]] = dict()
        res[arr[0]][arr[1]] = 1
    else:
        if arr[1] in res[arr[0]]:
            res[arr[0]][arr[1]] += 1
        else:
            res[arr[0]][arr[1]] = 1
train.close()
print(file + ':\tTotal: {} Have diff value: {}'.format(total, not_same))
files = os.listdir(DATA_INPUT_PATHH)
for file in files:
    print('another file!')
    print(file)
    train = open(os.path.join(DATA_INPUT_PATHH, file), encoding='UTF8')
    lines= train.read().splitlines()
    for i in range(1,len(lines)):
      
      ##LINE
       line = lines[i]
              
       if line == '':
            break
       total += 1
       pos = line.find('\t')
       text = line[pos + 1:]
       if text[:3] == '':
            continue
       arr = text.split('\t')
        ##PREV LINE
       prev_line = lines[i-1]
              
       if prev_line == '':
           break
       total += 1
       prev_pos = prev_line.find('\t')
       prev_text = prev_line[prev_pos + 1:]
       if prev_text[:3] == '':
           continue
       prev_arr = prev_text.split('\t')
      
        ##PREV PREV LINE
       prev_prev_line = lines[i-2]
             
       if prev_prev_line == '':
           break
       total += 1
       prev_prev_pos = prev_prev_line.find('\t')
       prev_prev_text = prev_prev_line[prev_prev_pos + 1:]
       if prev_prev_text[:3] == '':
           continue
       prev_prev_arr = prev_prev_text.split('\t')
        ##PREV PREV PREV LINE
       prev_prev_prev_line = lines[i-3]
             
       if prev_prev_prev_line == '':
           break
       total += 1
       prev_prev_prev_pos = prev_prev_prev_line.find('\t')
       prev_prev_prev_text = prev_prev_prev_line[prev_prev_prev_pos + 1:]
       if prev_prev_prev_text[:3] == '':
           continue
       prev_prev_prev_arr = prev_prev_prev_text.split('\t')
       ## NEXT LINE
       if i < len(lines)-1:
         next_line = lines[i+1]
             
         if next_line == '':
             break
         total += 1
         next_pos = next_line.find('\t')
         next_text = next_line[next_pos + 1:]
         if next_text[:3] == '':
             continue
         next_arr = next_text.split('\t')
       ## NEXT NEXT LINE
       if i < len(lines)-2:
         next_next_line = lines[i+2]
             
         if next_next_line == '':
             break
         total += 1
         next_next_pos = next_next_line.find('\t')
         next_next_text = next_next_line[next_next_pos + 1:]
         if next_next_text[:3] == '':
             continue
         next_next_arr = next_next_text.split('\t')
       if i < len(lines)-3:
         next_next_next_line = lines[i+3]
             
         if next_next_next_line == '':
             break
         total += 1
         next_next_next_pos = next_next_next_line.find('\t')
         next_next_next_text = next_next_next_line[next_next_next_pos + 1:]
         if next_next_next_text[:3] == '':
             continue
         next_next_next_arr = next_next_next_text.split('\t')
      ##CONTINUE
       if arr[0] == '<eos>':
           continue
       if arr[1] != '<self>':
          not_same += 1
           
       if arr[1] == '<self>' or arr[1] == 'sil':
          arr[1] = arr[0]
       if arr[1] == '<self>' or arr[1] == 'sil':            
           arr[1] = arr[0]
       if arr[0] not in res:
          res[arr[0]] = dict()
          res[arr[0]][arr[1]] = 1
       else:
          if arr[1] in res[arr[0]]:
              res[arr[0]][arr[1]] += 1
          else:
              res[arr[0]][arr[1]] = 1
       first_pieces = arr[0]
       first_pieces = str(first_pieces)
       first_pieces = first_pieces.split(' ')
       second_pieces = arr[1]
       second_pieces = str(second_pieces)
       second_pieces=second_pieces.split(' ')
       if prev_arr[0]=='<eos>':
         prev_arr = prev_prev_arr
       if prev_prev_arr[0]=='<eos>':
         prev_prev_arr = prev_prev_prev_arr
       if next_arr[0]=='<eos>':
         next_arr = next_next_arr
       if next_next_arr[0]=='<eos>':
         next_next_arr = next_next_next_arr
       if ' '.join(first_pieces).replace(' ','').isdigit():
         
         if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] +'BREAK' + prev_prev_arr[0] not in cardinal5_dict:
          cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         else:
          if arr[1] in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] + 'BREAK' + prev_prev_arr[0]]:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          else:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0]+'BREAK' +arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       #if ((len(first_pieces)==1 and first_pieces[0].isdigit()) or (len(first_pieces)==2 and first_pieces[0].isdigit() and first_pieces[1].isdigit())or (len(first_pieces)==3 and first_pieces[0].isdigit() and first_pieces[1].isdigit() and first_pieces[2].isdigit())):
         
         #if next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in cardinal3_dict:
          #cardinal3_dict[next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #cardinal3_dict[next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         #else:
          #if arr[1] in cardinal3_dict[next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            #cardinal3_dict[next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          #else:
            #cardinal3_dict[next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
            
         #if next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in cardinal3_dict:
          #cardinal3_dict[next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #cardinal3_dict[next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         #else:
          #if arr[1] in cardinal3_dict[next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            #cardinal3_dict[next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          #else:
            #cardinal3_dict[next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       #elif len(first_pieces)==1 and first_pieces[0] in roman_numerals_full:
         
         #if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in roman5_dict:
          #roman5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #roman5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         #else:
          #if arr[1] in roman5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            #roman5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          #else:
            #roman5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       elif len(first_pieces)==2 and first_pieces[0].isdigit() and any(x==first_pieces[1] for x in gen_months) :
         if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in year_len2_dict:
          year_len2_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          year_len2_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         else:
          if arr[1] in year_len2_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            year_len2_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          else:
            year_len2_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       elif ' '.join(first_pieces).replace(' ','').isdigit():
         if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in cardinal5_dict :
          cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          
          if 'ах' in second_pieces[0] or 'ах' in second_pieces[1]:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ах'] = 1
          elif 'ью'in second_pieces[0] or 'ью' in second_pieces[1] or 'ами'in second_pieces[0] or 'ами' in second_pieces[1]:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ью'] = 1
          elif 'ому'in second_pieces[0] or 'ому' in second_pieces[1]or 'ам'in second_pieces[0] or 'ам' in second_pieces[1]:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] = 1
          elif 'х' in second_pieces[0] or 'х' in second_pieces[1] or 'ти' in second_pieces[0] or 'ти' in second_pieces[1]:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ти'] = 1
          else:
            cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['acc'] = 1
         else:
          
          if 'ах' in second_pieces[0] or 'ах' in second_pieces[1]:
            if 'ах' in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ах'] +=1
            else:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ах'] =1
          elif 'ью'in second_pieces[0] or 'ью' in second_pieces[1] or 'ами'in second_pieces[0] or 'ами' in second_pieces[1]:
            if 'ью' in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ью'] +=1
            else:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ью'] =1
          elif 'ому'in second_pieces[0] or 'ому' in second_pieces[1]or 'ам'in second_pieces[0] or 'ам' in second_pieces[1]:
            if 'ому' in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] +=1
            else:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] =1
          elif 'х' in second_pieces[0] or 'х' in second_pieces[1] or 'ти' in second_pieces[0] or 'ти' in second_pieces[1]:
            if 'ти' in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ти'] +=1
            else:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ти'] =1
          else:
            if 'acc' in cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['acc'] +=1
            else:
              cardinal5_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['acc'] =1
       #elif len(first_pieces)==3 and first_pieces[0].isdigit() and any(x==first_pieces[1] for x in gen_months) and first_pieces[2].isdigit():
         #if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in year_len3_dict:
          #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         #else:
          #if arr[1] in year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          #else:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       #elif len(first_pieces)==4 and first_pieces[0].isdigit() and any(x==first_pieces[1] for x in gen_months) and first_pieces[2].isdigit():
         #if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in year_len4_dict:
          #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         #else:
          #if arr[1] in year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          #else:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       #elif len(first_pieces)==3 and first_pieces[0].isdigit() and any(x==first_pieces[1] for x in gen_months) and first_pieces[2].isdigit() and 'года' in second_pieces and 'годами' not in second_pieces:
         #if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in year_len3_dict :
          #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #if ('ое' in second_pieces[0] or 'ое' in second_pieces[1]) or 'третье' in second_pieces[0]or 'третье' in second_pieces[1]:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] = 1
          #elif 'го' in second_pieces[0] or 'го' in second_pieces[1]:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] = 1
          #elif 'ым'in second_pieces[0] or 'ым' in second_pieces[1]:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] = 1
          #elif 'ому'in second_pieces[0] or 'ому' in second_pieces[1]:
            #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] = 1
          #else:
            #print(second_pieces)
         #else:
          #if ('ое' in second_pieces[0] or 'ое' in second_pieces[1]) or 'третье' in second_pieces[0]or 'третье' in second_pieces[1]:
            #if 'ое' in year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] += 1
            #else:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] = 1
          #elif 'го' in second_pieces[0] or 'го' in second_pieces[1]:
            #if 'го' in year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] +=1
            #else:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] =1
          #elif 'ым'in second_pieces[0] or 'ым' in second_pieces[1]:
            #if 'ым' in year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] +=1
            #else:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] =1
          #elif 'ому' in second_pieces[0] or 'ому' in second_pieces[1]:
            #if 'ому' in year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] +=1
            #else:
              #year_len3_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] =1
       #elif len(first_pieces)==4 and first_pieces[0].isdigit() and any(x==first_pieces[1] for x in gen_months) and first_pieces[2].isdigit() and first_pieces[3]=='года':
         #if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in year_len4_dict :
          #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          #if ('ое' in second_pieces[0] or 'ое' in second_pieces[1]) or 'третье' in second_pieces[0]or 'третье' in second_pieces[1]:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] = 1
          #elif 'го' in second_pieces[0] or 'го' in second_pieces[1]:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] = 1
          #elif 'ым'in second_pieces[0] or 'ым' in second_pieces[1]:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] = 1
          #elif 'ому'in second_pieces[0] or 'ому' in second_pieces[1]:
            #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] = 1
          #else:
            #print(second_pieces)
         #else:
          #if ('ое' in second_pieces[0] or 'ое' in second_pieces[1]) or 'третье' in second_pieces[0]or 'третье' in second_pieces[1]:
            #if 'ое' in year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] += 1
            #else:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ое'] = 1
          #elif 'го' in second_pieces[0] or 'го' in second_pieces[1]:
            #if 'го' in year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] +=1
            #else:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['го'] =1
          #elif 'ым'in second_pieces[0] or 'ым' in second_pieces[1]:
            #if 'ым' in year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] +=1
            #else:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ым'] =1
          #elif 'ому' in second_pieces[0] or 'ому' in second_pieces[1]:
            #if 'ому' in year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] +=1
            #else:
              #year_len4_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]['ому'] =1
####        elif first_pieces[0].count(',')==1 and first_pieces[0].replace(',','').isdigit() :
###          
###          if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in decimal_dict:
###           decimal_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
##           decimal_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
##          else:
##           if arr[1] in decimal_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
##             decimal_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
##           else:
##             decimal_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
##        elif first_pieces[0].count('-')==1 and first_pieces[0].replace('-','').isdigit():
##          
##          if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in minus_dict:
##           minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
##           minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
##          else:
          ##if arr[1] in minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          ##else:
            ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
       ##elif first_pieces[0].count('—')==1 and first_pieces[0].replace('—','').isdigit():
         
         ##if next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0] not in minus_dict:
          ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]=dict()
          ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] = 1
         ##else:
          ##if arr[1] in minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]]:
            ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]] += 1
          ##else:
            ##minus_dict[next_next_arr[0]+ 'BREAK'+ next_arr[0]+'BREAK'+arr[0]+'BREAK'+prev_arr[0] + 'BREAK' + prev_prev_arr[0]][arr[1]]=1
###THE FILES
files = os.listdir(DATA_INPUT_PATH)
for file in files:
    print('another file!')
    print(file)
    train = open(os.path.join(DATA_INPUT_PATH, file), encoding='UTF8')
    lines= train.read().splitlines()
    for i in range(1,len(lines)):
      
      ##LINE
        line = lines[i]
              
        if line == '':
            break
        total += 1
        pos = line.find('\t')
        text = line[pos + 1:]
        if text[:3] == '':
            continue
        arr = text.split('\t')
      
        ##CONTINUE
        if arr[0] == '<eos>':
            continue
        if arr[1] != '<self>':
           not_same += 1
            
        if arr[1] == '<self>' or arr[1] == 'sil':
           arr[1] = arr[0]
        if arr[1] == '<self>' or arr[1] == 'sil':            
            arr[1] = arr[0]
        if arr[0] not in res:
           res[arr[0]] = dict()
           res[arr[0]][arr[1]] = 1
        else:
           if arr[1] in res[arr[0]]:
               res[arr[0]][arr[1]] += 1
           else:
               res[arr[0]][arr[1]] = 1
      
    
    train.close()        
    print(file + ':\tTotal: {} Have diff value: {}'.format(total, not_same))
    gc.collect()
    
#for file in files:
   #print('another file!')
   #print(file)
   #train = open(os.path.join(DATA_INPUT_PATH, file), encoding='UTF8')
   #while 1:
       #line = train.readline().strip()        
       #if line == '':
           #break
       #total += 1
       #pos = line.find('\t')
       #text = line[pos + 1:]
       #if text[:3] == '':
           #continue
       #arr = text.split('\t')
       #if arr[0] == '<eos>':
           #continue
       #if arr[1] != '<self>':
          #not_same += 1
           
       #if arr[1] == '<self>' or arr[1] == 'sil':
          #arr[1] = arr[0]
       #if arr[1] == '<self>' or arr[1] == 'sil':            
           #arr[1] = arr[0]
       #if arr[0] not in res:
          #res[arr[0]] = dict()
          #res[arr[0]][arr[1]] = 1
       #else:
           #if arr[1] in res[arr[0]]:
              #res[arr[0]][arr[1]] += 1
           #else:
              #res[arr[0]][arr[1]] = 1
   #train.close()        
   #print(file + ':\tTotal: {} Have diff value: {}'.format(total, not_same))
   #gc.collect()
sdict = {}
ddict={}
fdict={}



ddict['I'] =1
ddict['i'] = 1
ddict['II'] = 2
ddict['I I'] = 2
ddict['ii'] = 2
ddict['i i'] = 2
ddict['III'] =3
ddict['I I I'] =3
ddict['iii'] = 3
ddict['i i i'] = 3
ddict['IV'] =4
ddict['I V'] =4
ddict['iv'] = 4
ddict['i v'] = 4
ddict['V'] = 5
ddict['v'] = 5
ddict['VI'] =6
ddict['V I'] =6
ddict['vi'] = 6
ddict['v i'] = 6
ddict['VII'] =7
ddict['V I I'] =7
ddict['vii'] = 7
ddict['v i i'] = 7
ddict['VIII'] =8
ddict['V I I I'] =8
ddict['viii'] = 8
ddict['v i i i'] = 8
ddict['IX'] =9
ddict['I X'] =9
ddict['ix'] = 9
ddict['i x'] = 9
ddict['X'] =10
ddict['x'] = 10
ddict['XI'] = 11
ddict['X I'] = 11
ddict['xi'] = 11
ddict['x i'] = 11
ddict['XII'] = 12
ddict['X I I'] = 12
ddict['xii'] = 12
ddict['x i i'] = 12
ddict['XIII'] = 13
ddict['X I I I'] = 13
ddict['xiii'] = 13
ddict['x i i i'] = 13
ddict['XIV'] = 14
ddict['X I V'] = 14
ddict['xiv'] = 14
ddict['x i v'] = 14
ddict['XV'] = 15
ddict['X V'] = 15
ddict['xv'] = 15
ddict['x v'] = 15
ddict['XVI'] = 16
ddict['X V I'] = 16
ddict['xvi']=16
ddict['x v i']=16
ddict['XVII'] = 17
ddict['X V I I'] =17
ddict['xvii'] = 17
ddict['x v i i'] = 17
ddict['XVIII'] = 18
ddict['X V I I I'] = 18
ddict['xviii'] = 18
ddict['x v i i i'] = 18
ddict['XIX'] = 19
ddict['X I X'] =19
ddict['xix'] =19
ddict['x i x'] = 19
ddict['XX'] = 20
ddict['X X'] = 20
ddict['xx'] = 20
ddict['x x'] = 20
ddict['XXI'] =  21
ddict['ХХI']=21
ddict['X X I'] = 21
ddict['xxi'] = 21
ddict['x x i'] = 21
ddict['XXII'] = 22
ddict['X X I I'] = 22
ddict['xxii']= 22
ddict['x x i i']= 22
ddict['XXIII'] = 23
ddict['X X I I I'] = 23
ddict['xxiii']= 23
ddict['x x i i i']= 23
ddict['XXIV'] = 24
ddict['X X I V'] = 24
ddict['xxiv']= 24
ddict['x x i v']= 24
ddict['XXV'] = 25
ddict['X X V'] = 25
ddict['xxv']= 25
ddict['x x v']= 25
ddict['XXVI'] = 26
ddict['X X V I'] = 26
ddict['xxvi']= 26
ddict['x x v i']= 26
ddict['XXVII'] = 27
ddict['X X V I I'] = 27
ddict['xxvii']= 27
ddict['x x v i i']= 27
ddict['XXVIII'] = 28
ddict['X X V I I I'] = 28
ddict['xxviii']= 28
ddict['x x v i i i']= 28
ddict['XXIX'] = 29
ddict['X X I X'] = 29
ddict['xxix']= 29
ddict['x x i x']= 29
ddict['XXX'] = 30
ddict['X X X'] = 30
ddict['xxx']= 30
ddict['x x x']= 30
ddict['XXXI'] = 31
ddict['X X X I'] = 31
ddict['xxxi']= 31
ddict['x x x i']= 31
ddict['XXXII'] = 32
ddict['X X X I I'] = 32
ddict['xxxii']= 32
ddict['x x x i i']= 32
ddict['XXXIII'] = 33
ddict['X X X I I I'] = 33
ddict['xxxiii']= 33
ddict['x x x i i i']= 33
ddict['XXXIV'] = 34
ddict['X X X I V'] = 34
ddict['xxxiv']= 34
ddict['x x x i v']= 34
ddict['XXXV'] = 35
ddict['X X X V'] = 35
ddict['xxxv']= 35
ddict['x x x v']= 35
ddict['XXXVI'] = 36
ddict['X X X V I'] = 36
ddict['xxxvi']= 36
ddict['x x x v i']= 36
ddict['XXXVII'] = 37
ddict['X X X V I I'] = 37
ddict['xxxvii']= 37
ddict['x x x v i i']= 37
ddict['XXXVIII'] = 38
ddict['X X X V I I I'] = 38
ddict['xxxviii']= 38
ddict['x x x v i i i']= 38
ddict['XXXIX'] = 39
ddict['X X X I X'] = 39
ddict['xxxix']= 39
ddict['x x x i x']= 39
ddict['XXXX'] = 40
ddict['X X X X'] = 40
ddict['xxxx']= 40
ddict['x x x x']= 40
sdict['ГЭУ'] = 'главная энергетическая установка'


measurement_dict={}
measurement_dict['см'] = 'сантиметр'
measurement_dict['с.м.'] = 'сантиметр'
measurement_dict['см.'] = 'сантиметр'
measurement_dict['cm'] = 'сантиметр'
measurement_dict['c.m.'] = 'сантиметр'
measurement_dict['cm.']= 'сантиметр'
measurement_dict['км²'] = 'квадратных километров'
measurement_dict['км2'] = 'квадратных километров'
measurement_dict['km²'] = 'квадратных километров'
measurement_dict['см2'] = 'сантиметр'
measurement_dict['с.м.2'] = 'сантиметр'
measurement_dict['см2.'] = 'сантиметр'
measurement_dict['cm2'] = 'сантиметр'
measurement_dict['c.m².'] = 'сантиметр'
measurement_dict['см2'] = 'сантиметр'
measurement_dict['с.м.²'] = 'сантиметр'
measurement_dict['см².'] = 'сантиметр'
measurement_dict['cm²'] = 'сантиметр'
measurement_dict['c.m².'] = 'сантиметр'
measurement_dict['cm².']= 'сантиметр'
measurement_dict['км²'] = 'квадратных километров'
measurement_dict['км2'] = 'квадратных километров'
measurement_dict['km²'] = 'квадратных километров'
measurement_dict['км'] = 'сантиметр'
measurement_dict['к.м.'] = 'сантиметр'
measurement_dict['км.'] = 'сантиметр'
measurement_dict['km'] = 'сантиметр'
measurement_dict['k.m.'] = 'сантиметр'
measurement_dict['km.']= 'сантиметр'
measurement_dict['m²'] = 'квадратных метров'
measurement_dict['м²'] = 'квадратных метров'
measurement_dict['мм'] = '1'
measurement_dict['м.м.']='1'
measurement_dict['мм.']='1'

#measurement_dict['г'] = 'грамм'

measurement_dict['мг'] = 'грамм'
measurement_dict['мг.']='грамм'
measurement_dict['кг'] = 'сантиметр'
measurement_dict['к.г.'] = 'сантиметр'
measurement_dict['кг.'] = 'сантиметр'

measurement_dict['с.']= 'секунд'

measurement_dict['мин.']='1'



measurement_dict['л.']='1'
measurement_dict['л']='1'

measurement_dict['мл.']='1'
measurement_dict['мл']='1'

measurement_dict['т.']='1'
measurement_dict['%']='процент'
measurement_dict['%.']='процент'
measurement_dict['cm²'] = 'квадратный сантиметр'
measurement_dict['см²'] = 'квадратный сантиметр'
measurement_dict['cm³'] = 'квадратный сантиметр'
measurement_dict['см³'] = 'квадратный сантиметр'

measurement_single_dict={}
measurement_single_dict['см'] = 'сантиметр'
measurement_single_dict['с.м.'] = 'сантиметр'
measurement_single_dict['см.'] = 'сантиметр'
measurement_single_dict['cm'] = 'сантиметр'
measurement_single_dict['c.m.'] = 'сантиметр'
measurement_single_dict['cm.']= 'сантиметр'

measurement_single_dict['км'] = 'километр'
measurement_single_dict['к.м.'] = 'километр'
measurement_single_dict['км.'] = 'километр'
measurement_single_dict['km'] = 'километр'
measurement_single_dict['k.m.'] = 'километр'
measurement_single_dict['km.']= 'километр'
measurement_single_dict['мм'] = 'миллиметр'
measurement_single_dict['м.м.']='миллиметр'
measurement_single_dict['мм.']='миллиметр'
measurement_single_dict['g'] = 'грамм'
measurement_single_dict['g.']='грамм'
measurement_single_dict['г'] = 'грамм'
measurement_single_dict['г.']='грамм'
measurement_single_dict['mg'] = 'миллиграмм'
measurement_single_dict['mg.']='миллиграмм'
measurement_single_dict['мг'] = 'миллиграмм'
measurement_single_dict['мг.']='миллиграмм'
measurement_single_dict['кг'] = 'килограмм'
measurement_single_dict['к.г.'] = 'килограмм'
measurement_single_dict['кг.'] = 'килограмм'
measurement_single_dict['kg'] = 'килограмм'
measurement_single_dict['k.g.'] = 'килограмм'
measurement_single_dict['kg.']= 'килограмм'
measurement_single_dict['с.']= 'секунда'
measurement_single_dict['s.']= 'секунда'
measurement_single_dict['мин.']='минута'
measurement_single_dict['min.']='минута'
measurement_single_dict['минут.']='минута'
measurement_single_dict['l.']='литр'
measurement_single_dict['л.']='литр'
measurement_single_dict['л']='литр'
measurement_single_dict['l.']='литр'
measurement_single_dict['мл.']='миллилитр'
measurement_single_dict['мл']='миллилитр'
measurement_single_dict['т.']='тонна'
measurement_single_dict['%']='процент'
measurement_single_dict['%.']='процент'
measurement_single_dict['км²'] = 'квадратный километр'
measurement_single_dict['км2'] = 'квадратный километр'
measurement_single_dict['km²'] = 'квадратный километр'
measurement_single_dict['m²'] = 'квадратный метр'
measurement_single_dict['м²'] = 'квадратный метр'
measurement_single_dict['cm²'] = 'квадратный сантиметр'
measurement_single_dict['см²'] = 'квадратный сантиметр'
measurement_single_dict['cm³'] = 'кубический сантиметр'
measurement_single_dict['см³'] = 'кубический сантиметр'
measurement_single_dict['см2'] = 'квадратный сантиметр'

measurement_gen_single_dict={}
measurement_gen_single_dict['см'] = 'сантиметра'
measurement_gen_single_dict['с.м.'] = 'сантиметра'
measurement_gen_single_dict['см.'] = 'сантиметра'
measurement_gen_single_dict['cm'] = 'сантиметра'
measurement_gen_single_dict['c.m.'] = 'сантиметра'
measurement_gen_single_dict['cm.']= 'сантиметра'
measurement_gen_single_dict['сантиметр'] = 'сантиметра'
measurement_gen_single_dict['км'] = 'километра'
measurement_gen_single_dict['к.м.'] = 'километра'
measurement_gen_single_dict['км.'] = 'километра'
measurement_gen_single_dict['km'] = 'километра'
measurement_gen_single_dict['k.m.'] = 'километра'
measurement_gen_single_dict['km.']= 'километра'
measurement_gen_single_dict['мм'] = 'миллиметра'
measurement_gen_single_dict['м.м.']='миллиметра'
measurement_gen_single_dict['мм.']='миллиметра'
measurement_gen_single_dict['g'] = 'грамма'
measurement_gen_single_dict['g.']='грамма'
measurement_gen_single_dict['г'] = 'грамма'
measurement_gen_single_dict['г.']='грамма'
measurement_gen_single_dict['mg'] = 'миллиграмма'
measurement_gen_single_dict['mg.']='миллиграмма'
measurement_gen_single_dict['мг'] = 'миллиграмма'
measurement_gen_single_dict['мг.']='миллиграмма'
measurement_gen_single_dict['кг'] = 'килограмма'
measurement_gen_single_dict['к.г.'] = 'килограмма'
measurement_gen_single_dict['кг.'] = 'килограмма'
measurement_gen_single_dict['kg'] = 'килограмма'
measurement_gen_single_dict['k.g.'] = 'килограмма'
measurement_gen_single_dict['kg.']= 'килограмма'
measurement_gen_single_dict['с.']= 'секунды'
measurement_gen_single_dict['s.']= 'секунды'
measurement_gen_single_dict['мин.']='минуты'
measurement_gen_single_dict['min.']='минуты'
measurement_gen_single_dict['минут.']='минуты'
measurement_gen_single_dict['l.']='литра'
measurement_gen_single_dict['л.']='литра'
measurement_gen_single_dict['л']='литра'
measurement_gen_single_dict['l.']='литра'
measurement_gen_single_dict['мл.']='миллилитра'
measurement_gen_single_dict['мл']='миллилитра'
measurement_gen_single_dict['т.']='тонны'
measurement_gen_single_dict['%']='процента'
measurement_gen_single_dict['%.']='процента'
measurement_gen_single_dict['км²'] = 'квадратного километра'
measurement_gen_single_dict['км2'] = 'квадратного километра'
measurement_gen_single_dict['km²'] = 'квадратного километра'
measurement_gen_single_dict['m²'] = 'квадратного метра'
measurement_gen_single_dict['м²'] = 'квадратного метра'
measurement_gen_single_dict['cm²'] = 'квадратного сантиметра'
measurement_gen_single_dict['см²'] = 'квадратного сантиметра'
measurement_gen_single_dict['cm³'] = 'кубического сантиметра'
measurement_gen_single_dict['см³'] = 'кубического сантиметра'
measurement_gen_single_dict['см2'] = 'квадратного сантиметра'

measurement_gen_plural_dict={}
measurement_gen_plural_dict['см'] = 'сантиметров'
measurement_gen_plural_dict['с.м.'] = 'сантиметров'
measurement_gen_plural_dict['см.'] = 'сантиметров'
measurement_gen_plural_dict['cm'] = 'сантиметров'
measurement_gen_plural_dict['c.m.'] = 'сантиметров'
measurement_gen_plural_dict['cm.']= 'сантиметров'
measurement_gen_plural_dict['сантиметр'] = 'сантиметров'
measurement_gen_plural_dict['км'] = 'километров'
measurement_gen_plural_dict['к.м.'] = 'километров'
measurement_gen_plural_dict['км.'] = 'километров'
measurement_gen_plural_dict['km'] = 'километров'
measurement_gen_plural_dict['k.m.'] = 'километров'
measurement_gen_plural_dict['km.']= 'километров'
measurement_gen_plural_dict['мм'] = 'миллиметров'
measurement_gen_plural_dict['м.м.']='миллиметров'
measurement_gen_plural_dict['мм.']='миллиметров'
measurement_gen_plural_dict['g'] = 'граммов'
measurement_gen_plural_dict['g.']='граммов'
measurement_gen_plural_dict['г'] = 'граммов'
measurement_gen_plural_dict['г.']='граммов'
measurement_gen_plural_dict['mg'] = 'миллиграмм'
measurement_gen_plural_dict['mg.']='миллиграмм'
measurement_gen_plural_dict['мг'] = 'миллиграмм'
measurement_gen_plural_dict['мг.']='миллиграмм'
measurement_gen_plural_dict['кг'] = 'килограмм'
measurement_gen_plural_dict['к.г.'] = 'килограмм'
measurement_gen_plural_dict['кг.'] = 'килограмм'
measurement_gen_plural_dict['kg'] = 'килограмма'
measurement_gen_plural_dict['k.g.'] = 'килограмм'
measurement_gen_plural_dict['kg.']= 'килограммов'
measurement_gen_plural_dict['с.']= 'секунд'
measurement_gen_plural_dict['s.']= 'секунд'
measurement_gen_plural_dict['мин.']='минут'
measurement_gen_plural_dict['min.']='минут'
measurement_gen_plural_dict['минут.']='минут'
measurement_gen_plural_dict['l.']='литров'
measurement_gen_plural_dict['л.']='литров'
measurement_gen_plural_dict['л']='литров'
measurement_gen_plural_dict['l.']='литров'
measurement_gen_plural_dict['мл.']='миллилитров'
measurement_gen_plural_dict['мл']='миллилитров'
measurement_gen_plural_dict['т.']='тонн'
measurement_gen_plural_dict['%']='процентов'
measurement_gen_plural_dict['%.']='процентов'
measurement_gen_plural_dict['км²'] = 'квадратных километров'
measurement_gen_plural_dict['км2'] = 'квадратных километров'
measurement_gen_plural_dict['km²'] = 'квадратных километров'
measurement_gen_plural_dict['m²'] = 'квадратных метров'
measurement_gen_plural_dict['м²'] = 'квадратных метров'
measurement_gen_plural_dict['cm²'] = 'квадратных сантиметров'
measurement_gen_plural_dict['см²'] = 'квадратных сантиметров'
measurement_gen_plural_dict['cm³'] = 'кубических сантиметров'
measurement_gen_plural_dict['см³'] = 'кубических сантиметров'
measurement_gen_plural_dict['см2'] = 'квадратных сантиметров'


me_dict={}
me_single_dict={}
me_gen_plural_dict={}
me_gen_single_dict={}
me_prefix={}

me_prefix['к']='кило'
me_prefix['к.']='кило'
me_prefix['К']='кило'
me_prefix['К.']='кило'
me_prefix['k']='кило'
me_prefix['k.']='кило'
me_prefix['K']='кило'
me_prefix['K.']='кило'
me_prefix['м']='милли'
me_prefix['м.']='милли'
me_prefix['m']='милли'
me_prefix['m.']='милли'
me_prefix['Г']='гига'
me_prefix['Г.']='гига'
me_prefix['G']='гига'
me_prefix['G.']='гига'
me_prefix['М']='мега'
me_prefix['М.']='мега'
me_prefix['M']='мега'
me_prefix['M.']='мега'
me_prefix['П']='пета'
me_prefix['П.']='пета'
me_prefix['P']='пета'
me_prefix['P.']='пета'
me_prefix['Т']='тера'
me_prefix['Т.']='тера'
me_prefix['T']='тера'
me_prefix['T.']='тера'
me_prefix['μ']='микро'
me_prefix['μ.']='микро'
me_prefix['н']='нано'
me_prefix['н.']='нано'
me_prefix['n']='нано'
me_prefix['n.']='нано'

me_dict['км/час']=['километров в час','m']
me_dict['км/час.']=['километров в час','m']
me_dict['см'] = ['сантиметр', 'm']
me_dict['с.м.'] = ['сантиметр','m']
me_dict['см.'] = ['сантиметр','m']
me_dict['cm'] = ['сантиметр','m']
me_dict['c.m.'] = ['сантиметр','m']
me_dict['cm.'] = ['сантиметр','m']
me_dict['м'] = ['метр','m']
me_dict['м.'] = ['метр','m']
me_dict['m'] = ['метр','m']
me_dict['m.'] = ['метр','m']
me_dict['км²']=['квадратного километра','m']
me_dict['км².']=['квадратного километра','m']
me_dict['м²']=['квадратного метра','m']
me_dict['м².']=['квадратного метра','m']
me_dict['cm³'] = ['кубических сантиметров','m']
me_dict['см³'] = ['кубических сантиметров','m']
me_dict['см³.'] = ['кубических сантиметров','m']
me_dict['m³'] = ['кубических метров','m']
me_dict['м³'] = ['кубических метров','m']
me_dict['м³.'] = ['кубических метров','m']
me_dict['M'] = ['метр','m']
me_dict['M.'] = ['метр','m']
me_dict['кг'] = ['сантиметр','m']
me_dict['к.г.'] = ['сантиметр','m']
me_dict['кг.'] = ['сантиметр','m']
me_dict['г'] = ['ss','m']
me_dict['мг'] = ['миллиграмм','m']
me_dict['мг.'] = ['миллиграмм','m']
me_dict['kg'] = ['сантиметр','m']
me_dict['k.g.'] = ['сантиметр','m']
me_dict['kg.'] = ['сантиметр','m']
me_dict['л.']=['литр','m']
me_dict['л']=['литр','m']
me_dict['Л.']=['литр','m']
me_dict['Л']=['литр','m']
me_dict['l.']=['литр','m']
me_dict['l']=['литр','m']
me_dict['L.']=['литр','m']
me_dict['L']=['литр','m']
me_dict['%']=['процент','m']
me_dict['%.']=['процент','m']
me_dict['га']=['гектаров','m']
me_dict['га.']=['гектаров','m']
me_dict['Б']=['гигабайтов','m']
me_dict['Б.']=['гигабайтов','m']
me_dict['B']=['гигабайтов','m']
me_dict['B.']=['гигабайтов','m']
me_dict['б'] = ['байт','m']
me_dict['б.']=['байт','m']
me_dict['b']=['бит','m']
me_dict['b.']=['бит','m']
me_dict['В.']=['вольта','m']
me_dict['V.']=['вольта','m']
me_dict['V']=['вольт','m']
me_dict['К']=['кельвина','m']
me_dict['К.']=['кельвина','m']
me_dict['K']=['кельвина','m']
me_dict['K.']=['кельвина','m']
me_dict['А']=['ампер','m']
me_dict['А.']=['ампер','m']
me_dict['Гц']=['герц','m']
me_dict['Гц.']=['герц','m']
me_dict['Hz']=['герц','m']
me_dict['Hz.']=['герц','m']
me_dict['hz.']=['герц','m']
me_dict['hz']=['герц','m']
me_dict['Вт.']=['ватт','m']
me_dict['W']=['ватт','m']
me_dict['W.']=['ватт','m']
me_dict['т.']=['тонны','f']
me_dict['т']=['тонны','f']
me_dict['T.']=['тонны','f']
me_dict['T']=['тонны','f']
me_dict['USD']=['доллар','m']
me_dict['$']=['доллар','m']
me_dict['тонн']=['тонны','f']
me_dict['Гбит/с']=['гигабит в секунду','m']
me_dict['метр']=['метр','m']
me_dict['сантиметр']=['метр','m']
me_dict['литр']=['метр','m']
me_dict['процент']=['метр','m']
me_dict['гектар']=['метр','m']
me_dict['байт']=['метр','m']
me_dict['бит']=['метр','m']
me_dict['вольт']=['метр','m']
me_dict['кельвин']=['метр','m']
me_dict['ампер']=['метр','m']
me_dict['герц']=['метр','m']
me_dict['ватт']=['метр','m']
me_dict['ч']=['час','m']
me_dict['ч.']=['час','m']
me_dict['с.']= ['секунда','f']
me_dict['с']= ['секунда','f']
me_dict['s.']= ['секунда','f']
me_dict['мин.']=['минута','f']
me_dict['мин']=['минута','f']
me_dict['min.']=['минута','f']
me_dict['min']=['минута','f']
me_dict['минут.']=['минута','f']
me_dict['минут']=['минута','f']
#me_dict['тыс.']=

me_single_dict['Гбит/с']='гигабит в секунду'
me_single_dict['кг'] = 'килограмм'
me_single_dict['к.г.'] = 'килограмм'
me_single_dict['г.'] = 'грамм'
me_single_dict['г'] = 'грамм'
me_single_dict['мг'] = 'миллиграмм'
me_single_dict['мг.'] = 'миллиграмм'
me_single_dict['кг.'] = 'килограмм'
me_single_dict['kg'] = 'килограмм'
me_single_dict['k.g.'] = 'килограмм'
me_single_dict['kg.'] = 'килограмм'
me_single_dict['см'] = 'сантиметр'
me_single_dict['с.м.'] = 'сантиметр'
me_single_dict['см.'] = 'сантиметр'
me_single_dict['cm'] = 'сантиметр'
me_single_dict['c.m.'] = 'сантиметр'
me_single_dict['cm.'] = 'сантиметр'
me_single_dict['м.'] = 'метр'
me_single_dict['м'] = 'метр'
me_single_dict['M.'] = 'метр'
me_single_dict['M'] = 'метр'
me_single_dict['m.'] = 'метр'
me_single_dict['m'] = 'метр'
me_single_dict['м²']='квадратный метр'
me_single_dict['м².']='квадратный метр'
me_single_dict['км²']='квадратный километр'
me_single_dict['км².']='квадратный километр'
me_single_dict['cm³'] = 'кубический сантиметр'
me_single_dict['см³'] = 'кубический сантиметр'
me_single_dict['см³.'] = 'кубический сантиметр'
me_single_dict['m³'] = 'кубический метр'
me_single_dict['м³'] = 'кубический метр'
me_single_dict['м³.'] = 'кубический метр'
me_single_dict['км/час']='километр в час'
me_single_dict['км/час.']='километр в час'
me_single_dict['л.']='литр'
me_single_dict['л']='литр'
me_single_dict['Л.']='литр'
me_single_dict['Л']='литр'
me_single_dict['l.']='литр'
me_single_dict['l']='литр'
me_single_dict['L.']='литр'
me_single_dict['L']='литр'
me_single_dict['%']='процент'
me_single_dict['%.']='процент'
me_single_dict['га']='гектар'
me_single_dict['га.']='гектар'
me_single_dict['Б']='байт'
me_single_dict['Б.']='байт'
me_single_dict['B']='байт'
me_single_dict['B.']='байт'
me_single_dict['б'] = 'байт'
me_single_dict['б.']='байт'
me_single_dict['b']='бит'
me_single_dict['b.']='бит'
me_single_dict['В.']='вольт'
me_single_dict['V.']='вольт'
me_single_dict['V']='вольт'
me_single_dict['К']='кельвина'
me_single_dict['К.']='кельвина'
me_single_dict['K']='кельвина'
me_single_dict['K.']='кельвина'
me_single_dict['А']='ампер'
me_single_dict['А.']='ампер'
me_single_dict['Гц']='герц'
me_single_dict['Гц.']='герц'
me_single_dict['Hz']='герц'
me_single_dict['Hz.']='герц'
me_single_dict['hz.']='герц'
me_single_dict['Вт.']='ватт'
me_single_dict['W']='ватт'
me_single_dict['W.']='ватт'
me_single_dict['с.']= 'секунда'
me_single_dict['с']= 'секунда'
me_single_dict['s.']= 'секунда'
me_single_dict['мин.']='минута'
me_single_dict['мин']='минута'
me_single_dict['min.']='минута'
me_single_dict['min']='минута'
me_single_dict['минут.']='минута'
me_single_dict['минут']='минута'
me_single_dict['ч']='час'
me_single_dict['ч.']='час'
me_single_dict['USD']='доллар сэ ш а'
me_single_dict['$']='доллар сэ ш а'
me_single_dict['т.']='тонна'
me_single_dict['т']='тонна'
me_single_dict['T.']='тонна'
me_single_dict['T']='тонна'
me_single_dict['тонн']='тонна'
me_single_dict['метр']='метр'
me_single_dict['сантиметр']='сантиметр'
me_single_dict['литр']='литр'
me_single_dict['процент']='процент'
me_single_dict['гектар']='гектар'
me_single_dict['байт']='байт'
me_single_dict['бит']='бит'
me_single_dict['вольт']='вольт'
me_single_dict['кельвин']='кельвин'
me_single_dict['ампер']='ампер'
me_single_dict['герц']='герц'
me_single_dict['ватт']='ватт'

me_gen_single_dict['Гбит/с']='гигабита в секунду'
me_gen_single_dict['кг'] = 'килограмма'
me_gen_single_dict['к.г.'] = 'килограмма'
me_gen_single_dict['кг.'] = 'килограмма'
me_gen_single_dict['мг'] = 'миллиграмма'
me_gen_single_dict['мг.'] = 'миллиграмма'
me_gen_single_dict['kg'] = 'килограмма'
me_gen_single_dict['k.g.'] = 'килограмма'
me_gen_single_dict['kg.'] = 'килограмма'
me_gen_single_dict['г.'] = 'грамма'
me_gen_single_dict['г'] = 'грамма'
me_gen_single_dict['см'] = 'сантиметра'
me_gen_single_dict['с.м.'] = 'сантиметра'
me_gen_single_dict['см.'] = 'сантиметра'
me_gen_single_dict['cm'] = 'сантиметра'
me_gen_single_dict['c.m.'] = 'сантиметра'
me_gen_single_dict['cm.'] = 'сантиметра'
me_gen_single_dict['м.'] ='метра'
me_gen_single_dict['м'] = 'метра'
me_gen_single_dict['m.'] ='метра'
me_gen_single_dict['m'] = 'метра'
me_gen_single_dict['м²']='квадратного метра'
me_gen_single_dict['м².']='квадратного метра'
me_gen_single_dict['км²']='квадратного километра'
me_gen_single_dict['км².']='квадратного километра'
me_gen_single_dict['cm³'] = 'кубического сантиметра'
me_gen_single_dict['см³'] = 'кубического сантиметра'
me_gen_single_dict['см³.'] = 'кубического сантиметра'
me_gen_single_dict['m³'] = 'кубического метра'
me_gen_single_dict['м³'] = 'кубического метра'
me_gen_single_dict['м³.'] = 'кубического метра'
me_gen_single_dict['км/час']='километра в час'
me_gen_single_dict['км/час.']='километра в час'
me_gen_single_dict['M.'] ='метра'
me_gen_single_dict['M'] = 'метра'
me_gen_single_dict['л.']='литра'
me_gen_single_dict['л']='литра'
me_gen_single_dict['Л.']='литра'
me_gen_single_dict['Л']='литра'
me_gen_single_dict['l.']='литра'
me_gen_single_dict['l']='литра'
me_gen_single_dict['L.']='литра'
me_gen_single_dict['L']='литра'
me_gen_single_dict['%']='процента'
me_gen_single_dict['%.']='процента'
me_gen_single_dict['га']='гектара'
me_gen_single_dict['га.']='гектара'
me_gen_single_dict['Б']='байта'
me_gen_single_dict['Б.']='байта'
me_gen_single_dict['B']='байта'
me_gen_single_dict['B.']='байта'
me_gen_single_dict['б'] = 'байта'
me_gen_single_dict['б.']='байта'
me_gen_single_dict['b']='бита'
me_gen_single_dict['b.']='бита'
me_gen_single_dict['В.']='вольта'
me_gen_single_dict['USD']='доллара сэ ш а'
me_gen_single_dict['$']='доллара сэ ш а'
me_gen_single_dict['V']='вольта'
me_gen_single_dict['V.']='вольта'
me_gen_single_dict['К']='кельвина'
me_gen_single_dict['К.']='кельвина'
me_gen_single_dict['K']='кельвина'
me_gen_single_dict['K.']='кельвина'
me_gen_single_dict['А']='ампера'
me_gen_single_dict['А.']='ампера'
me_gen_single_dict['Гц']='герца'
me_gen_single_dict['Гц.']='герца'
me_gen_single_dict['Hz']='герца'
me_gen_single_dict['Hz.']='герца'
me_gen_single_dict['hz.']='герца'
me_gen_single_dict['Вт.']='ватта'
me_gen_single_dict['W']='ватта'
me_gen_single_dict['W.']='ватта'
me_gen_single_dict['с.']= 'секунды'
me_gen_single_dict['с']= 'секунды'
me_gen_single_dict['s.']= 'секунды'
me_gen_single_dict['ч']='часа'
me_gen_single_dict['ч.']='часа'
me_gen_single_dict['мин.']='минуты'
me_gen_single_dict['мин']='минуты'
me_gen_single_dict['min.']='минуты'
me_gen_single_dict['min']='минуты'
me_gen_single_dict['минут.']='минуты'
me_gen_single_dict['минут']='минуты'
me_gen_single_dict['т.']='тонны'
me_gen_single_dict['т']='тонны'
me_gen_single_dict['T.']='тонны'
me_gen_single_dict['T']='тонны'
me_gen_single_dict['т.']='тонны'
me_gen_single_dict['т']='тонны'
me_gen_single_dict['T.']='тонны'
me_gen_single_dict['T']='тонны'
me_gen_single_dict['тонн']='тонны'
me_gen_single_dict['метр']='метра'
me_gen_single_dict['сантиметр']='сантиметра'
me_gen_single_dict['литр']='литра'
me_gen_single_dict['процент']='процента'
me_gen_single_dict['гектар']='гектара'
me_gen_single_dict['байт']='байта'
me_gen_single_dict['бит']='бита'
me_gen_single_dict['вольт']='вольта'
me_gen_single_dict['кельвин']='кельвина'
me_gen_single_dict['ампер']='ампера'
me_gen_single_dict['герц']='герца'
me_gen_single_dict['ватт']='ватта'


me_gen_plural_dict['Гбит/с']='гигабит в секунду'
me_gen_plural_dict['м²']='квадратных метров'
me_gen_plural_dict['м².']='квадратных метров'
me_gen_plural_dict['км²']='квадратных километров'
me_gen_plural_dict['км².']='квадратных километров'
me_gen_plural_dict['cm³'] = 'кубических сантиметров'
me_gen_plural_dict['см³'] = 'кубических сантиметров'
me_gen_plural_dict['см³.'] = 'кубических сантиметров'
me_gen_plural_dict['m³'] = 'кубических метров'
me_gen_plural_dict['м³'] = 'кубических метров'
me_gen_plural_dict['м³.'] = 'кубических метров'
me_gen_plural_dict['кг'] = 'килограмм'
me_gen_plural_dict['мг'] = 'миллиграмм'
me_gen_plural_dict['мг.'] = 'миллиграмм'
me_gen_plural_dict['к.г.'] = 'килограмм'
me_gen_plural_dict['кг.'] = 'килограмм'
me_gen_plural_dict['kg'] = 'килограмм'
me_gen_plural_dict['k.g.'] = 'килограмм'
me_gen_plural_dict['kg.'] = 'килограмм'
me_gen_plural_dict['г.']='грамм'
me_gen_plural_dict['г']='грамм'
me_gen_plural_dict['USD'] = 'долларов сэ ш а'
me_gen_plural_dict['$'] = 'долларов сэ ш а'
me_gen_plural_dict['см'] = 'сантиметров'
me_gen_plural_dict['с.м.'] = 'сантиметров'
me_gen_plural_dict['см.'] = 'сантиметров'
me_gen_plural_dict['cm'] = 'сантиметров'
me_gen_plural_dict['c.m.'] = 'сантиметров'
me_gen_plural_dict['cm.'] = 'сантиметров'
me_gen_plural_dict['м.'] ='метров'
me_gen_plural_dict['м'] = 'метров'
me_gen_plural_dict['m.'] ='метров'
me_gen_plural_dict['m'] = 'метров'
me_gen_plural_dict['M.'] ='метров'
me_gen_plural_dict['M'] = 'метров'
me_gen_plural_dict['л.']='литров'
me_gen_plural_dict['л']='литров'
me_gen_plural_dict['Л.']='литров'
me_gen_plural_dict['Л']='литров'
me_gen_plural_dict['l.']='литров'
me_gen_plural_dict['l']='литров'
me_gen_plural_dict['L.']='литров'
me_gen_plural_dict['L']='литров'
me_gen_plural_dict['%']='процентов'
me_gen_plural_dict['%.']='процентов'
me_gen_plural_dict['га']='гектаров'
me_gen_plural_dict['га.']='гектаров'
me_gen_plural_dict['Б']='байтов'
me_gen_plural_dict['Б.']='байтов'
me_gen_plural_dict['B']='байтов'
me_gen_plural_dict['B.']='байтов'
me_gen_plural_dict['б'] = 'байтов'
me_gen_plural_dict['б.']='байтов'
me_gen_plural_dict['b']='бит'
me_gen_plural_dict['b.']='бит'
me_gen_plural_dict['В.']='вольт'
me_gen_plural_dict['V.']='вольт'
me_gen_plural_dict['V']='вольт'
me_gen_plural_dict['К']='кельвин'
me_gen_plural_dict['К.']='кельвин'
me_gen_plural_dict['K']='кельвин'
me_gen_plural_dict['K.']='кельвин'
me_gen_plural_dict['А']='ампер'
me_gen_plural_dict['А.']='ампер'
me_gen_plural_dict['Гц']='герц'
me_gen_plural_dict['Гц.']='герц'
me_gen_plural_dict['Hz']='герц'
me_gen_plural_dict['Hz.']='герц'
me_gen_plural_dict['hz.']='герц'
me_gen_plural_dict['Вт.']='ватт'
me_gen_plural_dict['W.']='ватт'
me_gen_plural_dict['W']='ватт'
me_gen_plural_dict['с.']= 'секунд'
me_gen_plural_dict['с']= 'секунд'
me_gen_plural_dict['s.']= 'секунд'
me_gen_plural_dict['ч']='часов'
me_gen_plural_dict['ч.']='часов'
me_gen_plural_dict['мин.']='минут'
me_gen_plural_dict['мин']='минут'
me_gen_plural_dict['min.']='минут'
me_gen_plural_dict['min']='минут'
me_gen_plural_dict['минут.']='минут'
me_gen_plural_dict['минут']='минут'
me_gen_plural_dict['т.']='тонн'
me_gen_plural_dict['т']='тонн'
me_gen_plural_dict['T.']='тонн'
me_gen_plural_dict['T']='тонн'
me_gen_plural_dict['тонн']='тонн'
me_gen_plural_dict['метр']='метров'
me_gen_plural_dict['сантиметр']='сантиметров'
me_gen_plural_dict['км/час']='километров в час'
me_gen_plural_dict['км/час.']='километров в час'
me_gen_plural_dict['литр']='литров'
me_gen_plural_dict['процент']='процентов'
me_gen_plural_dict['гектар']='гектаров'
me_gen_plural_dict['байт']='байтов'
me_gen_plural_dict['бит']='бит'
me_gen_plural_dict['вольт']='вольт'
me_gen_plural_dict['кельвин']='кельвин'
me_gen_plural_dict['ампер']='ампер'
me_gen_plural_dict['герц']='герц'
me_gen_plural_dict['ватт']='ватт'



me_prep_exceptions=['ч.','ч','с']

me_prep_single_dict={}
me_prep_single_dict['кг'] = 'килограмме'
me_prep_single_dict['мг'] = 'миллиграмме'
me_prep_single_dict['к.г.'] = 'килограмме'
me_prep_single_dict['г.'] = 'грамме'
me_prep_single_dict['г'] = 'грамме'
me_prep_single_dict['кг.'] = 'килограмме'
me_prep_single_dict['kg'] = 'килограмме'
me_prep_single_dict['k.g.'] = 'килограмме'
me_prep_single_dict['kg.'] = 'килограмме'
me_prep_single_dict['см'] = 'сантиметре'
me_prep_single_dict['с.м.'] = 'сантиметре'
me_prep_single_dict['см.'] = 'сантиметре'
me_prep_single_dict['cm'] = 'сантиметре'
me_prep_single_dict['c.m.'] = 'сантиметре'
me_prep_single_dict['cm.'] = 'сантиметре'
me_prep_single_dict['м.'] ='метре'
me_prep_single_dict['м'] = 'метре'
me_prep_single_dict['m.'] ='метре'
me_prep_single_dict['m'] = 'метре'
me_prep_single_dict['M.'] ='метре'
me_prep_single_dict['M'] = 'метре'
me_prep_single_dict['л.']='литре'
me_prep_single_dict['л']='литре'
me_prep_single_dict['Л.']='литре'
me_prep_single_dict['Л']='литре'
me_prep_single_dict['l.']='литре'
me_prep_single_dict['l']='литре'
me_prep_single_dict['L.']='литре'
me_prep_single_dict['L']='литре'
me_prep_single_dict['%']='проценте'
me_prep_single_dict['%.']='проценте'
me_prep_single_dict['га']='гектаре'
me_prep_single_dict['га.']='гектаре'
me_prep_single_dict['Б']='байте'
me_prep_single_dict['Б.']='байте'
me_prep_single_dict['B']='байте'
me_prep_single_dict['B.']='байте'
me_prep_single_dict['б'] = 'байте'
me_prep_single_dict['б.']='байте'
me_prep_single_dict['b']='бите'
me_prep_single_dict['b.']='бите'
me_prep_single_dict['В.']='вольте'
me_prep_single_dict['V']='вольте'
me_prep_single_dict['V.']='вольте'
me_prep_single_dict['К']='кельвине'
me_prep_single_dict['К.']='кельвине'
me_prep_single_dict['K']='кельвине'
me_prep_single_dict['K.']='кельвине'
me_prep_single_dict['А']='ампере'
me_prep_single_dict['А.']='ампере'
me_prep_single_dict['Гц']='герце'
me_prep_single_dict['Гц.']='герце'
me_prep_single_dict['Hz']='герце'
me_prep_single_dict['Hz.']='герце'
me_prep_single_dict['hz.']='герце'
me_prep_single_dict['Вт.']='ватте'
me_prep_single_dict['W']='ватте'
me_prep_single_dict['W.']='ватте'
me_prep_single_dict['с.']= 'секунде'
me_prep_single_dict['s.']= 'секунде'
me_prep_single_dict['мин.']='минут'
me_prep_single_dict['мин']='минут'
me_prep_single_dict['min.']='минут'
me_prep_single_dict['min']='минут'
me_prep_single_dict['минут.']='минут'
me_prep_single_dict['минут']='минут'
me_prep_single_dict['т.']='тонне'
me_prep_single_dict['т']='тонне'
me_prep_single_dict['T.']='тонне'
me_prep_single_dict['T']='тонне'
me_prep_single_dict['тонн']='тонне'
me_prep_single_dict['метр']='метре'
me_prep_single_dict['сантиметр']='сантиметре'
me_prep_single_dict['литр']='литре'
me_prep_single_dict['процент']='проценте'
me_prep_single_dict['гектар']='гектаре'
me_prep_single_dict['байт']='байте'
me_prep_single_dict['бит']='бите'
me_prep_single_dict['вольт']='вольте'
me_prep_single_dict['кельвин']='кельвине'
me_prep_single_dict['ампер']='ампере'
me_prep_single_dict['герц']='герце'
me_prep_single_dict['ватт']='ватте'


me_prep_plural_dict={}
me_prep_plural_dict['мг'] = 'миллиграммах'
me_prep_plural_dict['кг'] = 'килограммах'
me_prep_plural_dict['к.г.'] = 'килограммах'
me_prep_plural_dict['кг.'] = 'килограммах'
me_prep_plural_dict['г.'] = 'граммах'
me_prep_plural_dict['г'] = 'граммах'
me_prep_plural_dict['kg'] = 'килограммах'
me_prep_plural_dict['k.g.'] = 'килограммах'
me_prep_plural_dict['kg.'] = 'килограммах'
me_prep_plural_dict['см'] = 'сантиметрах'
me_prep_plural_dict['с.м.'] = 'сантиметрах'
me_prep_plural_dict['см.'] = 'сантиметрах'
me_prep_plural_dict['cm'] = 'сантиметрах'
me_prep_plural_dict['c.m.'] = 'сантиметрах'
me_prep_plural_dict['cm.'] = 'сантиметрах'
me_prep_plural_dict['м.'] ='метрах'
me_prep_plural_dict['м'] = 'метрах'
me_prep_plural_dict['m.'] ='метрах'
me_prep_plural_dict['m'] = 'метрах'
me_prep_plural_dict['M.'] ='метрах'
me_prep_plural_dict['M'] = 'метрах'
me_prep_plural_dict['л.']='литрах'
me_prep_plural_dict['л']='литрах'
me_prep_plural_dict['Л.']='литрах'
me_prep_plural_dict['Л']='литрах'
me_prep_plural_dict['l.']='литрах'
me_prep_plural_dict['l']='литрах'
me_prep_plural_dict['L.']='литрах'
me_prep_plural_dict['L']='литрах'
me_prep_plural_dict['%']='процентах'
me_prep_plural_dict['%.']='процентах'
me_prep_plural_dict['га']='гектарах'
me_prep_plural_dict['га.']='гектарах'
me_prep_plural_dict['Б']='байтах'
me_prep_plural_dict['Б.']='байтах'
me_prep_plural_dict['B']='байтах'
me_prep_plural_dict['B.']='байтах'
me_prep_plural_dict['б'] = 'байтах'
me_prep_plural_dict['б.']='байтах'
me_prep_plural_dict['b']='битах'
me_prep_plural_dict['b.']='битах'
me_prep_plural_dict['В.']='вольтах'
me_prep_plural_dict['V.']='вольтах'
me_prep_plural_dict['V']='вольтах'
me_prep_plural_dict['К']='кельвинах'
me_prep_plural_dict['К.']='кельвинах'
me_prep_plural_dict['K']='кельвинах'
me_prep_plural_dict['K.']='кельвинах'
me_prep_plural_dict['А']='амперах'
me_prep_plural_dict['А.']='амперах'
me_prep_plural_dict['Гц']='герцах'
me_prep_plural_dict['Гц.']='герцах'
me_prep_plural_dict['Hz']='герцах'
me_prep_plural_dict['Hz.']='герцах'
me_prep_plural_dict['hz.']='герцах'
me_prep_plural_dict['Вт.']='ваттах'
me_prep_plural_dict['W.']='ваттах'
me_prep_plural_dict['W']='ваттах'
me_prep_plural_dict['с.']= 'секундах'
me_prep_plural_dict['s.']= 'секундах'
me_prep_plural_dict['мин.']='минут'
me_prep_plural_dict['мин']='минут'
me_prep_plural_dict['min.']='минут'
me_prep_plural_dict['min']='минут'
me_prep_plural_dict['минут.']='минут'
me_prep_plural_dict['минут']='минут'
me_prep_plural_dict['т.']='тоннах'
me_prep_plural_dict['т']='тоннах'
me_prep_plural_dict['T.']='тоннах'
me_prep_plural_dict['T']='тоннах'
me_prep_plural_dict['тонн']='тоннах'
me_prep_plural_dict['метр']='метрах'
me_prep_plural_dict['сантиметр']='сантиметрах'
me_prep_plural_dict['литр']='литрах'
me_prep_plural_dict['процент']='процентах'
me_prep_plural_dict['гектар']='гектарах'
me_prep_plural_dict['байт']='байтах'
me_prep_plural_dict['бит']='битах'
me_prep_plural_dict['вольт']='вольтах'
me_prep_plural_dict['кельвин']='кельвинах'
me_prep_plural_dict['ампер']='амперах'
me_prep_plural_dict['герц']='герцах'
me_prep_plural_dict['ватт']='ваттах'
me_prep_plural_dict['км².']='квадратных километрах'
me_prep_plural_dict['км²']='квадратных километрах'

me_inst_single_dict={}
me_inst_single_dict['кг'] = 'килограммом'
me_inst_single_dict['мг'] = 'миллиграммом'
me_inst_single_dict['г'] = 'граммом'
me_inst_single_dict['г.'] = 'граммом'
me_inst_single_dict['к.г.'] = 'килограммом'
me_inst_single_dict['кг.'] = 'килограммом'
me_inst_single_dict['kg'] = 'килограммом'
me_inst_single_dict['k.g.'] = 'килограммом'
me_inst_single_dict['kg.'] = 'килограммом'
me_inst_single_dict['см'] = 'сантиметром'
me_inst_single_dict['с.м.'] = 'сантиметром'
me_inst_single_dict['см.'] = 'сантиметром'
me_inst_single_dict['cm'] = 'сантиметром'
me_inst_single_dict['c.m.'] = 'сантиметром'
me_inst_single_dict['cm.'] = 'сантиметром'
me_inst_single_dict['м.'] ='метром'
me_inst_single_dict['м'] = 'метром'
me_inst_single_dict['m.'] ='метром'
me_inst_single_dict['m'] = 'метром'
me_inst_single_dict['M.'] ='метром'
me_inst_single_dict['M'] = 'метром'
me_inst_single_dict['л.']='литром'
me_inst_single_dict['л']='литром'
me_inst_single_dict['Л.']='литром'
me_inst_single_dict['Л']='литром'
me_inst_single_dict['l.']='литром'
me_inst_single_dict['l']='литром'
me_inst_single_dict['L.']='литром'
me_inst_single_dict['L']='литром'
me_inst_single_dict['%']='процентом'
me_inst_single_dict['%.']='процентом'
me_inst_single_dict['га']='гектаром'
me_inst_single_dict['га.']='гектаром'
me_inst_single_dict['Б']='байтом'
me_inst_single_dict['Б.']='байтом'
me_inst_single_dict['B']='байтом'
me_inst_single_dict['B.']='байтом'
me_inst_single_dict['б'] = 'байтом'
me_inst_single_dict['б.']='байтом'
me_inst_single_dict['b']='битом'
me_inst_single_dict['b.']='битом'
me_inst_single_dict['В.']='вольтом'
me_inst_single_dict['V']='вольтом'
me_inst_single_dict['V.']='вольтом'
me_inst_single_dict['К']='кельвином'
me_inst_single_dict['К.']='кельвином'
me_inst_single_dict['K']='кельвином'
me_inst_single_dict['K.']='кельвином'
me_inst_single_dict['А']='ампером'
me_inst_single_dict['А.']='ампером'
me_inst_single_dict['Гц']='герцом'
me_inst_single_dict['Гц.']='герцом'
me_inst_single_dict['Hz']='герцом'
me_inst_single_dict['Hz.']='герцом'
me_inst_single_dict['hz.']='герцом'
me_inst_single_dict['Вт.']='ваттом'
me_inst_single_dict['W']='ваттом'
me_inst_single_dict['W.']='ваттом'
me_inst_single_dict['с.']= 'секундой'
me_inst_single_dict['s.']= 'секундой'
me_inst_single_dict['мин.']='минуты'
me_inst_single_dict['мин']='минуты'
me_inst_single_dict['min.']='минуты'
me_inst_single_dict['min']='минуты'
me_inst_single_dict['минут.']='минуты'
me_inst_single_dict['минут']='минуты'
me_inst_single_dict['т.']='тонной'
me_inst_single_dict['т']='тонной'
me_inst_single_dict['T.']='тонной'
me_inst_single_dict['T']='тонной'
me_inst_single_dict['тонн']='тонной'
me_inst_single_dict['метр']='метром'
me_inst_single_dict['сантиметр']='сантиметром'
me_inst_single_dict['литр']='литром'
me_inst_single_dict['процент']='процентом'
me_inst_single_dict['гектар']='гектаром'
me_inst_single_dict['байт']='байтом'
me_inst_single_dict['бит']='битом'
me_inst_single_dict['вольт']='вольтом'
me_inst_single_dict['кельвин']='кельвином'
me_inst_single_dict['ампер']='ампером'
me_inst_single_dict['герц']='герцом'
me_inst_single_dict['ватт']='ваттом'


me_inst_plural_dict={}
me_inst_single_dict['мг'] = 'миллиграммами'
me_inst_plural_dict['кг'] = 'килограммами'
me_inst_plural_dict['к.г.'] = 'килограммами'
me_inst_plural_dict['г.'] = 'граммами'
me_inst_plural_dict['г'] = 'граммами'
me_inst_plural_dict['кг.'] = 'килограммами'
me_inst_plural_dict['kg'] = 'килограммами'
me_inst_plural_dict['k.g.'] = 'килограммами'
me_inst_plural_dict['kg.'] = 'килограммами'
me_inst_plural_dict['см'] = 'сантиметрами'
me_inst_plural_dict['с.м.'] = 'сантиметрами'
me_inst_plural_dict['см.'] = 'сантиметрами'
me_inst_plural_dict['cm'] = 'сантиметрами'
me_inst_plural_dict['c.m.'] = 'сантиметрами'
me_inst_plural_dict['cm.'] = 'сантиметрами'
me_inst_plural_dict['м.'] ='метрами'
me_inst_plural_dict['м'] = 'метрами'
me_inst_plural_dict['m.'] ='метрами'
me_inst_plural_dict['m'] = 'метрами'
me_inst_plural_dict['M.'] ='метрами'
me_inst_plural_dict['M'] = 'метрами'
me_inst_plural_dict['л.']='литрами'
me_inst_plural_dict['л']='литрами'
me_inst_plural_dict['Л.']='литрами'
me_inst_plural_dict['Л']='литрами'
me_inst_plural_dict['l.']='литрами'
me_inst_plural_dict['l']='литрами'
me_inst_plural_dict['L.']='литрами'
me_inst_plural_dict['L']='литрами'
me_inst_plural_dict['%']='процентами'
me_inst_plural_dict['%.']='процентами'
me_inst_plural_dict['га']='гектарами'
me_inst_plural_dict['га.']='гектарами'
me_inst_plural_dict['Б']='байтами'
me_inst_plural_dict['Б.']='байтами'
me_inst_plural_dict['B']='байтами'
me_inst_plural_dict['B.']='байтами'
me_inst_plural_dict['б'] = 'байтами'
me_inst_plural_dict['б.']='байтами'
me_inst_plural_dict['b']='битами'
me_inst_plural_dict['b.']='битами'
me_inst_plural_dict['В.']='вольтами'
me_inst_plural_dict['V.']='вольтами'
me_inst_plural_dict['V']='вольтами'
me_inst_plural_dict['К']='кельвинами'
me_inst_plural_dict['К.']='кельвинами'
me_inst_plural_dict['K']='кельвинами'
me_inst_plural_dict['K.']='кельвинами'
me_inst_plural_dict['А']='амперами'
me_inst_plural_dict['А.']='амперами'
me_inst_plural_dict['Гц']='герцами'
me_inst_plural_dict['Гц.']='герцами'
me_inst_plural_dict['Hz']='герцами'
me_inst_plural_dict['Hz.']='герцами'
me_inst_plural_dict['hz.']='герцами'
me_inst_plural_dict['Вт.']='ваттами'
me_inst_plural_dict['W.']='ваттами'
me_inst_plural_dict['W']='ваттами'
me_inst_plural_dict['с.']= 'секундами'
me_inst_plural_dict['s.']= 'секундами'
me_inst_plural_dict['мин.']='минут'
me_inst_plural_dict['мин']='минут'
me_inst_plural_dict['min.']='минут'
me_inst_plural_dict['min']='минут'
me_inst_plural_dict['минут.']='минут'
me_inst_plural_dict['минут']='минут'
me_inst_plural_dict['т.']='тоннами'
me_inst_plural_dict['т']='тоннами'
me_inst_plural_dict['T.']='тоннами'
me_inst_plural_dict['T']='тоннами'
me_inst_plural_dict['тонн']='тоннами'
me_inst_plural_dict['метр']='метрами'
me_inst_plural_dict['сантиметр']='сантиметрами'
me_inst_plural_dict['литр']='литрами'
me_inst_plural_dict['процент']='процентами'
me_inst_plural_dict['гектар']='гектарами'
me_inst_plural_dict['байт']='байтами'
me_inst_plural_dict['бит']='битами'
me_inst_plural_dict['вольт']='вольтами'
me_inst_plural_dict['кельвин']='кельвинами'
me_inst_plural_dict['ампер']='амперами'
me_inst_plural_dict['герц']='герцами'
me_inst_plural_dict['ватт']='ваттами'
me_inst_plural_dict['м²']='квадратными метрами'

def measurise(line):
      number=str(line.split(' ')[0])
      splitted = number
      if line.split(' ')[1] in me_dict:
        case=0
        number=str(line.split(' ')[0])
        measurement = line.split(' ')[1]
        
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      if splitted[-1] == '1' and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
        a=num2words(int(number), lang='ru')
        b = me_single_dict[measurement]
        if me_dict[measurement][1]=='f':
          a = a.split(' ')
          a[-1]='одна'
          a = ' '.join(a)
       
      elif (splitted[-1] == '2') and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
        a=num2words(int(number), lang='ru')
        #print(a)
        b = me_gen_single_dict[measurement]
        if me_dict[measurement][1]=='f':
         
          a = a.split(' ')
          #print(a)
          a[-1]='две'
          a = ' '.join(a)
      elif ((splitted[-1] == '3') or (splitted[-1] == '4')) and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
        a=num2words(int(number), lang='ru')
        b = me_gen_single_dict[measurement]
     
      else:
        a=num2words(int(number), lang='ru')
        b = me_gen_plural_dict[measurement]
     
      if len(number)==4 and number[0]=='1':
       
        a = a.split(' ')
        a = a[1:]
        a = ' '.join(a)
          #print(a)
      
      if case ==0:
        
      
        line=a +' '+b
        if 'целых и ноль десятых' in line:
          line=line.replace('целых и ноль десятых ','')
        
      elif case==1:
        line=a +' '+prefix+b
        
        if 'целых и ноль десятых' in line:
          line=line.replace('целых и ноль десятых ','')
        
      else:
        print('notsure')
      return line

def genitive_measure_comma(line):
      number = line.split(' ')[0]
      number=decimal_genitive(number)
      
      if line.split(' ')[1] in me_dict:
        case=0
        
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]

      b=me_gen_single_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = number + ' '+b
      if 'целых и ноль десятых' in line:
          line=line.replace('целых и ноль десятых ','')
      return line
def prepositional_measure_comma(line):
      number = line.split(' ')[0]
      number=decimal_genitive(number)
      
      if line.split(' ')[1] in me_dict:
        case=0
        
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]

      b=me_prep_plural_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = number + ' '+b
      if 'целых и ноль десятых' in line:
          line=line.replace('целых и ноль десятых ','')
      if 'километрах' in line:
        line=line.replace('километрах', 'километра')
      return line    
def trans(word):
  #print(word)
  #words=map(''.join, itertools.product(*((c.upper(), c.lower()) for c in word)))
  l = len(word)
  op=0
  tr=0
 
  while op<l:
      if word[:op] in res and word[op:] in res:
        srtd1 = sorted(res[word[:op]].items(), key=operator.itemgetter(1), reverse=True)
        srtd1=srtd1[0][0]
        srtd1 = srtd1.replace('_letter_latin','').replace('_latin','').lower()
        srtd2 = sorted(res[word[op:]].items(), key=operator.itemgetter(1), reverse=True)
        srtd2=srtd2[0][0]
        srtd2 = srtd2.replace('_letter_latin','').replace('_latin','').lower()
        tr = srtd1 + ' ' + srtd2
        print('FOUND ONE!!')
        print(tr)
      op+=1
      
  if tr==0:
    op=0
    j=1
    while op<l:
      while j<l-op-1:
       if word[:op] in res and word[op:op+j] in res and word[op+j:] in res:
         srtd1 = sorted(res[word[:op]].items(), key=operator.itemgetter(1), reverse=True)
         srtd1=srtd1[0][0]
         srtd1 = srtd1.replace('_letter_latin','').replace('_latin','').lower()
         srtd2 = sorted(res[word[op:op+j]].items(), key=operator.itemgetter(1), reverse=True)
         srtd2=srtd2[0][0]
         srtd2 = srtd2.replace('_letter_latin','').replace('_latin','').lower()
         srtd3 = sorted(res[word[op+j:]].items(), key=operator.itemgetter(1), reverse=True)
         srtd3=srtd3[0][0]
         srtd3 = srtd3.replace('_letter_latin','').replace('_latin','').lower()
         tr = srtd1 + ' ' + srtd2 + ' ' + srtd3
         print('FOUND ONE!!')
         print(tr)
       j+=1
      op+=1
  if tr==0:
    op = 0
    a=[]
    while op<l:
      b = translit(word[op], 'ru').lower() + '_trans'
      a.append(b)
      op+=1
    tr = ' '.join(a)
  
  return tr



web_endings3={}
web_endings4={}
web_endings5={}
web_endings6={}
web_endings4['.org']='точка о_trans р_trans г_trans'
web_endings4['.com']='точка к_trans о_trans м_trans'
web_endings3['.ru']='точка р_trans у_trans'
web_endings4['.net']='точка н_trans е_trans т_trans'
web_endings3['.ba']='точка b a'
web_endings4['.ruz']='точка р_trans у_trans с_trans'
web_endings3['.at'] = 'точка a t'
web_endings4['.doc']='точка д_trans о_trans к_trans'
web_endings4['.xls']='точка x l s'
web_endings4['.pdf']='точка p d f'
web_endings3['.ua']='точка u a'
web_endings5['.html'] = 'точка h t m l'
web_endings6['.co.uk']='точка к_trans о_trans точка u k'
web_endings3['.us']='точка u s'
web_endings3['.fi']='точка f i'
web_endings3['.de']='точка d e'
web_endings3['.cz']='точка c z'
web_endings3['.fr']='точка f r'
def webby_1(line):
  if any(x in line for x in web_endings3):
    front = line[:-4]
    end = line[-4:]
    end = web_endings3[end]
  elif any(x in line for x in web_endings4):
    front = line[:-5]
    end = line[-5:]
    end = web_endings4[end]
  elif any(x in line for x in web_endings5):
    front = line[:-6]
    end = line[-6:]
    end = web_endings5[end]
  elif any(x in line for x in web_endings6):
    front = line[:-7]
    end = line[-7:]
    end = web_endings6[end]
  if 'www.' in front:
    front = front.replace('www.','')
  if front in res:
    front = sorted(res[front].items(), key=operator.itemgetter(1), reverse=True)
    front=front[0][0]
  else:
    front = trans(front)
  if 'www.' in line:
    front = 'w w w точка' + ' ' + front
  line = front + ' ' + end
  return line
total = 0
changes = 0
out = open(os.path.join(SUBM_PATH, 'ru_testing_do_oye_newidea.csv'), "w", encoding='UTF8')
dub = open(os.path.join(SUBM_PATH, 'sdub.csv'), "w", encoding='UTF8')
notin = open(os.path.join(SUBM_PATH, 'notinnn.csv'), "w", encoding='UTF8')
out.write('"id","after"\n')
test = open(os.path.join(INPUT_PATH, "ru_test_2.csv"), encoding='UTF8')
lines= test.read().splitlines()

for i in range(1,len(lines)):
    line = lines[i]
    
    if line == '':
        break

    pos = line.find(',')
    i1 = line[:pos]
    line = line[pos + 1:]

    pos = line.find(',')
    i2 = line[:pos]
    line = line[pos + 1:]

    line = line[1:-1]
    remove_colon = line.replace(':', '')
    remove_hyphen = line.replace('-', '')
    #print(remove_colon)
    out.write('"' + i1 + '_' + i2 + '",')
    
    #if line in ddict:
    #  
    #  next_line = lines[i+1]
    #  if next_line == '':
    #    break

     # pos = next_line.find(',')
     # i1 = next_line[:pos]
     # next_line = next_line[pos + 1:]
#
#      pos = next_line.find(',')
#      i2 = next_line[:pos]
#      next_line = next_line[pos + 1:]

 #     next_line = next_line[1:-1]
     # if next_line =='век':
     #   line = ddict[line]
     #   print(line)
        
        
    #    
    #    line = ddict[line]
    #    out.write('"' + line +'"')
      
      
    #if line in fdict:
    #  line = fdict[line]
    prev_line = lines[i-1]
    if prev_line == '':
        break

    pos = prev_line.find(',')
    #i1 = prev_line[:pos]
    prev_line = prev_line[pos + 1:]

    pos = prev_line.find(',')
    #i2 = prev_line[:pos]
    prev_line = prev_line[pos + 1:]
    
    
    prev_line = prev_line[1:-1]
    ##PREV PREV
    prev_prev_line = lines[i-2]
    if prev_prev_line == '':
        break

    pos = prev_prev_line.find(',')
    #i1 = prev_prev_line[:pos]
    prev_prev_line = prev_prev_line[pos + 1:]

    pos = prev_prev_line.find(',')
    #i2 = prev_prev_line[:pos]
    prev_prev_line = prev_prev_line[pos + 1:]
    
    
    prev_prev_line = prev_prev_line[1:-1]
    
    prev_prev_prev_line = lines[i-3]
    if prev_prev_prev_line == '':
        break

    pos = prev_prev_prev_line.find(',')
    #i1 = prev_prev_prev_line[:pos]
    prev_prev_prev_line = prev_prev_prev_line[pos + 1:]

    pos = prev_prev_prev_line.find(',')
    #i2 = prev_prev_prev_line[:pos]
    prev_prev_prev_line = prev_prev_prev_line[pos + 1:]
    
    
    prev_prev_prev_line = prev_prev_prev_line[1:-1]
    prev_prev_prev_prev_line = lines[i-4]
    if prev_prev_prev_prev_line == '':
        break

    pos = prev_prev_prev_prev_line.find(',')
    #i1 = prev_prev_prev_line[:pos]
    prev_prev_prev_prev_line = prev_prev_prev_prev_line[pos + 1:]

    pos = prev_prev_prev_prev_line.find(',')
    #i2 = prev_prev_prev_line[:pos]
    prev_prev_prev_line = prev_prev_prev_line[pos + 1:]
    
    
    prev_prev_prev_prev_line = prev_prev_prev_prev_line[1:-1]
    if i <len(lines)-1:
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
    else:
      next_line=prev_line
      
      
    if i <len(lines)-2:
      next_next_line = lines[i+2]
      if next_next_line == '':
        break

      pos = next_next_line.find(',')
      #i1 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]

      pos = next_next_line.find(',')
      #i2 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]
    
    
      next_next_line = next_next_line[1:-1] 
    else:
      next_next_line=prev_line
    if i <len(lines)-3:
      next_next_next_line = lines[i+3]
      if next_next_next_line == '':
        break

      pos = next_next_next_line.find(',')
      #i1 = next_next_next_line[:pos]
      next_next_next_line = next_next_next_line[pos + 1:]

      pos = next_next_next_line.find(',')
      #i2 = next_next_next_line[:pos]
      next_next_next_line = next_next_next_line[pos + 1:]
    
    
      next_next_next_line = next_next_next_line[1:-1] 
    else:
      next_next_next_line=prev_line
    #NEXT
   
      #elif (((len(line)>1 and line[-1] in me_dict and line[:-1].isdigit() ) or (len(line)>2 and line[-2] in me_prefix and line[-1] in me_dict and line[:-2].isdigit()) or (len(line)>3 and line[-3:-1] in   me_prefix and line[-1] in me_dict and line[-3:].isdigit()) or (len(line)>3 and line[-3] in me_prefix and line[-2:] in me_dict and line[:-3].isdigit()) ) and any((y==prev_line for y in genitive_prepositions)or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния') and prev_line!='с'):
   #(line.isdigit()) and ((prev_line == 'с') or (prev_line == 'до') or (prev_line =='от') or (prev_line=='после'))
    ## PREVIOUS LINE A GENITIVE PREPOSITION AND WHOLE LINE NUMBERS
    #if line.isupper() and not ('1' in line or '2' in line or '3' in line or '4' in line or '5' in line or '6' in line or '7' in line or '8' in line or '9' in line or '0' in line) and line in res and len(line)>1 and len(sorted(res[line].items(), key=operator.itemgetter(1), reverse=True))>1: #and '.' in line:
      
        
          ##print('longer')
          ##print(line)
          ##print(prev_line)
          ##print(sorted(res[line].items(), key=operator.itemgetter(1), reverse=True))
      ##srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
      ##line=srtd[0][0]
    
      #if line in ddict:
        
        #number=str(ddict[line])
        #if any(x==prev_line for x in genitive_prepositions):
         #line=geni(number)
        #elif any(y==prev_line for y in prepositional_prepositions):
          #line=prepi(number)
        #elif any(z==prev_line for z in dative_prepositions):
          #line=dati(number)
        #elif any(v==prev_line for v in accusative_prepositions):
          #line=acci(number)
        #elif any(w==prev_line for w in nominative_prepositions):
          #line=nomi(number)
        #else:
          #line=sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)[0][0]
        
        
       
        
         
      #else:
        #line=sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)[0][0]
       ##print('the line')
        ##print(line)
        ##print(sorted(res[line].items(), key=operator.itemgetter(1), reverse=True))
        
      
      
      #double.write('"' + line + '"')
      #out.write('"' + line + '"')
   
    
    #if (line.isdigit()  and (any(x == prev_line for x in genitive_prepositions))and len(line)==4):
      #line=geni(line)
      #out.write('"' + line +'"')
      
     ##CHEATS?
    #if len(line.split(' '))==2 and line.split(' ')[0].isdigit() and line.split(' ')[1].isdigit():
      #line = line.replace(' ','')
    #if len(line.split(' '))==3 and line.split(' ')[0].isdigit() and line.split(' ')[1].isdigit() and line.split(' ')[2].isdigit():
      #line = line.replace(' ','')
    #if len(line.split(' '))==2 and line.split(' ')[0].isdigit() and 'тыс.' in line:
      #line=line.replace('тыс.','тысяч')
    #if i==5211:
    if line.isdigit() and line[0]=='0' and len(line)>1:
      number = line
      a=[]
      ii=0
      while ii < len(number):
        single = num2words(number[ii],lang='ru')
        a.append(single)
        ii+=1
      line = ' '.join(a)
      out.write('"' + line +'"')  
      
      
    elif (next_line+'BREAK'+prev_line +'BREAK'+line+ 'BREAK' + prev_prev_line  in cardinal5_dict) and ((line.replace(' ','').isdigit())):
      srtd = sorted(cardinal5_dict[next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      #print('cheat: cardinalstraight')
      srtd = srtd[0][0]
      line = srtd
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+ 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in cardinal5_dict)and line.replace(' ','').isdigit():
      srtd = sorted(cardinal5_dict[next_next_line + 'BREAK'+ next_line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      srtd=srtd[0][0]
      
      if srtd=='ти':
        line = genitivise(line)
        print('OYE')
      elif srtd=='ью':
        line = instrumentalise(line)
        print('OGO')
      elif srtd=='ах':
        line = prepositionalise(line)
      elif srtd=='ому':
        line = dativise(line)
      else:
        line = accusativise(line)
      
     
      out.write('"' + line +'"')
    elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in roman5_dict):
      srtd = sorted(roman5_dict[next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      #print('cheat: roman')
      srtd = srtd[0][0]
      line = srtd
      line = line.replace('_letter_latin', '_latin')
      roman+=1
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len2_dict):
      srtd = sorted(year_len2_dict[next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      srtd = srtd[0][0]
      line = srtd
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len3_dict):
      srtd = sorted(year_len3_dict[next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      srtd = srtd[0][0]
      line = srtd
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len4_dict):
      srtd = sorted(year_len4_dict[next_next_line + 'BREAK'+ next_line+'BREAK'+line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      srtd = srtd[0][0]
      line = srtd
      out.write('"' + line +'"')
    
    #elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in decimal_dict):
      #srtd = sorted(decimal_dict[next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      #print('cheat: decimal')
      #srtd = srtd[0][0]
      #line = srtd
      #out.write('"' + line +'"')
    #elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in minus_dict):
      #srtd = sorted(minus_dict[next_next_line+ 'BREAK'+ next_line+'BREAK'+line+'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      #print('cheat:minus')
      #srtd = srtd[0][0]
      #line = srtd
      #out.write('"' + line +'"')
     ##NUMBERS FUN
    
    elif line.isdigit() and line[0]=='0' and len(line)>1:
      number = line
      a=[]
      ii=0
      while ii < len(number):
        single = num2words(number[ii],lang='ru')
        a.append(single)
        ii+=1
      line = ' '.join(a)
      out.write('"' + line +'"')
    elif line.isdigit() and len(line)==1 and line in powers_dict:
      line = powers_dict[line]
      out.write('"' + line +'"')
    elif (prev_line==':' or prev_line==' :' or prev_line==': ') and (next_line==',' or next_line==' ,' or next_line==', ' or next_line=='—' or next_line=='— ' or next_line==' —') and line.isdigit():
      if len(line)>4:
        a=[]
        op=0
        while op<len(line):
          a.append(accusativise(line[op]))
          op+=1
        line = ' '.join(a)
      else:
        line = line
      out.write('"' + line +'"')
    elif (prev_line==':' or prev_line==' :' or prev_line==': ') and (next_line=='.' or next_line==' .' or next_line=='. ' or next_line==',' or next_line==' ,' or next_line==', ' or next_line=='—' or next_line=='— ' or next_line==' —') and len(line.split(' '))==2 and line.split(' ')[0].isdigit() :
        if line.split(' ')[1].isdigit():
          line=line.replace(' ','')
          a=[]
          op=0
          while op<len(line):
            a.append(accusativise(line[op]))
            op+=1
          line = ' '.join(a)
        else:
         line = line
        out.write('"' + line +'"')
    elif prev_line==')' and next_line==':' and line.replace('%','').isdigit():
      line = line
      out.write('"' + line +'"')
    elif prev_line==')' and next_line=='-' and line.replace('%','').isdigit():
      line = line
      out.write('"' + line +'"')
    elif prev_line=='—' and next_line==':' and line.replace('%','').isdigit():
      line = line
      out.write('"' + line +'"')
    elif line.count('%')==1 and line.replace('%','').replace(' ','').isdigit() and prev_prev_line.isdigit() and prev_line=='-' and prev_prev_prev_line=='от' and len(line.replace('%','').replace(' ',''))<3:
      line = line
      out.write('"' + line +'"')
    elif next_next_line.count('%')==1 and next_next_line.replace('%','').replace(' ','').isdigit() and line.isdigit() and prev_line=='от':
      line = line
      out.write('"' + line +'"')
    elif prev_line=='от' and 'г' in line and line.replace('г','').replace('.','').replace(' ','').isdigit():
      
      line = line.replace('г','').replace('.','').replace(' ','')
      line = genitivise(line)
      g = 'грамм'
      line = line + ' ' + g
      out.write('"' + line +'"')
    elif prev_prev_line=='С' and prev_line=='-' and line.isdigit():
      line = instrumentalise(line)
      out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (prev_line=='-' or prev_line==' -' or prev_line==' - ' or prev_line=='- ')and prev_prev_line.isdigit() and (prev_prev_prev_line=='от'):
      
      line = line
      out.write('"' + line+'"')
    elif line.isdigit() and (prev_line=='.' or prev_line=='. ' or prev_line==' .' or prev_line==',' or prev_line==', ' or prev_line==' ,') and (next_line[-3:]=='ыми' or next_line[-2:]=='ым'):
      
      line=genitivise(line)
      out.write('"' + line+'"')
    elif prev_line=='от' and line.isdigit() and (next_line=='-' or next_line==' - ' or next_line=='- ' or next_line==' -')and len(next_next_line.split(' '))==2:
      if len(line)==1:
        line = line
      else:
        a = []
        op=0
        while op<len(line):
          a.append(accusativise(line[op]))
          op+=1
        line = ' '.join(a)
      out.write('"' + line+'"')
    elif prev_line==',' and any(x==prev_prev_line for x in prepositional_prepositions) and line.isdigit():
      line = prepositionalise(line)
      out.write('"' + line+'"')
    elif prev_line=='от' and line.isdigit() and (next_line=='—' or next_line==' — ' or next_line=='— ' or next_line==' —'):
      line = genitivise(line)
      out.write('"' + line+'"')
    elif line.isdigit() and next_line=='—' and next_next_line.isdigit():
      line = accusativise(line)
      out.write('"' + line +'"')
    #elif any(x==prev_line for x in full_prepositions) and next_line[-2:]=='ых' and line.isdigit() and line!='на':
    #  line = genitivise(line)
    #  out.write('"' + line +'"')
    elif line.isdigit() and len(line)==1 and line[-1]=='1' and prev_line[-1]=='ы':
      line = 'одной'
      out.write('"' + line +'"')
    elif line.isdigit() and any(x==next_line for x in gen_months):
      line = genitivise(line)
      out.write('"' + line +'"')
    elif prev_line=='после' and line.isdigit() and len(line)==4:
      line = accusativise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and all(x!=prev_line for x in full_prepositions) and next_line[-2:]=='ях':
      line =prepositionalise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and (next_line=='летний' or next_line=='летия' or next_line=='летию' or next_line=='летнего' or next_line=='летие'):
      line=genitivise(line)
      out.write('"' + line +'"')
    
    elif line.isdigit() and next_line[-2:]=='-х' and next_line[0]=='-' and next_line[0:-2].isdigit():
      line = line
      out.write('"' + line +'"')
    elif line[-2:]=='-х' and line[0]=='-' and line[0:-2].isdigit() and prev_line.isdigit():
      line=line
      out.write('"' + line +'"') 
    elif line.isdigit() and '-х' in next_line and 'год' in next_next_line :
      line = line
      out.write('"' + line +'"')
    elif prev_line.isdigit() and '-х' in line and 'год' in next_line and '—' not in line:
      line = line
      out.write('"' + line +'"')
    elif line.isdigit() and prev_line==':' and next_line==':':
      line = line
      out.write('"' + line +'"')
    elif line.isdigit() and next_line=='года' and all(x!=prev_line for x in full_prepositions):
      line= genitivise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and next_line=='годам' and all(x!=prev_line for x in full_prepositions):
      line=dativise(line)
      out.write('"' + line +'"')
    
    elif line.isdigit() and next_line=='годов':
      genitivise(line)
      out.write('"' + line +'"')
   
    elif line.isdigit() and prev_line[-2:]=='ми' and next_line[-2:]=='ми':
      line=instrumentalise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and prev_line[-2:]=='ым' and next_line[-2:]=='ым':
      line=dativise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and prev_line[-2:]=='ых' and next_line[-2:]=='ых':
      line = accusativise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and next_line=='.' and (prev_line=='.' or prev_line==',')and any(x==prev_prev_line for x in prepositional_prepositions):
      line = prepositionalise(line)
      out.write('"' + line +'"')
      
      
      
      
      ##DIGITISE
    elif line.isdigit() and line[0]=='0' and len(line)>1:
      number = line
      a=[]
      ii=0
      while ii < len(number):
        single = num2words(number[ii],lang='ru')
        a.append(single)
        ii+=1
      line = ' '.join(a)
      out.write('"' + line +'"')
       
    elif (line.isdigit()  and ((any(x == prev_line for x in genitive_prepositions)) or prev_line=='ой'or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния')):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      
      next_next_line = lines[i+2]
      if next_next_line == '':
        break

      pos = next_next_line.find(',')
      #i1 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]

      pos = next_next_line.find(',')
      #i2 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]
    
    
      next_next_line = next_next_line[1:-1]
      
      if (prev_line=='от' or prev_line=='От') and (len(next_next_line.split(' '))==2) and (next_next_line.split(' ')[0].isdigit()) and  not next_next_line.split(' ')[1].isdigit()and next_line=='-':
        line = line
          
      
        a = []
        op=0
        while op<len(line):
          a.append(num2words(line[op],lang='ru'))
          op+=1
        line = ' '.join(a)
      elif (next_line.replace('-','').isdigit() or next_line.replace('—','').isdigit()) and (next_line[0]=='-' or next_line[0]=='—'):
        line = geni(line)
      elif any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
        line=geni(line)
      elif prev_line=='с':
        if line=='1':
          line = instrumentalise(line)
        elif next_line=='—' and len(line)!=4:
          line = accusativise(line)
        elif next_line=='—' and len(line)==4:
          line = instrumentalise(line)
        elif next_line[-2:]=='ым' or next_line[-2:]=='ов' or next_line[-2:]=='им' or next_line[-2:]=='ев' or next_line[-2:]=='ии' or ('года' in next_line):
          line = genitivise(line)
        elif (next_line[-1:]=='м' or next_line[-2:]=='ми'):
          line = instrumentalise(line)
        elif next_line=='до' and  not next_next_line.isdigit():
          line = genitivise(line)
        elif (any(y==next_line for y in genitive_prepositions) or any(x==next_line for x in po)) and len(line)==4: 
          line=geni(line)
        elif (any(x==next_line for x in po)):
          line = instrumentalise(line)
        elif(any(x==next_line for x in genitive_prepositions)):
          line = genitivise(line)
        elif ('лет' in next_line and len(next_line)>3)or('века' ==next_line)or(next_line[-2:]=='го'):
          line = genitivise(line)
        elif next_line.count('-')==1 and next_line[0]=='-' and next_line[1:].isdigit():
          line = genitivise(line)
        else:
          line = instrumentalise(line)
      elif prev_line=='С':
         
        if (next_line[-1:]=='м' or next_line[-2:]=='ми'):
          line = instrumentalise(line)
        elif next_line=='—' and len(line)==4:
          line = instrumentalise(line)
        elif next_line=='и' or next_line=='на':
          line = instrumentalise(line)
        elif next_line[-1:]=='и':
          line = genitivise(line)
        elif ('лет' in next_line and len(next_line)>3)or('века' ==next_line)or(next_line[-2:]=='го') or next_line=='года':
          line = genitivise(line)
        elif next_line=='до' and  not next_next_line.isdigit():
          line = genitivise(line)
        elif (any(y==next_line for y in genitive_prepositions) or any(x==next_line for x in po)) and len(line)==4: 
          line=genitivise(line)
        else:
          line=accusativise(line)
      
      elif any(y==next_line for y in po):
        next_next_line = lines[i+2]
        if next_next_line == '':
          break

        pos = next_next_line.find(',')
       # i1 = next_next_line[:pos]
        next_next_line = next_next_line[pos + 1:]

        pos = next_next_line.find(',')
        #i2 = next_next_line[:pos]
        next_next_line = next_next_line[pos + 1:]
    
    
        next_next_line = next_next_line[1:-1]
        
        next_next_next_line = lines[i+3]
        if next_next_next_line == '':
          break

        pos = next_next_next_line.find(',')
        #i1 = next_next_next_line[:pos]
        next_next_next_line = next_next_next_line[pos + 1:]

        pos = next_next_next_line.find(',')
        #i2 = next_next_next_line[:pos]
        next_next_next_line = next_next_next_line[pos + 1:]
    
    
        next_next_next_line = next_next_next_line[1:-1]
        if any(z==next_next_next_line for z in years_abbreviations) or any(y==next_next_next_line for y in gen_months) or any(z==next_next_next_line for z in prep_months) and next_next_line.isdigit():
            line=geni(line)
        
      
        elif (next_line[-1:]=='м' or next_line[-2:]=='ми') and any(x==prev_line for x in crossover):
          ipip+=1
          line = instrumentalise(line)
        else:
          line=genitivise(line)
      else:
        line=genitivise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and ((prev_line[-2:]=='ых'or prev_line[-2:]=='их') and len(prev_line)>3):
      line = genitivise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and (prev_line=='и') and len(line)!=4 and len(prev_prev_line)!=4 and prev_prev_line.isdigit() and any(x==prev_prev_prev_line for x in full_prepositions):
      if any(x==prev_prev_prev_line for x in genitive_prepositions):
        line=genitivise(line)
      elif any(x==prev_prev_prev_line for x in instrumental_prepositions):
        line=instrumentalise(line)
      elif any(x==prev_prev_prev_line for x in dative_prepositions):
        line=dativise(line)
      elif any(x==prev_prev_prev_line for x in accusative_prepositions):
        line=accusativise(line)
      elif any(x==prev_prev_prev_line for x in prepositional_prepositions):
        line=prepositionalise(line)
      elif any(x==prev_prev_prev_line for x in nominative_prepositions):
        line=accusativise(line)
      fem+=1
      out.write('"' + line +'"')
    
    elif line.isdigit() and (prev_line=='и') and len(line)==4 and next_line=='годов' and (any(x==prev_prev_prev_line for x in genitive_prepositions) or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния'):
      line = genitivise(line)
      out.write('"' + line +'"')
    ##NUMBERS FUN
    elif line.isdigit() and prev_line=='и' and len(line)!=4:
      if next_line[-2:]=='ам':
        line = dativise(line)
      elif next_line[-2:]=='ах':
        line = prepositionalise(line)
      elif next_line[-2:]=='их':
        line = genitivise(line)
      elif next_line[-3:]=='дов' or next_line[-3:]=='ков'or next_line[-1]=='ы' or next_line[-2:]=='ия':
        line = genitivise(line)
      elif next_line[-2:]=='ми':
        line = instrumentalise(line)
      else:
        line = accusativise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and next_line[-2:]=='ам':
      line = dativise(line)
      out.write('"' + line +'"')
    #elif (line.isdigit()  and (any(x == prev_line for x in prepositional_prepositions)) and len(line)==4):
      #line=prepi(line)
      #out.write('"' + line +'"')  
    elif line.isdigit() and prev_line=='.' and next_line=='.' and any(x==prev_prev_line for x in prepositional_prepositions):
      line = prepositionalise(line)
      out.write('"' + line +'"')
    elif line.isdigit() and (any(x == prev_line for x in prepositional_prepositions)):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      
      next_next_line = lines[i+2]
      if next_line == '':
        break

      pos = next_next_line.find(',')
      i1 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]

      pos = next_next_line.find(',')
      #i2 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]
    
    
      next_next_line = next_next_line[1:-1]
      
      if any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
        line=prepositionalise(line)##ISTHISTRUE
      elif next_line[-3:]=='ами':
        line = instrumentalise(line)
      elif (next_line.replace('-','').replace(' ','').isdigit() or next_line.replace('—','').replace(' ','').isdigit()) and (next_line[0]=='-' or next_line[0]=='—'):
        line = prepi(line)
      elif next_line[-2:]=='ов' or next_line=='раз' or next_line=='раза' or next_line=='лет' or next_line[-2:]=='ый' or (next_line[-2:]=='их' and len(next_line)>3):
        line=accusativise(line)
      else:
        line=prepositionalise(line)
        if next_line[-1]=='а' or next_line[-2:]=='ой':
          line = line.split(' ')
          line[-1]=='одной'
          line = ' '.join(line)
      out.write('"' + line +'"')
    #elif (line.isdigit()  and (any(x == prev_line for x in dative_prepositions)) and len(line)==4):
      #line=dati(line)
      
      #out.write('"' + line +'"')
    elif line.isdigit() and (any(x == prev_line for x in dative_prepositions)):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      if any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
        line=dati(line)
      elif next_line=='летию':
        line = genitivise(line)
      else:
        line=dativise(line)
      
      out.write('"' + line +'"')
    #elif (line.isdigit()  and (any(x == prev_line for x in instrumental_prepositions)) and len(line)==4):  
      #line=insti(line)
      #out.write('"' + line +'"')
    elif line.isdigit() and (any(x == prev_line for x in instrumental_prepositions)):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      if any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
        line=insti(line)
      else:
       line=instrumentalise(line)
      out.write('"' + line +'"')
    #elif (line.isdigit()  and (any(x == prev_line for x in accusative_prepositions)) and len(line)==4):
      #line=acci(line)
      #out.write('"' + line +'"')
    elif (line.isdigit()  and (any(x == prev_line for x in accusative_prepositions))):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
     # i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      next_next_line = lines[i+2]
      if next_next_line == '':
        break

      pos = next_next_line.find(',')
      #i1 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]

      pos = next_next_line.find(',')
      #i2 = next_next_line[:pos]
      next_next_line = next_next_line[pos + 1:]
      if any(x==prev_line for x in po):
        
        
        if next_line[-2:]=='ям' or (next_line[-2:]=='му') or (next_line[-2:]=='ам'):
          
          line=dativise(line)
        elif next_line[-2:]=='ов' or 'век' in next_line:
          
           line = accusativise(line)
        elif prev_prev_line.isdigit() and any(x==prev_prev_prev_line for x in genitive_prepositions) and any(y==next_line for y in years_abbreviations):
          
          line = acci(line)
        elif prev_prev_line.isdigit() and len(prev_prev_line)==4 and len(line)==4:
          line = acci(line)
        elif any(z==next_line for z in years_abbreviations):
          
          line = dativise(line)
        else:
            line=accusativise(line)
           
      elif any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
        line=acci(line)
      elif next_line[-1]=='м' or next_line[-1]=='у':
        line = dativise(line)
      else:
        line=accusativise(line)
      out.write('"' + line +'"')
    #elif (line.isdigit()  and (any(x == prev_line for x in nominative_prepositions)) and len(line)==4):
      #line=nomi(line)
      #out.write('"' + line +'"')
    elif (line.isdigit()  and (any(x == prev_line for x in nominative_prepositions))):
      num=line
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      if any(x==next_line for x in years_abbreviations) or any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):

        if any(y==next_line for y in gen_months) or any(z==next_line for z in prep_months):
          line=nomi(line)
        elif next_line=='году':
          line = prepositionalise(line)
        else:
          if line=='2':
            line = accusativise(line)
          else:
            line = acci(line)
      elif (next_line[-1]=='е' or next_line[-2:]=='ах'or next_line[-2:]=='ии' or next_line[-2:]=='ом' or next_line[-2:]=='ой' or next_line[-1:]=='и') and len(next_line)>3:
        line = prepositionalise(line)
      elif next_line[-2:]=='ых':
        line = genitivise(line)
      elif line =='1000':
        line = 'тысячу'
      else:
        line=num2words(line,lang='ru')
        if len(num)==4 and num[0]=='1':
          a = line.split(' ')
          a = a[1:]
          line = ' '.join(a)
      out.write('"' + line +'"')
    
   
   
   ###
    
    
    
    #elif len(line)>1 and line.iser() and not any(x in line for x in russian_dict ) and not any(y in line for y in ddict) and not any(z in line for z in numbers)and '.' in line:
    elif line.isupper() and (not any(x in line for x in ddict)) and  (not any(y in line for y in russian_dict)) and not (any(z in line for z in numbers)) and len(line)>1:
      
      if line in res:
        srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
        line=srtd[0][0]
      else:
       
        line = line.replace(' ','')
        line=line.replace('.','')
        line=line.replace('(','')
        line=line.replace(')','')
        a=[]
        length=len(line)
        ii=0
        while ii <length:
          number=line[ii]
          if line[ii].isdigit():
            number =line[ii]
            number = number.translate(SUB)
            number = number.translate(SUP)
            number = number.translate(OTH)
            
          a.append(combined_dict[number])
          ii+=1
        line = ' '.join(a)
      
      out.write('"' + line +'"')
    ## ROMAN NUMERALS
    #elif (any(x==line for x in roman_numerals) and (any(y==prev_line for y in genitive_prepositions) or any(y==prev_line for y in prepositional_prepositions) or any(y==prev_line for y in instrumental_prepositions) or any(y==prev_line for y in accusative_prepositions) or any(y==prev_line for y in dative_prepositions) or any(y==prev_line for y in nominative_prepositions))):
      #number = str(ddict[line])
      
      #if (any(y==prev_line for y in genitive_prepositions)):
        #line = geni(number)
      #elif (any(y==prev_line for y in prepositional_prepositions)):
        #line = prepi(number)
      #elif (any(y==prev_line for y in dative_prepositions)):
        #line = dati(number)
      #elif (any(y==prev_line for y in instrumental_prepositions)):
        #line = insti(number)
      #else:
        #line = acci(number)
      
      #out.write('"' + line + '"')
    elif (any(x==line for x in roman_numerals)):
      number = str(ddict[line])
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
      
    
      next_line = next_line[1:-1]
      
      
      if any(y==prev_line for y in gen_royals):
        line = geni(number)
      elif any(y==prev_line for y in royals):
        line = acci(number)
      elif any(y==prev_line for y in royals_female):
        number = accusativise(number)
        a = number.split(' ')
        a[-1] = aya_ord_dict[a[-1]]
        line = ' '.join(a)
      elif any(y==prev_line for y in gen_royals_female):
        number = accusativise(number)
        a = number.split(' ')
        a[-1] = oy_ord_dict[a[-1]]
        line  = ' '.join(a)
      elif any(y==prev_line for y in inst_royals):
        line = insti(number)
      elif any(y==prev_line for y in dat_royals):
        line = dati(number)
      elif any(y==prev_line for y in prep_royals):
        line = prepi(number)
      elif any(x==prev_prev_line for x in royalties):
        line = acci(number)
      
      elif len(prev_line.split(' '))==1 and len(prev_line)>2 and prev_prev_line!='.' and any(x in prev_line for x in russian_capital_letters) and prev_line[0].isupper() and prev_line[1].islower() and prev_line[-1]!='а' and prev_line[-1]!='м' and prev_line[-1]!='е':
        line = acci(number)
      elif len(prev_line.split(' '))==1 and len(prev_line)>2 and prev_prev_line!='.' and any(x in prev_line for x in russian_capital_letters)and prev_line[0].isupper() and prev_line[1].islower() and prev_line[-1]=='а':
        line = geni(number)  
      elif (next_line=='века'or next_line=='Века'):
        line = geni(number)
      elif prev_line=='годов':
        line = geni(number)
      elif line=='XX':
        line = 'x x'
      elif line=='VI':
        line = 'v i'
      elif line=='IV':
        line='i v'
      elif line=='XI':
        line='x i'
      elif line=='IX':
        line='i x' 
      elif any(x==prev_line for x in dative_prepositions) and next_line[-1]=='у':
        line = dativise(number)
      elif (next_line=='век' or next_line=='Век') and line!='XX' and line!='VI' and line!='IV' and line!='XI' and line!='IX':
        line = num2words(number,lang='ru')
      elif (next_line=='веке' or next_line=='Веке') or next_line[-2:]=='ии':
        line = prepi(number)
      elif (next_line=='веках' or next_line=='Веках') and prev_line!='—':
        line = genitivise(number)
      elif any(x==prev_line for x in full_prepositions):
        line = genitivise(number)
      elif (next_line=='и' or next_line=='-')and any(x==next_next_line for x in roman_numerals_full) and next_next_next_line=='веков':
        line = genitivise(number)
      elif next_line=='—' and any(p==next_next_line for p in roman_numerals_full) and next_next_next_line=='веков':
        line = accusativise(number)
      elif next_line=='веков' and (prev_line=='-') and any(x==prev_prev_line for x in roman_numerals_full):
       line = genitivise(number)
      elif prev_line[-2:]=='ом':
        line = insti(number)
      elif any(x==next_line[-2:] for x in trigger_gen_a4) and len(next_line)>3:
        line = geni(number)
      elif any(x==next_line[-2:] for x in trigger_prep_a4)and len(next_line)>3:
        line = prepi(number)
      elif any(x==prev_line[-2:] for x in trigger_endings_b4_2)and len(next_line)>3 or prev_line[-2:]=='ля':
        line = genitivise(number)
      else:
        line = num2words(number,lang='ru')
      
      out.write('"' + line + '"')
    elif prev_line.isdigit() and (line=='х' or line=='x'):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      if next_line.isdigit():
        line = 'на'
        
        nana+=1
      else:
          if line == 'х':
            line = line
          else:
            line=line+'_latin'
      
      out.write('"' + line + '"')
    elif any(x==line for x in roman_numeral_exceptions):
      number = str(ddict[line])
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      #i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      #i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      
      
        
      if (next_line=='века'or next_line=='Века'):
        line = geni(number)
        
      elif (next_line=='веке' or next_line=='Веке'):
        line = prepi(number)
      elif (next_line=='веках' or next_line=='Веках'):
        line = genitivise(number)
      elif any(y==prev_line for y in gen_royals):
        line = geni(number)
      elif any(y==prev_line for y in royals):
        line = acci(number)
      elif any(y==prev_line for y in royals_female):
        number = accusativise(number)
        a = number.split(' ')
        a[-1] = aya_ord_dict[a[-1]]
        line = ' '.join(a)
      elif any(y==prev_line for y in gen_royals_female):
        number = accusativise(number)
        a = number.split(' ')
        a[-1] = oy_ord_dict[a[-1]]
        line  = ' '.join(a)
      elif any(y==prev_line for y in inst_royals):
        line = insti(number)
      elif any(y==prev_line for y in dat_royals):
        line = dati(number)
      elif any(y==prev_line for y in prep_royals):
        line = prepi(number)
      elif any(x==prev_prev_line for x in royalties):
        line = acci(number)
      #elif len(prev_line.split(' '))==1 and len(prev_line)>2 and prev_prev_line!='.' and any(x in prev_line for x in russian_capital_letters)and prev_line[0].isupper() and prev_line[1].islower() and prev_line[-1]!='а' and prev_line[-1]!='м' and prev_line[-1]!='е':
        #line = acci(number)
      #elif len(prev_line.split(' '))==1 and len(prev_line)>2 and prev_prev_line!='.' and any(x in prev_line for x in russian_capital_letters)and prev_line[0].isupper() and prev_line[1].islower() and prev_line[-1]=='а':
        #line = geni(number)
      else:
        a = line
        if a in latin_dict:
          a = latin_dict[a]
        b = '_latin'
        line = a+b
      
      out.write('"' + line + '"')
    elif line.islower() and (not any(x in line for x in ddict)) and  (not any(y in line for y in russian_dict)) and not (any(z in line for z in numbers)) and len(line)>1:
      
      if line in res:
        srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
        line=srtd[0][0]
        
      else:
       
        line = line.replace(' ','')
        line=line.replace('.','')
        a=[]
        length=len(line)
        ii=0
        while ii <length:
          a.append(line[ii])
          ii+=1
        line = ' '.join(a)
      
      out.write('"' + line +'"')
      ##/////////////////////////////////////BEGINNING OF DATES ///////////////////////////////////
    
    ##OYE DATES
    #elif (len(line.split(' '))==3 and line.split(' ')[0].isdigit() and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and next_next_line=='.' and (prev_line + 'BREAK' + prev_prev_line + 'BREAK' + prev_prev_prev_line in newspaper_dict)):
      #print(line)
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' ' + d
      
      #out.write('"' + line +'"')
      #oyeeee+=1
    #elif (len(line.split(' '))==3 and line.split(' ')[0].isdigit() and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and ((prev_line=='(' and next_line==')')) and next_next_line=='.' and ((any(y==prev_prev_line for y in dating)))):
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' ' + d
      
      #out.write('"' + line +'"')
      #oyeeee+=1
    #elif (len(line.split(' '))==3 and line.split(' ')[0].isdigit() and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and ((prev_line=='(' and next_line==')')) and next_next_line=='.' and ((any(y==prev_prev_line for y in websites_oye)))):
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' ' + d
      
      #out.write('"' + line +'"')
      #oyeeee+=1
    #elif (len(line.split(' '))==3 and line.split(' ')[0].isdigit() and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and ((prev_line=='(' and next_line==')')) and next_next_line=='.' and ((prev_prev_line==two_step[0] and prev_prev_prev_line==two_step[1]) or (prev_prev_line==two_step[2] and prev_prev_prev_line==two_step[3]) or (prev_prev_line==two_step[4] and prev_prev_prev_line==two_step[5]))):
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' ' + d
      
      #out.write('"' + line +'"')
      #oyeeee+=1
    elif (prev_line=='(' and next_line=='-') or (prev_line=='-' and next_line==')') and len(line.split(' '))==3 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      line = line
      out.write('"' + line +'"')
    elif 'гг.' in line and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and prev_line=='-':
      line = line
      out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(x==prev_line for x in months) and line.split(' ')[1]=='г.':
      year = line.split(' ')[0]
      year = geni(year)
      d = 'года'
      line = year + ' ' + d
      out.write('"' + line +'"')
      
      ## THE OYE
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and prev_line =='.':
      month = line.split(' ')[1]
      day = accusativise(line.split(' ')[0])
      day = day.split(' ')
      day[-1]=oye_ord_dict[day[-1]]
      day = ' '.join(day)
      line = day + ' ' + month
      out.write('"' + line +'"')
      
      oyeeee+=1
    #elif i2=='0' and (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #if next_line=='.' or next_line==')'or (any(y==next_line for y in prepositional_prepositions) and next_line!='в') or (any(z==next_line for z in nominative_prepositions)):
        #case = 0
      #elif next_line[-2:]=='ия' or next_line[-1]=='у' or next_line[-3:]=='дан':
        #case = 0
      #elif next_line=='здесь' or next_line=='его' or (next_line=='назначен' and next_next_line[-2:]=='ом') or next_line=='происходит':
        #case = 0
      #else:
        #case = 1
      #if case==0:
        ##print('oye')
        #day[-1]=oye_ord_dict[day[-1]]
      #elif case==1:
        #day[-1]=gen_ord_dict[day[-1]]
      #day = ' '.join(day)
      #line = day + ' ' + month + ' ' + year + ' '+ d
      #out.write('"' + line +'"')
    #elif (i2=='1') and (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and prev_line=='Проверено' and next_line=='.':
      
      #print(prev_prev_line)
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' '+ d
      #out.write('"' + line +'"')
    #elif (i2=='2' or i2=='3') and (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and ((prev_line=='(' and next_line==')') or (prev_line==';' and next_line==','))and any(x in prev_prev_line for x in latin_letters):
      
      #print(prev_prev_line)
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' '+ d
      #out.write('"' + line +'"')
    #elif (i2=='5') and (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and ((prev_line=='(' and next_line==')')or (prev_line==';' and next_line==',')) and any(x in prev_prev_line for x in latin_letters)and any(x in prev_prev_prev_line for x in latin_letters)and any(x in prev_prev_prev_prev_line for x in latin_letters):
      
      #print(prev_prev_line)
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' '+ d
      #out.write('"' + line +'"')
    #elif  (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and prev_line=='(' and (len(prev_prev_line.split(' '))==3 or len(prev_prev_line.split(' '))==4) and prev_prev_line.split(' ')[0].isdigit() and any(x==prev_prev_line.split(' ')[1] for x in gen_months) and prev_prev_line.split(' ')[2].isdigit():
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      
      #day = ' '.join(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' '+ d
      #out.write('"' + line +'"')
    #elif len(line.split(' '))==3 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and prev_prev_line =='.' and (next_line=='-' or next_line=='—' or next_line==',') and (prev_line=='-' or prev_line=='—' or prev_line==','):
      #month = line.split(' ')[1]
      #day = accusativise(line.split(' ')[0])
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #line = day + ' ' + month + ' ' + year + ' ' + d
      #out.write('"' + line +'"')
      
      #oyeeee+=1
  ##FIRSTLY, ££££ GODA FOR DIFFERENT PREPOSITIONS,
    elif 'годами' in line and line.split(' ')[0].isdigit() and prev_line=='и':
      number = line.split(' ')[0]
      b = 'годом'
      number = insti(number)
      line = number + ' ' + b
      out.write('"' + line +'"')
    elif prev_line=='.' and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and line.split(' ')[1]=='году':
      year = acci(line.split(' ')[0])
      d = 'год'
      line = year + ' ' + d
      out.write('"' + line +'"')
    elif ((line.split(' ')[0].isdigit()) and ((any(x == prev_line for x in genitive_prepositions))or(any(x == prev_line for x in accusative_prepositions))or(any(x == prev_line for x in dative_prepositions))or(any(x == prev_line for x in instrumental_prepositions))or(any(x == prev_line for x in prepositional_prepositions))or(any(x == prev_line for x in nominative_prepositions))) 
      and (len(line.split(' ')) == 2) and ((line.split(' ')[1] == 'года') or (line.split(' ')[1] == 'г.') or (line.split(' ')[1] == 'г')or (line.split(' ')[1] == 'году')or (line.split(' ')[1] == 'годом') or (line.split(' ')[1]=='гг.')or (line.split(' ')[1] == 'год')) 
      and (not any(x in line.split(' ')[0] for x in punc_marks))):
     
      number = line.split(' ')[0]
      if any(x == prev_line for x in genitive_prepositions):
        if prev_line=='с' and 'годом' in line:
          a=insti(number)
          b='годом'
        elif ( prev_line=='с')and 'г.' in line:

          a=insti(number)
          b='годом'
        elif prev_line=='С' and 'г.' in line:
          a = acci(number)
          b = 'год'
        else:
          a=geni(number)
          b = 'года'
      elif any(x == prev_line for x in prepositional_prepositions):
        
        if 'года' in line:
         a = geni(number)
         b='года'
         
        else:
          a=prepi(number)
          b = 'году'
      elif any(x == prev_line for x in nominative_prepositions):
        if prev_line=='за':
         a = acci(number)
         b = 'год'
        elif 'г.' in line:
          a = prepi(number)
          b = 'году'
        else:
          a=acci(number)
          b = 'год'
      elif any(x == prev_line for x in dative_prepositions):
        a=dati(number)
        b = 'году'
      elif any(x == prev_line for x in instrumental_prepositions):
        a=insti(number)
        b = 'годом'
      elif any(x == prev_line for x in accusative_prepositions):
        a=acci(number)
        b='год'
      line=a + ' '+ b
      out.write('"' + line + '"')
      
     
    elif line=='К' and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months):
      a = geni(line.split(' ')[0])
      b = line.split(' ')[1]
      line = a + ' ' + b
      out.write('"' + line + '"')
    elif line=='к' and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months):
      a = dati(line.split(' ')[0])
      b = line.split(' ')[1]
      line = a + ' ' + b
      out.write('"' + line + '"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and len(prev_line)>4 and prev_line[-4:]=='ание' and any(x==line.split(' ')[1] for x in years_abbreviations):
      number = line.split(' ')[0]
      a=geni(number)
      b = 'года'
      line=a + ' '+ b
      out.write('"' + line + '"')
    ## THIS NEXT PART IS FOR IF GODA IS TOUCHING THE NUMBERS, E.G. 1907GODA FOR ALL PREPOSITIONS. AGAIN NOMINATIVE IGNORED FOR NOW
    elif ( (('года' in line) or ('году' in line) or ('годом' in line) or ('гг.' in line) or ('г' in line) or ('г.' in line)or ('гг' in line))and (line.replace('года','').isdigit() or line.replace('году','').isdigit() or line.replace('гг.','').isdigit() or line.replace('г.','').isdigit() or line.replace('г','').isdigit() or line.replace('годом','').isdigit() or line.replace('год','').isdigit()) and ((any(x == prev_line for x in genitive_prepositions))or(any(x == prev_line for x in accusative_prepositions))or(any(x == prev_line for x in dative_prepositions))or(any(x == prev_line for x in instrumental_prepositions))or(any(x == prev_line for x in prepositional_prepositions))or(any(x == prev_line for x in nominative_prepositions))) and (len(line.split(' ')) == 1)):
      
      if 'года' in line:
        number = line.replace('года','')
        
      elif 'году' in line:
        number = line.replace('году','')
        
      elif 'годом' in line:
        number = line.replace('годом','')
        
      elif 'г' in line and not 'г.' in line and not 'гг.' in line and not 'гг' in line:
        number = line.replace('г','')
        
      elif 'г.' in line and not 'гг.' in line:
        number = line.replace('г.','')
        
      elif 'гг' in line and not 'гг.' in line:
        number = line.replace('гг','')
        
      elif 'гг.' in line:
        number = line.replace('гг.','')
        
      if any(x == prev_line for x in genitive_prepositions):
        if prev_line=='с' and 'годом' in line:
          a=insti(number)
          b='годом'
        else:
          a=geni(number)
          b = 'года'
      elif any(x == prev_line for x in prepositional_prepositions):
        
          a=prepi(number)
          b = 'году'
      elif any(x == prev_line for x in nominative_prepositions):
        if 'г.' in line:
          a = prepi(number)
          b = 'году'
        else:
          a=acci(number)
          b = 'год'
      elif any(x == prev_line for x in dative_prepositions):
        a=dati(number)
        b = 'году'
      elif any(x == prev_line for x in instrumental_prepositions):
        a=insti(number)
        b = 'годом'
      elif any(x == prev_line for x in accusative_prepositions):
        a=acci(number)
        b='год'
      line=a + ' '+ b
      out.write('"' + line + '"')
    elif len(line.split(' '))==2 and 'г.' in line and line.split(' ')[0].isdigit() and all(x!=prev_line for x in full_prepositions) and all(y!=prev_line for y in measurement_marks):
      year = line.split(' ')[0]
      year = acci(year)
      d = 'год'
      line = year + ' ' + d
      out.write('"' + line + '"')
    ##MOVING ON TO THE MONTHS. FIRSTLY, ## MONTH ££££ WITH PREPOSITIONS
    elif ((len(line.split(' '))==3 ) and (line.split(' ')[2].isdigit()) and (line.split(' ')[0].isdigit()) and (any(x ==line.split(' ')[1] for x in gen_months) or any(x ==line.split(' ')[1] for x in gen_months_upper)or any(x ==line.split(' ')[1] for x in prep_months)or any(x ==line.split(' ')[1] for x in dat_months) or any(x ==line.split(' ')[1] for x in inst_months))and((any(x == prev_line for x in genitive_prepositions))or(any(x == prev_line for x in accusative_prepositions))or(any(x == prev_line for x in dative_prepositions))or(any(x == prev_line for x in instrumental_prepositions))or(any(x == prev_line for x in prepositional_prepositions))or(any(x == prev_line for x in nominative_prepositions)))) :
     
      number = line.split(' ')[0]
      b = line.split(' ')[1].lower()
      year = line.split(' ')[2]
      if any(x == prev_line for x in genitive_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a)
        
      elif any(x == prev_line for x in prepositional_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=prep_ord_dict[a[-1]]
        a=' '.join(a)
        
      elif any(x == prev_line for x in dative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=dat_ord_dict[a[-1]]
        a=' '.join(a)
        
      elif any(x == prev_line for x in instrumental_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=inst_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in accusative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a) 
      elif any(x == prev_line for x in nominative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=oye_ord_dict[a[-1]]
        a=' '.join(a) 
      if b[0].isupper():
        print('')
      #a = genitivise(line.split(' ')[0])
      
      c=geni(year) 
      d = 'года'
      out.write('"' + a +' '+ b +' '+ c + ' ' + d +'"')
      gen+=1
      
     ## NOW IF GODA IS INCLUDED AND IF GODA IS AND ISN'T TOUCHING 
    elif len(line.split(' '))==3 and line.split(' ')[0].isdigit() and prev_line[-4:]=='ание' and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months):
      number = line.split(' ')[0]
      b = line.split(' ')[1]
      year = line.split(' ')[2]
      a=num2words(int(number), lang='ru')
      a = a.split(' ')
      a[-1]=gen_ord_dict[a[-1]]
      a=' '.join(a)
      c=geni(year) 
      d = 'года'
      out.write('"' + a +' '+ b +' '+ c + ' ' + d +'"') 
    elif ((len(line.split(' '))==3 ) and (line.split(' ')[0].isdigit()) and ('года' in line or 'году' in line or 'годом' in line or 'гг.' in line or 'г' in line or 'г.' in line or 'гг' in line or 'год' in line)  and (line.split(' ')[2].replace('года','').isdigit()or line.split(' ')[2].replace('году','').isdigit()or line.split(' ')[2].replace('годом','').isdigit())  and (any(x ==line.split(' ')[1] for x in gen_months) or any(x ==line.split(' ')[1] for x in gen_months_upper)or any(x ==line.split(' ')[1] for x in prep_months)or any(x ==line.split(' ')[1] for x in dat_months) or any(x ==line.split(' ')[1] for x in inst_months))and((any(x == prev_line for x in genitive_prepositions))or(any(x == prev_line for x in accusative_prepositions))or(any(x == prev_line for x in dative_prepositions))or(any(x == prev_line for x in instrumental_prepositions))or(any(x == prev_line for x in prepositional_prepositions))or(any(x == prev_line for x in nominative_prepositions)))) :
      #elif (len(line.split(' '))==3 ) and (line.split(' ')[2].isdigit()) and (line.split(' ')[0].isdigit()) and (any(x ==line.split(' ')[1] for x in gen_months))and(any(x == prev_line for x in genitive_prepositions) or #any(y==prev_line for y in accusative_prepositions))
      
      number = line.split(' ')[0]
      b = line.split(' ')[1].lower()
      if 'года' in line:
        year = line.split(' ')[2].replace('года','')
      elif 'году' in line:
        year = line.split(' ')[2].replace('году','')
      elif 'годом' in line:
        year = line.split(' ')[2].replace('годом','')
      elif 'г' in line and not 'гг.' in line and not 'гг' in line and not 'г.' in line:
        year = line.split(' ')[2].replace('г','')
      elif 'г.' in line and not 'гг.' in line:
        year = line.split(' ')[2].replace('г.','')
      elif 'гг' in line and not 'гг.' in line:
        year = line.split(' ')[2].replace('гг','')
      elif 'гг.' in line:
        year = line.split(' ')[2].replace('гг.','')
      elif 'год' in line:
        year = line.split(' ')[2].replace('год.','')
      if any(x == prev_line for x in genitive_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in prepositional_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=prep_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in dative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=dat_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in accusative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in instrumental_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=inst_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in nominative_prepositions):
        a=num2words(int(number), lang='ru')
        a = a.split(' ')
        a[-1]=oye_ord_dict[a[-1]]
        a=' '.join(a)
      if b[0].isupper():
        print('')
      #a = genitivise(line.split(' ')[0])
      
      c=geni(year)
      d = 'года'
      out.write('"' + a +' '+ b +' '+ c + ' ' + d +'"')
      gen+=1
   
    elif (((len(line.split(' '))==4 ) and (line.split(' ')[2].isdigit()) and (line.split(' ')[0].isdigit()) and (any(x ==line.split(' ')[1] for x in gen_months) or any(x ==line.split(' ')[1] for x in gen_months_upper)or any(x ==line.split(' ')[1] for x in prep_months)or any(x ==line.split(' ')[1] for x in dat_months) or any(x ==line.split(' ')[1] for x in inst_months)) and((any(x == prev_line for x in genitive_prepositions))or(any(x == prev_line for x in accusative_prepositions))or(any(x == prev_line for x in dative_prepositions))or(any(x == prev_line for x in instrumental_prepositions))or(any(x == prev_line for x in prepositional_prepositions))or(any(x == prev_line for x in nominative_prepositions))) and ((line.split(' ')[3] == 'года') or (line.split(' ')[3] == 'г.')or (line.split(' ')[3] == 'г')or (line.split(' ')[3] == 'гг')or (line.split(' ')[3] == 'гг.')or(line.split(' ')[3]=='году')or (line.split(' ')[3]=='годом')or (line.split(' ')[3]=='год')))):
      #elif ((len(line.split(' '))==4 ) and (line.split(' ')[2].isdigit()) and (line.split(' ')[0].isdigit()) and (any(x ==line.split(' ')[1] for x in gen_months)) and(any(x == prev_line for x in #genitive_prepositions)or any(y==prev_line for y in accusative_prepositions)) and ((line.split(' ')[3] == 'года') or (line.split(' ')[3] == 'г.')or (line.split(' ')[1] == 'г'))):
      number = line.split(' ')[0]
      c = geni(line.split(' ')[2])
      d = 'года'
      b = line.split(' ')[1].lower()
      a=num2words(int(number), lang='ru')
      a = a.split(' ')
      if any(x == prev_line for x in genitive_prepositions):
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in prepositional_prepositions):
        a[-1]=prep_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in dative_prepositions):
        if line.split(' ')[3] == 'года':
          a[-1]=gen_ord_dict[a[-1]]
        else:
          a[-1]=dat_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in instrumental_prepositions):
        a[-1]=inst_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in accusative_prepositions):
        a[-1]=gen_ord_dict[a[-1]]
        a=' '.join(a)
      elif any(x == prev_line for x in nominative_prepositions):
        a[-1]=oye_ord_dict[a[-1]]
        a=' '.join(a)
      if b[0].isupper():
        print('UPPER')
      
      gen+=1
      out.write('"' + a +' '+ b +' '+ c+' '+d+'"')
    
    elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(x == line.split(' ')[1] for x in gen_months) and (any(y==prev_line for y in genitive_prepositions))):
      
      month = line.split(' ')[1]
      number = line.split(' ')[0]
      inwords = num2words(number,lang='ru')
      inwords=inwords.split(' ')
      inwords[-1]=gen_ord_dict[inwords[-1]]
      inwords = ' '.join(inwords)
      #print(inwords)
      out.write('"' + inwords + ' ' + month + '"')
    elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(x == line.split(' ')[1] for x in gen_months) and (any(y==prev_line for y in nominative_prepositions))):
      
      month = line.split(' ')[1]
      number = line.split(' ')[0]
      inwords = num2words(number,lang='ru')
      inwords=inwords.split(' ')
      inwords[-1]=oye_ord_dict[inwords[-1]]
      inwords = ' '.join(inwords)
      #print(inwords)
      out.write('"' + inwords + ' ' + month + '"')
    elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(x == line.split(' ')[1] for x in gen_months) and (any(y==prev_line for y in dative_prepositions))):
      
      month = line.split(' ')[1]
      number = line.split(' ')[0]
      inwords = num2words(number,lang='ru')
      inwords=inwords.split(' ')
      inwords[-1]=dat_ord_dict[inwords[-1]]
      inwords = ' '.join(inwords)
      #print(inwords)
      out.write('"' + inwords + ' ' + month + '"')
    elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and (any(x == line.split(' ')[1] for x in gen_months) or any(x == line.split(' ')[1] for x in prep_months)) and (any(y==prev_line for y in prepositional_prepositions))):
      
      month = line.split(' ')[1]
      number = line.split(' ')[0]
      inwords = num2words(number,lang='ru')
      inwords=inwords.split(' ')
      inwords[-1]=prep_ord_dict[inwords[-1]]
      inwords = ' '.join(inwords)
      #print(inwords)
      out.write('"' + inwords + ' ' + month + '"')
    elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(x == line.split(' ')[1] for x in gen_months)):
      
      month = line.split(' ')[1]
      number = line.split(' ')[0]
      inwords = num2words(number,lang='ru')
      inwords=inwords.split(' ')
      inwords[-1]=gen_ord_dict[inwords[-1]]
      inwords = ' '.join(inwords)
      #print(inwords)
      out.write('"' + inwords + ' ' + month + '"')
    elif len(line.split(' '))==2 and line.split(' ')[1].isdigit() and any(x==line.split(' ')[0] for x in gen_months) and any(y==prev_line for y in genitive_prepositions):
      year = line.split(' ')[1]
      year = geni(year)
      d = 'года'
      month = line.split(' ')[0]
      line = month + ' ' + year + ' ' + d
      out.write('"' + line + '"')
    #elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len2_dict) and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months):
      #day = line.split(' ')[0]
      #month = line.split(' ')[1]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #line = day + ' ' + month + ' ' + year + ' ' + d
      #print('cheat: year2')
      #out.write('"' + line +'"')
    #elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len3_dict) and len(line.split(' '))==3 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      #day = line.split(' ')[0]
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #day = accusativise(day)
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #year = geni(year)
      #d = 'года'
      #print('cheat: year3')
      
      #line = day + ' ' + month + ' ' + year + ' ' + d
      #out.write('"' + line +'"')
    #elif (next_next_line+ 'BREAK'+ next_line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len4_dict) and len(line.split(' '))==4 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #day = day.split(' ')
      #day[-1]=oye_ord_dict[day[-1]]
      #day = ' '.join(day)
      #year = geni(year)
      #d = 'года'
      #print('cheat: year4')
      
      #line = day + ' ' + month + ' ' + year + ' ' + d
      #out.write('"' + line +'"')
    #elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(x == line.split(' ')[1] for x in gen_months) and (prev_line=='.')):
    #  print(line)
    #  month = line.split(' ')[1]
    #  number = line.split(' ')[0]
    #  inwords = num2words(number,lang='ru')
    #  inwords=inwords.split(' ')
    #  inwords[-1]=oye_ord_dict[inwords[-1]]
    #  inwords = ' '.join(inwords)
      #print(inwords)
    #  out.write('"' + inwords + ' ' + month + '"')
    #elif (len(line.split(' '))==2 and (line.split(' ')[0].isdigit()) and any(v == line.split(' ')[1] for v in gen_months) and ((not any(x == prev_line for x in genitive_prepositions)) and (not any(y ==prev_line for y #in prepositional_prepositions)) and (not any(z ==prev_line for z in accusative_prepositions)) and (not any(w ==prev_line for w in dative_prepositions)))):
    #  print(line)
    #  month = line.split(' ')[1]
    #  number = line.split(' ')[0]
    #  inwords = num2words(number,lang='ru')
    #  inwords=inwords.split(' ')
    #  inwords[-1]=oye_ord_dict[inwords[-1]]
    #  inwords = ' '.join(inwords)
    #  print(prev_line)
    #  print(inwords)
    #  out.write('"' + inwords + ' ' + month + '"')
    ##ACCUSATIVE
    
  
    
    ##NUMBERS OF THE FORM ##.##.## ETC
    elif (('.' in line ) and (line.count('.')==2)and(len(line.split('.'))==3)  and (len(line.split('.')[0])==2) and (len(line.split('.')[1])==2 or len(line.split('.')[1])==1) and (len(line.split('.')[2])==4 or len(line.split('.')[2])==2 or len(line.split('.')[2])==3) and (line.split('.')[0].isdigit()) and (line.split('.')[1].isdigit()) and (line.split('.')[2].isdigit())):
      year=line.split('.')[2]
      
      day=line.split('.')[0]
      month=line.split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    ##THIS ONE IS REVERSE
    elif (('.' in line ) and (line.count('.')==2)and(len(line.split('.'))==3)  and (len(line.split('.')[0])==4) and (len(line.split('.')[1])==2 or len(line.split('.')[1])==1) and (len(line.split('.')[2])==2) and (line.split('.')[0].isdigit()) and (line.split('.')[1].isdigit()) and (line.split('.')[2].isdigit())):
      day=line.split('.')[2]
      
      year=line.split('.')[0]
      month=line.split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('-' in line ) and (line.count('-')==2)and(len(line.split('-'))==3)  and (len(line.split('-')[0])==4) and (len(line.split('-')[1])==2 or len(line.split('-')[1])==1) and (len(line.split('-')[2])==2) and (line.split('-')[0].isdigit()) and (line.split('-')[1].isdigit()) and (line.split('-')[2].isdigit())):
      day=line.split('-')[2]
      
      year=line.split('-')[0]
      month=line.split('-')[1]
      
      c = geni(year)
      month=int(month)
      if month<12:
        b=gen_months[month-1]
      
      else:
       b='the'
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('.' in line ) and (line.count('.')==2)and(len(line.split('.'))==3)  and (len(line.split('.')[0])==1) and (len(line.split('.')[1])==2 or len(line.split('.')[1])==1) and (len(line.split('.')[2])==4 or len(line.split('.')[2])==2 or len(line.split('.')[2])==3) and (line.split('.')[0].isdigit()) and (line.split('.')[1].isdigit()) and (line.split('.')[2].isdigit())):
      year=line.split('.')[2]
      
      day=line.split('.')[0]
      month=line.split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('-' in line ) and (line.count('-')==2)and(len(line.split('-'))==3)  and (len(line.split('-')[0])==2) and (len(line.split('-')[1])==2 or len(line.split('-')[1])==1) and (len(line.split('-')[2])==4 or len(line.split('-')[2])==2 or len(line.split('-')[2])==3) and (line.split('-')[0].isdigit()) and (line.split('-')[1].isdigit()) and (line.split('-')[2].isdigit())):
      print(line)
      year=line.split('-')[2]
      
      day=line.split('-')[0]
      month=line.split('-')[1]
      
      c = geni(year)
      month=int(month)
      if month>12:
        b = 'hello'
      else:
        b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif prev_line=='с':
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('-' in line ) and (line.count('-')==2)and(len(line.split('-'))==3)  and (len(line.split('-')[0])==1) and (len(line.split('-')[1])==2 or len(line.split('-')[1])==1) and (len(line.split('-')[2])==4 or len(line.split('-')[2])==2 or len(line.split('-')[2])==3) and (line.split('-')[0].isdigit()) and (line.split('-')[1].isdigit()) and (line.split('-')[2].isdigit())):
      year=line.split('-')[2]
      
      day=line.split('-')[0]
      month=line.split('-')[1]
      
      c = geni(year)
      month=int(month)
      if month>12:
        b = 'hello'
      else:
        b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif prev_line=='с':
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('.' in line ) and len(line.split(' '))==2 and(len(line.split(' ')[0].split('.'))==3)  and (len(line.split(' ')[0].split('.')[0])==2) and (len(line.split(' ')[0].split('.')[1])==2 or len(line.split(' ')[0].split('.')[1])==1) and (len(line.split(' ')[0].split('.')[2])==4 or len(line.split(' ')[0].split('.')[2])==2 or len(line.split(' ')[0].split('.')[2])==3) and (line.split(' ')[0].split('.')[0].isdigit()) and (line.split(' ')[0].split('.')[1].isdigit()) and (line.split(' ')[0].split('.')[2].isdigit())) and 'года' in line:
      
      year=line.split(' ')[0].split('.')[2]
      day=line.split(' ')[0].split('.')[0]
      month=line.split(' ')[0].split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif prev_line=='с':
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('.' in line ) and len(line.split(' '))==2 and(len(line.split(' ')[0].split('.'))==3)  and (len(line.split(' ')[0].split('.')[0])==2) and (len(line.split(' ')[0].split('.')[1])==2 or len(line.split(' ')[0].split('.')[1])==1) and (len(line.split(' ')[0].split('.')[2])==4 or len(line.split(' ')[0].split('.')[2])==2 or len(line.split(' ')[0].split('.')[2])==3) and (line.split(' ')[0].split('.')[0].isdigit()) and (line.split(' ')[0].split('.')[1].isdigit()) and (line.split(' ')[0].split('.')[2].isdigit())) and 'г.' in line:
      
      year=line.split(' ')[0].split('.')[2]
      day=line.split(' ')[0].split('.')[0]
      month=line.split(' ')[0].split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions) or prev_line=='на' or prev_line=='На':
        #a[-1] = prep_ord_dict[a[-1]]
      #elif prev_line=='с':
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    elif (('/' in line ) and (line.count('/')==2)and(len(line.split('.'))==3)  and (len(line.split('.')[0])==2) and (len(line.split('.')[1])==2) and (len(line.split('.')[2])==4 or len(line.split('.')[2])==2) and (line.split('.')[0].isdigit()) and (line.split('.')[1].isdigit()) and (line.split('.')[2].isdigit())):
      year=line.split('/')[2]
      if len(line.split('/')[2])==2:
        if int(year)<17 and int(year)>=0:
          year = '20'+year
        else:
          year = '19'+year
      day=line.split('.')[0]
      month=line.split('.')[1]
      
      c = geni(year)
      month=int(month)
      b=gen_months[month-1]
      a = num2words(int(day), lang='ru')
     
      a = a.split(' ')
      #if any(x == prev_line for x in prepositional_prepositions):
        #a[-1] = prep_ord_dict[a[-1]]
      #elif any(x == prev_line for x in genitive_prepositions):
        #a[-1] = gen_ord_dict[a[-1]]
      #elif any(x == prev_line for x in dative_prepositions):
        #a[-1] = dat_ord_dict[a[-1]]
      #elif any(x == prev_line for x in instrumental_prepositions):
        #a[-1] = inst_ord_dict[a[-1]]
      #elif any(x == prev_line for x in accusative_prepositions):
        #a[-1] = acc_ord_dict[a[-1]]
      #else:
      a[-1]=oye_ord_dict[a[-1]]
      a=' '.join(a)
      d = 'года'
      line=a +' ' + b+' '+c + ' '+d
      
      out.write('"' + line +'"')
    ##IS THE LINE OF THE FORM $$$$ GODA WITH PREV LINE IN YEARS_abbreviations OR SEASONS?
    elif (len(line.split(' '))==2 and any(x==line.split(' ')[1] for x in years_abbreviations) and line.split(' ')[0].isdigit() and (any(y==prev_line for y in gen_months) or any(y==prev_line for y in seasons) or any(y==prev_line for y in prep_months) or any(y==prev_line for y in months))):
      prev_prev_line = lines[i-2]
      if prev_prev_line == '':
        break

      pos = prev_prev_line.find(',')
      #i1 = prev_prev_line[:pos]
      prev_prev_line = prev_prev_line[pos + 1:]

      pos = prev_prev_line.find(',')
      #i2 = prev_prev_line[:pos]
      prev_prev_line = prev_prev_line[pos + 1:]
    
    
      prev_prev_line = prev_prev_line[1:-1]
      if any(y==prev_line for y in months) and (all(x!=prev_prev_line for x in genitive_prepositions) and all(x!=prev_prev_line for x in accusative_prepositions) and all(x!=prev_prev_line for x in instrumental_prepositions) and all(x!=prev_prev_line for x in dative_prepositions) and all(x!=prev_prev_line for x in nominative_prepositions)):
        
        d = 'год'
        number = acci(line.split(' ')[0])
        line = number + ' ' + d
      else:
        d = 'года'
        number = geni(line.split(' ')[0])
        line = number + ' ' + d 
      out.write('"' + line +'"')
      
      seasonss+=1
    
    elif any(y==prev_line for y in genitive_prepositions) and len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(z==line.split(' ')[1] for z in gen_months):
      month = line.split(' ')[1]
      day = geni(line.split(' '))
      line = day + ' ' + month
      out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and any(z==line.split(' ')[1] for z in years_abbreviations_ab) and any(y==prev_line for y in months):
      d = 'года'
      year= geni(line.split(' ')[0])
      line = day + ' ' + month
      out.write('"' + line +'"')
    #elif len(line.split(' '))==3 and line.split(' ')[2].isdigit()and line.split(' ')[0].isdigit() and prev_line=='(' and any(x==line.split(' ')[1] for x in gen_months):
      #number = line.split(' ')[0]
      #number = nomi(number)
      #d = 'года'
      #month = line.split(' ')[1]
      #year = geni(line.split(' ')[2])
      #line = number + ' ' + month + ' ' + year + ' ' + d
      #out.write('"' + line +'"')
      #seasonss+=1
    #elif len(line.split(' '))==2 and ('года' in line or 'Года' in line) and line.split(' ')[0].isdigit() and prev_line=='(':
      #number = line.split(' ')[0]
      #number = acci(number)
      #d = 'года'
      #line = number + ' ' + d 
      #out.write('"' + line +'"')
    #elif len(line.split(' '))==2 and (line.split(' ')[1]=='год' or line.split(' ')[1]=='Год') and line.split(' ')[0].isdigit() and (any(y==prev_line for y in clause_marks)):
      #number = line.split(' ')[0]
      #number = acci(number)
      #d = 'год'
      #line = number + ' ' + d 
      #out.write('"' + line +'"')
    
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (line.split(' ')[1]=='Годах' or line.split(' ')[1]=='годах' or line.split(' ')[1]=='году' or line.split(' ')[1]=='Году'):
     year = line.split(' ')[0]
     year = prepi(year)
     line = year + ' ' + 'году'
     fem+=1
     out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (line.split(' ')[1]=='гг.'):
     year = line.split(' ')[0]
     year = acci(year)
     line = year + ' ' + 'год'
     fem+=1
     out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (line.split(' ')[1]=='Год' or line.split(' ')[1]=='год'):
     year = line.split(' ')[0]
     year = acci(year)
     line = year + ' ' + 'год'
     fem+=1
     out.write('"' + line +'"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (line.split(' ')[1]=='Года' or line.split(' ')[1]=='года'):
     year = line.split(' ')[0]
     year = geni(year)
     line = year + ' ' + 'года'
     fem+=1
     out.write('"' + line +'"')
   
    elif (len(line.split(' '))==2 and (any(x==line.split(' ')[1] for x in years_abbreviations)) and line.split(' ')[0].isdigit() and (all(y!=prev_line for y in full_clause_marks)) and (all(y!=prev_line for y in full_prepositions))):
      number = line.split(' ')[0]
      number = geni(number)
      d = 'года'
      line = number + ' ' + d 
      fem+=1
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+ 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len3_dict) and len(line.split(' '))==3 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      srtd = sorted(year_len3_dict[next_next_line + 'BREAK'+ next_line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      print(srtd)
      srtd = srtd[0][0]
      day = line.split(' ')[0]
      day = accusativise(day)
      day = day.split(' ')
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      year = geni(year)
      d= 'года'
      
      if srtd=='ое':
        print('OYE')
        day[-1]=oye_ord_dict[day[-1]]
      elif srtd=='го':
        day[-1]=gen_ord_dict[day[-1]]
        print('OGO')
      elif srtd=='ым':
        day[-1]=inst_ord_dict[day[-1]]
      elif srtd=='ому':
        ay[-1]=dat_ord_dict[day[-1]]
      else:
        print('THIS SHOULD NOT BE HAPPENING')
      day = ' '.join(day)
      line = day + ' ' + month + ' ' + year + ' ' + d
      out.write('"' + line +'"')
    elif (next_next_line + 'BREAK'+ next_line+ 'BREAK'+prev_line + 'BREAK' + prev_prev_line  in year_len4_dict)and len(line.split(' '))==4 and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit() and line.split()[3]=='года':
      srtd = sorted(year_len4_dict[next_next_line + 'BREAK'+ next_line + 'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      print(srtd)
      srtd = srtd[0][0]
      day = line.split(' ')[0]
      day = accusativise(day)
      day = day.split(' ')
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      year = geni(year)
      d= 'года'
      
      if srtd=='ое':
        day[-1]=oye_ord_dict[day[-1]]
        print('OYE')
      elif srtd=='го':
        day[-1]=gen_ord_dict[day[-1]]
        print('OGO')
      elif srtd=='ым':
        day[-1]=inst_ord_dict[day[-1]]
      elif srtd=='ому':
        ay[-1]=dat_ord_dict[day[-1]]
      else:
        print('THIS SHOULD NOT BE HAPPENING')
      day = ' '.join(day)
      line = day + ' ' + month + ' ' + year + ' ' + d
      out.write('"' + line +'"')
    #elif (len(line.split(' '))==3 or len(line.split(' '))==4) and line.split(' ')[0].isdigit() and any(x==line.split(' ')[1] for x in gen_months) and line.split(' ')[2].isdigit():
      
      #day = line.split(' ')[0]
      #day = accusativise(day)
      #day = day.split(' ')
      #month = line.split(' ')[1]
      #year = line.split(' ')[2]
      #year = geni(year)
      #d = 'года'
      #if next_line=='.' or next_line==')'or (any(y==next_line for y in prepositional_prepositions)) or (any(z==next_line for z in nominative_prepositions)):
        #case = 0
      #elif next_line[-2:]=='ия' or next_line[-1]=='у' or next_line[-3:]=='дан':
        #case = 0
      #elif next_line=='здесь' or next_line=='его' or (next_line=='назначен' and next_next_line[-2:]=='ом') or next_line=='происходит':
        #case = 0
      #else:
        #case = 1
      #if case==0:
        #print('OYEEEEEEEEEEE')
        #day[-1]=oye_ord_dict[day[-1]]
        
      #elif case==1:
        #day[-1]=gen_ord_dict[day[-1]]
      #day = ' '.join(day)
      #line = day + ' ' + month + ' ' + year + ' '+ d
      
      #out.write('"' + line +'"')
    elif (len(line.split(' '))==3  and line.split(' ')[0].isdigit() and line.split(' ')[2].isdigit() and any(x==line.split(' ')[1] for x in gen_months)and (all(y!=prev_line for y in full_clause_marks)) and (all(y!=prev_line for y in full_prepositions))):
      day = line.split(' ')[0]
      day = geni(day)
      year = line.split(' ')[2]
      year = geni(year)
      month = line.split(' ')[1]
      d = 'года'
      line = day + ' ' + month + ' ' + year + ' ' + d
      fem+=1
      out.write('"' + line +'"')
    elif line.isdigit() and any(x==prev_line for x in months):
      num=line
      line = num2words(line,lang='ru')
      if len(num)==4 and num[0]=='1':
        a= line.split(' ')
        a = a[1:]
        line = ' '.join(a)
      out.write('"' + line +'"')
    elif any(x in line for x in of_our_lord) and line.split(' ')[0].isdigit() and (prev_line=='-' or prev_line=='/'):
      line = line
      out.write('"' + line +'"')
    elif any(x in line for x in of_our_lord) and line.split(' ')[0].isdigit():
      c = 'до нашей эры'
      a = line.split(' ')[0]
      if 'году' in line or 'годах' in line:
        b= 'году'
        a = prepi(a)
      elif 'годами' in line:
        b = 'годом'
        a = insti(line)
      elif 'года' in line:
        b = 'года'
        a=geni(a)
      elif any(x==prev_line for x in prepositional_prepositions):
        b= 'году'
        a = prepi(a)
      elif any(x==prev_line for x in genitive_prepositions):
        b= 'года'
        a = geni(a)
      else:
        a = acci(a)
        b='год'
      line = a + ' ' + b + ' ' + c
      out.write('"' + line +'"')
    
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and (line.split(' ')[1]=='г.' or line.split(' ')[1]=='г') and any(z==prev_line for z in full_clause_marks):
      a = line.split(' ')[0]
      a = acci(a)
      b='год'
      line = a + ' ' + b
      out.write('"' + line +'"')
    elif len(line.split(' '))==3 and line.split(' ')[0].isdigit() and any(y==line.split(' ')[1] for y in gen_months) and line.split(' ')[2].isdigit():
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      day = line.split(' ')[0]
      day = geni(day)
      year = geni(year)
      d = 'года'
      line = day + ' ' + month + ' ' + year + ' ' + d
    
      out.write('"' + line +'"')
      
    elif len(line.split(' '))==4 and line.split(' ')[0].isdigit() and any(y==line.split(' ')[1] for y in gen_months) and line.split(' ')[2].isdigit() and line.split(' ')[3]=='года':
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      day = line.split(' ')[0]
      day = geni(day)
      year = geni(year)
      d = 'года'
      line = day + ' ' + month + ' ' + year + ' ' + d
      out.write('"' + line +'"')
    elif len(line.split(' '))==4 and line.split(' ')[0].isdigit() and any(y==line.split(' ')[1] for y in gen_months) and line.split(' ')[2].isdigit() and line.split(' ')[3]=='году':
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      day = line.split(' ')[0]
      day = geni(day)
      year = prepi(year)
      d = 'году'
      line = day + ' ' + month + ' ' + year + ' ' + d
      
      out.write('"' + line +'"')
    elif len(line.split(' '))==4 and line.split(' ')[0].isdigit() and any(y==line.split(' ')[1] for y in gen_months) and line.split(' ')[2].isdigit() and (line.split(' ')[3]=='г.' or line.split(' ')[3]=='год'):
      month = line.split(' ')[1]
      year = line.split(' ')[2]
      day = line.split(' ')[0]
      day = geni(day)
      year = acci(year)
      d = 'год'
      line = day + ' ' + month + ' ' + year + ' ' + d
      
      out.write('"' + line +'"') 
    
    ##/////////////////////////////////////END OF DATES ///////////////////////////////////
    
    
    #elif (('.' in line ) and (line.count('.')==2)and(len(line.split('.'))==3)  and (len(line.split('.')[0])==2) and (len(line.split('.')[1])==2) and (len(line.split('.')[2])==4) and (line.split('.')[0].isdigit()) and #(line.split('.')[1].isdigit()) and (line.split('.')[2].isdigit()) and prev_line=='.'):
    #  print(line)
    #  day=line.split('.')[0]
    #  month=line.split('.')[1]
    #  year=line.split('.')[2]
    #  c = genitivise(year)
    #  month=int(month)
    #  b=gen_months[month-1]
    #  a = num2words(int(day), lang='ru')
    #  
    #  a = a.split(' ')
    #  a[-1] = oye_ord_dict[a[-1]]
    #  a=' '.join(a)
    #  d = 'года'
    #  line = a+' '+b+ ' '+c+' '+d
    #  print(line)
    #  out.write('"' + line+'"')
    ##MEASUREMENTS##
    
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict)) and prev_line=='-' and prev_prev_line.isdigit() and any(y==prev_prev_prev_line for y in genitive_prepositions)):
      line = line
      out.write('"' + line+'"')
    
   
    
    #elif line in ddict and (any(x==prev_line for x in genitive_prepositions)):
      
      #line=geni(str(ddict[line]))
      
      #out.write('"' + line+'"')
    #elif line in ddict and (any(x==prev_line for x in dative_prepositions)):
     
      #line=dati(str(ddict[line]))
      
      #out.write('"' + line+'"')
    #elif line in ddict and (any(x==prev_line for x in instrumental_prepositions)):
      
      #line=insti(str(ddict[line]))
      
      #out.write('"' + line+'"')
      
    ##PREPOSITIONAL MEASUREMENTS
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and any(y==prev_line for y in prepositional_prepositions)):
      number=str(line.split(' ')[0])
      splitted = number
      
      if line.split(' ')[1] in me_dict:
        case=0
        number=str(line.split(' ')[0])
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      if measurement=='т.':
        
        
        num=accusativise(number)
        if len(number)==4 and number[0]=='1':
         num=num.split(' ')
         num=num[1:]
         num=' '.join(num)
        if number[-1]=='1' and (len(number)==1 or (len(number)>1 and number[-2]!='1')): 
         num=num.split(' ')
         num[-1]='одна'
         num = ' '.join(num)
         bbb=me_single_dict[measurement]
        elif (number[-1]=='2'or number[-1]=='3' or number[-1]=='4') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
         bbb=me_gen_single_dict[measurement]
         if number[-1]=='2':
          num=num.split(' ')
          num[-1]='две'
          num = ' '.join(num)
        else:
         bbb=me_gen_plural_dict[measurement]
      elif measurement in me_prep_exceptions:
        num = num2words(number, lang='ru')
       
        if len(number)==4 and number[0]=='1':
         num=num.split(' ')
         num=num[1:]
         num=' '.join(num)
        if number[-1]=='1' and (len(number)==1 or (len(number)>1 and number[-2]!='1')): 
         num=num.split(' ')
         num[-1]='один'
         num = ' '.join(num)
         bbb=me_single_dict[measurement]
        elif (number[-1]=='2'or number[-1]=='3' or number[-1]=='4') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
         bbb=me_gen_single_dict[measurement]
         if number[-1]=='2':
          num=num.split(' ')
          num[-1]='два'
          num = ' '.join(num)
        else:
         bbb=me_gen_plural_dict[measurement]
      else: 
        num = num2words(number, lang='ru')
        
        a=[]
        ii=0
        while ii<len(num.split(' ')):
          a.append(prep_car_dict[num.split(' ')[ii]])
          ii+=1
        num = ' '.join(a)
        if len(number)==4 and number[0]=='1':
          num=num.split(' ')
          num=num[1:]
          num=' '.join(num)
        if number[-1]=='1' and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
          if me_dict[measurement][1]=='f':
              num=num.split(' ')
              num[-1]='одной'
              num = ' '.join(num)
          bbb=me_prep_single_dict[measurement]
        
        else:
          bbb=me_prep_plural_dict[measurement]
      
      if prefix!=0:
        bbb = prefix+bbb
      line = num + ' '+bbb
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      if 'километрах' in line and all(x!=next_line for x in full_prepositions) and all(y not in next_line for y in directions):
        line = line.replace('километрах','километра')
      out.write('"' + line+'"')
      ##NOMINATIVE MEASUREMENTS
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and (any(y==prev_line for y in nominative_prepositions) or (any(y==prev_line for y in accusative_prepositions)))):
      number=str(line.split(' ')[0])
      splitted = number
      
      if line.split(' ')[1] in me_dict:
        case=0
        number=str(line.split(' ')[0])
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      if measurement=='т.' or measurement=='тонн':
        num = num2words(number, lang='ru')
       
        if len(number)==4 and number[0]=='1':
         num=num.split(' ')
         num=num[1:]
         num=' '.join(num)
        if number[-1]=='1' and (len(number)==1 or (len(number)>1 and number[-2]!='1')): 
         num=num.split(' ')
         num[-1]='одна'
         num = ' '.join(num)
         b=me_single_dict[measurement]
        elif (number[-1]=='2'or number[-1]=='3' or number[-1]=='4') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
         b=me_gen_single_dict[measurement]
         if number[-1]=='2':
          num=num.split(' ')
          num[-1]='две'
          num = ' '.join(num)
        else:
         b=me_gen_plural_dict[measurement]
      else: 
        num = num2words(number, lang='ru')
        
        if len(number)==4 and number[0]=='1':
          num=num.split(' ')
          num=num[1:]
          num=' '.join(num)
        if number[-1]=='1' and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
          if me_dict[measurement][1]=='f':
              num=num.split(' ')
              num[-1]='одна'
              num = ' '.join(num)
            
          b=me_gen_single_dict[measurement]
        else:
          b=me_gen_plural_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = num + ' '+b
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line+'"')
      ##MEASUREMENTS WITH COMMAS
    elif (len(line.split(' '))==2 and line.count(',')==1 and line.split(' ')[0].replace(',','').isdigit() and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and ((any(y==prev_line for y in genitive_prepositions))or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния') and prev_line!='с'):
      
      line = genitive_measure_comma(line)
      out.write('"' + line+'"')
    elif (len(line.split(' '))==2 and line.count(',')==1 and line.split(' ')[0].replace(',','').isdigit() and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and any(y==prev_line for y in prepositional_prepositions)):
      line = prepositional_measure_comma(line)
      out.write('"' + line+'"')
      ##GENITIVE MEASUREMENT
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and ((any(y==prev_line for y in genitive_prepositions))or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния') and prev_line!='с' and prev_line!='С'):
      
      number=str(line.split(' ')[0])
      splitted = number
      
      if line.split(' ')[1] in me_dict:
        case=0
        number=str(line.split(' ')[0])
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      
      num = num2words(number, lang='ru')
      a=[]
      ii=0
      while ii<len(num.split(' ')):
        a.append(gen_car_dict[num.split(' ')[ii]])
        ii+=1
      num = ' '.join(a)
      if len(number)==4 and number[0]=='1':
        num=num.split(' ')
        num=num[1:]
        num=' '.join(num)
      #if (number[-1]=='1') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
        #if number[-1]=='1' and me_dict[measurement][1]=='f':
            #num=num.split(' ')
            #num[-1]='одной'
            #num = ' '.join(num)
            
        ##if number[-1]=='2' and me_dict[measurement][1]=='f':
            ##num=num.split(' ')
            ##num[-1]='две'
            ##num = ' '.join(num)
            
        #b=me_gen_single_dict[measurement]
        
        #gengengen+=1
      #else:
      b=me_gen_plural_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = num + ' '+b
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line+'"')
      ##TOUCHING GENITIVE
      
    elif (((len(line)>1 and line[-1] in me_dict and line[:-1].isdigit() ) or (len(line)>2 and line[-2] in me_prefix and line[-1] in me_dict and line[:-2].isdigit()) or (len(line)>3 and line[-3:-1] in   me_prefix and line[-1] in me_dict and line[-3:].isdigit()) or (len(line)>3 and line[-3] in me_prefix and line[-2:] in me_dict and line[:-3].isdigit()) ) and (any((y==prev_line for y in genitive_prepositions))or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния') and prev_line!='с' and prev_line!='С'):
      
      if line[-1] in me_dict and not line[-2] in me_prefix:
        case=0
        number=(line[:-1])
        measurement = line[-1]
        prefix=0
      elif line[-2] in me_prefix and line[-1] in me_dict:
          case=1
          prefix = line[-2]
          prefix = me_prefix[prefix]
          measurement = line[-1]
          number=(line[:-2])
      elif line[-3:-1] in me_prefix and line[-1] in me_dict:
          case=1
          prefix = line[-3:-1]
          prefix = me_prefix[prefix]
          measurement = line[-1]
          number = line[:-3]
      elif line[-3] in me_prefix and line[-2:] in me_dict:
          case = 1 
          prefix = line[-3]
          prefix = me_prefix[prefix]
          measurement = line[-2:]
          number = line[:-3]
      num = genitivise(number)
      #if (number[-1]=='1') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
        #if number[-1]=='1' and me_dict[measurement][1]=='f':
            #num=num.split(' ')
            #num[-1]='одной'
            #num = ' '.join(num)
            
        ##if number[-1]=='2' and me_dict[measurement][1]=='f':
            ##num=num.split(' ')
            ##num[-1]='две'
            ##num = ' '.join(num)
            
        #b=me_gen_single_dict[measurement]
        
        #gengengen+=1
      #else:
      b=me_gen_plural_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = num + ' '+b
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line+'"')
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and (any(x==prev_line for x in genitive_prepositions) or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния') and prev_line!='с' and prev_line!='С'):
      number = line.split(' ')[0]
      other = line.split(' ')[1]
      if other=='000':
        other = 'тысяч'
      number = genitivise(number)
      line = number + ' ' + other
      out.write('"' + line+'"')
    ##INSTRUMENTAL MEASUREMENTS
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and (any(y==prev_line for y in instrumental_prepositions) or prev_line=='с' or prev_line=='С')):
      number=str(line.split(' ')[0])
      splitted = number
      
      if line.split(' ')[1] in me_dict:
        case=0
        number=str(line.split(' ')[0])
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      
      num = num2words(number, lang='ru')
      a=[]
      ii=0
      while ii<len(num.split(' ')):
        a.append(inst_car_dict[num.split(' ')[ii]])
        ii+=1
      num = ' '.join(a)
      if len(number)==4 and number[0]=='1':
        num=num.split(' ')
        num=num[1:]
        num=' '.join(num)
      if (number[-1]=='1') and (len(number)==1 or (len(number)>1 and number[-2]!='1')):
        if number[-1]=='1' and me_dict[measurement][1]=='f':
            num=num.split(' ')
            num[-1]='одной'
            num = ' '.join(num)
        
        #if number[-1]=='2' and me_dict[measurement][1]=='f':
            #num=num.split(' ')
            #num[-1]='две'
            #num = ' '.join(num)
            #print(num)
        b=me_inst_single_dict[measurement]
        
        gengengen+=1
      else:
        b=me_inst_plural_dict[measurement]
      if prefix!=0:
        b = prefix+b
      line = num + ' '+b
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line+'"')
   ##GENITIVE MEASUREMENTS
   # elif (len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and (line.split(' ')[1] in me_dict) and (not any(x in line.split(' ')[0] for x in punc_marks)) and ((prev_line=='от') or (prev_line=='От')):
    #  print(line)
    #  number=str(line.split(' ')[0])
     # measurement = line.split(' ')[1]
     # splitted = number
      
     # if splitted[-1] == '1' and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
     #  a=num2words(int(number), lang='ru')
     #  
     #  b = me_single_dict[measurement]
     #  if (b=='секунда') or (b=='минута') or (b =='тонна'):
     #    a = a.split(' ')
     #    a[-1]='одна'
     #    a = ' '.join(a)
     #  
     # elif (splitted[-1] == '2') and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
     #  a=num2words(int(number), lang='ru')
     #  #print(a)
     #  b = me_gen_single_dict[measurement]
     #  if (b=='секунды') or (b=='минуты')or (b =='тонны'):
     #    
     #    a = a.split(' ')
     #    #print(a)
     #    a[-1]='две'
     #    a = ' '.join(a)
     # elif ((splitted[-1] == '3') or (splitted[-1] == '4')) and (len(splitted)==1 or ((len(splitted)>1) and (splitted[-2]!='1'))):
     #  a=num2words(int(number), lang='ru')
     #  b = me_gen_single_dict[measurement]
     # 
     # else:
     #  a=num2words(int(number), lang='ru')
     #  b = me_gen_plural_dict[measurement]
     # 
     # how_many_words=len(a.split(' '))
     # i=0
     # a = a.split(' ')
     # while i < how_many_words:
     #   a[i]=gen_car_dict[a[i]]
     #   i+=1
     # a = ' '.join(a)
     # line=a +' '+b
    
     # out.write('"' + line+'"')
    elif ((len(line.split(' '))==2) and (line.split(' ')[0].isdigit()) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict)) and (not any(x in line.split(' ')[0] for x in punc_marks)) and ((not any(x == prev_line for x in genitive_prepositions)) and (not any(y ==prev_line for y in prepositional_prepositions)) and (not any(z ==prev_line for z in accusative_prepositions)) and (not any(w ==prev_line for w in dative_prepositions)))):
      
      line = measurise(line)
      out.write('"' + line+'"')
    ##45-Y 83-M ETC##
    elif (len(line)==2 or len(line)==3 or len(line)==4 or len(line)==5) and (line[-2]=='-') and (line[:-2].isdigit())and not (line[-1].isdigit()) and not prev_line=='-':
      number = line[:-2].replace('-','')
      #print(number)
      a=num2words(int(number), lang='ru')
      if line[-1]=='м':
        if any(x==prev_line for x in full_prepositions):
          splitted=a.split()
          splitted[-1]=prep_ord_dict[splitted[-1]]
        else:
         splitted=a.split()
         splitted[-1]=acc_ord_dict[splitted[-1]]
         
      elif line[-1]=='й':
        splitted=a.split()
        splitted[-1]=acc_ord_dict[splitted[-1]]
      
      elif line[-1]=='ю':
        splitted=a.split()
        splitted[-1]=uyu_ord_dict[splitted[-1]]
      
      elif line[-1]=='е':
        splitted=a.split()
        splitted[-1]=uye_ord_dict[splitted[-1]]
     
      elif line[-1]=='х':
        splitted=a.split()
        splitted[-1]=ukh_ord_dict[splitted[-1]]
        
      elif line[-1]=='и':
        splitted=a.split()
        splitted[-1]=gen_car_dict[splitted[-1]]
        
      elif line[-1]=='я':
        splitted=a.split()
        splitted[-1]=aya_ord_dict[splitted[-1]]
        
      else:
        print('exception')
        #print(line)
      #print(line)
      if number=='2000':
        splitted[0]='двух'
      if len(number)==4 and number[0]=='1':
        splitted = splitted[1:]
      line=' '.join(splitted)
      out.write('"' + line + '"')
    elif (len(line)==2 or len(line)==3 or len(line)==4 or len(line)==5) and (line[-2]=='-') and (line[:-2].isdigit())and not (line[-1].isdigit()) and prev_line=='-':
      line = line
      out.write('"' + line + '"')
    elif line.isdigit() and next_line=='-' and (len(next_next_line)==2 or len(next_next_line)==3 or len(next_next_line)==4 or len(next_next_line)==5) and (next_next_line[-2]=='-') and (next_next_line[:-2].isdigit())and not (next_next_line[-1].isdigit()):
      line = line
      out.write('"' + line + '"')
    elif '-го' in line and line.replace('-го','').isdigit():
      number = line.replace('-го','')
      in_words = num2words(int(number), lang='ru')
      a = in_words.split(' ')
      a[-1] = gen_ord_dict[a[-1]]
      if len(number)==4 and number[0]=='1':
        a = a[1:]
      line = ' '.join(a)
      out.write('"' + line + '"')
    #ARE NUMBERS OF THE FORM X,YZ ??#
    elif (line.count(',')==1) and line[:line.find(',')].isdigit() and line.replace(',','').isdigit() and any(prev_line==x for x in nominative_prepositions):
     line = decimal_prepositional(line)
     out.write('"' + line + '"')
    elif (line.count(',')==1) and line.replace(',','').isdigit() and (any(prev_line==x for x in genitive_prepositions)or prev_line=='ой'or prev_line[-2:]=='ии' or prev_line[-2:]=='ше' or prev_line[-3:]=='ния'or next_line=='года'):
      line = decimal_genitive(line)
      out.write('"' + line + '"')
    elif (line.count(',')==1)  and line.replace(',','').isdigit() and any(prev_line==x for x in prepositional_prepositions):
      line = decimal_prepositional(line)
      out.write('"' + line + '"')
    elif (line.count(',')==1) and line[:line.find(',')].isdigit() and line.replace(',','').isdigit() and (any(prev_line==x for x in instrumental_prepositions) or prev_line[-2:]=='ым'):
      line = decimal_instrumental(line)
      out.write('"' + line + '"')
    elif (line.count(',')==1) and line[:line.find(',')].isdigit() and line.replace(',','').isdigit() and any(prev_line==x for x in dative_prepositions):
     line = decimal_dative(line)
     out.write('"' + line + '"')
    
    elif (line.count(',')==1) and line[:line.find(',')].isdigit() and line.replace(',','').isdigit():
      
      line = decimal(line)
     
      out.write('"' + line + '"')
      #ARE NUMBERS OF THE FORM X,YZ ??# WITH MEASUREMENTS
    elif (',' in line) and (line.count(',')==1) and line.split(' ')[0].replace(',','').isdigit() and(len(line.split(' ')) == 2) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (any(z==prev_line for z in genitive_prepositions) and prev_line!='с' and prev_line!='С'):
      
      if line.split(' ')[1] in me_dict:
        case=0
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      
      number_string = line.split(' ')[0]
      measurement=me_gen_single_dict[measurement]
      if prefix!=0:
        measurement = prefix+measurement
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = genitivise(before_decimal)
      after = genitivise(after_decimal)
      comma = 'целых и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятых'
        if after_decimal=='1':
          frac='десятой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотых'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячных'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячных'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac + ' '+measurement 
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line + '"')
    elif (',' in line) and (line.count(',')==1) and line.split(' ')[0].replace(',','').isdigit() and(len(line.split(' ')) == 2) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and ((any(z==prev_line for z in instrumental_prepositions)or prev_line=='с' or prev_line=='С')):
     
      if line.split(' ')[1] in me_dict:
        case=0
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      
      number_string = line.split(' ')[0]
      measurement=me_gen_single_dict[measurement]
      if prefix!=0:
        measurement = prefix+measurement
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = instrumentalise(before_decimal)
      after = instrumentalise(after_decimal)
      comma = 'целыми и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятыми'
        if after_decimal=='1':
          frac='десятой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотыми'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячными'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячными'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac + ' '+measurement 
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line + '"')
    elif (',' in line) and (line.count(',')==1) and line.split(' ')[0].replace(',','').isdigit() and(len(line.split(' ')) == 2) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict)) and (any(z==prev_line for z in dative_prepositions)):
      
      if line.split(' ')[1] in me_dict:
        case=0
        measurement = line.split(' ')[1]
        prefix=0
      elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][1:]
      elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
          case=1
          prefix = line.split(' ')[1][0:2]
          prefix = me_prefix[prefix]
          measurement = line.split(' ')[1][2:]
      
      number_string = line.split(' ')[0]
      measurement=me_gen_single_dict[measurement]
      if prefix!=0:
        measurement = prefix+measurement
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      before = dativise(before_decimal)
      after = dativise(after_decimal)
      comma = 'целым и'
      if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
        comma = 'целой и'
        split = before.split(' ')
        split[-1]='одной'
        before = ' '.join(split)
      if len(after_decimal)==1:
        frac = 'десятым'
        if after_decimal=='1':
          frac='десятой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==2:
        frac = 'сотым'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'сотой'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==3:
        frac = 'тысячным'
        if after_decimal[-1]=='1' and after_decimal[-2]!='1':
          frac = 'тысячной'
          split= after.split(' ')
          split[-1]='одной'
          after = ' '.join(split)
      elif len(after_decimal)==4:
         frac = 'десяти тысячным'
      line = before+ ' '+ comma + ' '+ after + ' '+ frac + ' '+measurement 
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line + '"')
    elif ((',' in line) and (line.count(',')==1) and line.split(' ')[0].replace(',','').isdigit() and(len(line.split(' ')) == 2) and ((line.split(' ')[1] in me_dict) or (line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict) or (line.split(' ')[1][0:2] in   me_prefix and line.split(' ')[1][2:] in me_dict))):
      number_string = line.split(' ')[0]
      before_decimal=number_string[:number_string.find(',')]
      after_decimal = number_string[number_string.find(',')+1:]
      if int(after_decimal)==0:
        line = before_decimal+' ' + line.split(' ')[1]
        line = measurise(line)
      else:
        if line.split(' ')[1] in me_dict:
          case=0
          measurement = line.split(' ')[1]
          prefix=0
        elif line.split(' ')[1][0] in me_prefix and line.split(' ')[1][1:] in me_dict:
            case=1
            prefix = line.split(' ')[1][0]
            prefix = me_prefix[prefix]
            measurement = line.split(' ')[1][1:]
        elif line.split(' ')[1][0:2] in me_prefix and line.split(' ')[1][2:] in me_dict:
            case=1
            prefix = line.split(' ')[1][0:2]
            prefix = me_prefix[prefix]
            measurement = line.split(' ')[1][2:]
      
        number_string = line.split(' ')[0]
        before_decimal=number_string[:number_string.find(',')]
        after_decimal = number_string[number_string.find(',')+1:]
      
        measurement=me_gen_single_dict[measurement]
      
        if prefix!=0:
          measurement = prefix+measurement
      
        before = num2words(int(before_decimal),lang='ru')
        after = num2words(int(after_decimal),lang='ru')
        comma = 'целых и'
        if before_decimal[-1]=='1' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
          comma = 'целая и'
          split = before.split(' ')
          split[-1]='одна'
          before = ' '.join(split)
        elif before_decimal[-1]=='2' and (len(before_decimal)==1 or before_decimal[-2]!='1'):
          split= before.split(' ')
          split[-1]='две'
          before = ' '.join(split)
        if len(after_decimal)==1:
          frac = 'десятых'
          if after_decimal=='1':
            frac='десятая'
            split= after.split(' ')
            split[-1]='одна'
            after = ' '.join(split)
          elif after_decimal=='2':
            split= after.split(' ')
            split[-1]='две'
            after = ' '.join(split)
        elif len(after_decimal)==2:
          frac = 'сотых'
          if after_decimal[-1]=='1' and after_decimal[-2]!='1':
            frac = 'сотая'
            split= after.split(' ')
            split[-1]='одна'
            after = ' '.join(split)
          elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
            split= after.split(' ')
            split[-1]='две'
            after = ' '.join(split)
        elif len(after_decimal)==3:
          frac = 'тысячных'
          if after_decimal[-1]=='1' and after_decimal[-2]!='1':
            frac = 'тысячная'
            split= after.split(' ')
            split[-1]='одна'
            after = ' '.join(split)
          elif after_decimal[-1]=='2' and after_decimal[-2]!='1':
          
            split= after.split(' ')
            split[-1]='две'
            after = ' '.join(split)
        elif len(after_decimal)==4:
           frac = 'десятая тысячная'
        line = before+ ' '+ comma + ' '+ after + ' '+ frac + ' '+measurement 
      if 'целых и ноль' in line:
          line=line.replace('целых и ноль ','')
      out.write('"' + line + '"')
    
    elif (((line.count('-')==2 or line.count('—')==2 or (line.count('—')==1 and line.count('-')==1)) and (line[0]=='-'or line[0]=='—') and (line[-2]=='-'or line[-2]=='—') and (len(line)==4 or len(line)==5 or len(line)==6 or len(line)==7)) or (line.count('-')==1 and line[0]!='-')and (len(line)==3 or len(line)==4 or len(line)==5 or len(line)==6)) and (line[-2]=='-') and ((line[:-2].isdigit()) or (line[1:-2].isdigit())) and not (line[-1].isdigit()) and not (line[-1]=='м') :
      
      if line.count('-')==2 or line.count('—')==2 or (line.count('—')==1 and line.count('-')==1):
        
        line = line[1:]
      number = line[:-2]
      #print(number)
      a=num2words(int(number), lang='ru')
      #if line[-1]=='м':
        #splitted=a.split()
        #splitted[-1]=prep_ord_dict[splitted[-1]]
        #if len(number)==4 and number[0]=='1':
          #splitted = splitted[1:]
        #line=' '.join(splitted) 
      if line[-1]=='й':
        splitted=a.split()
        splitted[-1]=acc_ord_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      elif line[-1]=='ю':
        splitted=a.split()
        splitted[-1]=uyu_ord_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      elif line[-1]=='е':
        splitted=a.split()
        splitted[-1]=uye_ord_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      elif line[-1]=='х':
        splitted=a.split()
        splitted[-1]=ukh_ord_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      elif line[-1]=='и':
        splitted=a.split()
        splitted[-1]=gen_car_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      elif line[-1]=='я':
        splitted=a.split()
        splitted[-1]=aya_ord_dict[splitted[-1]]
        if len(number)==4 and number[0]=='1':
          splitted = splitted[1:]
        line=' '.join(splitted)
      else:
        print('exception')
       
      #if len(number)==4:
        #print(number)
        #print(line)
      #print(line)
      out.write('"' + line + '"')
    ##LATIN
    elif any(x ==line for x in latin_letters) and len(line)==1:
      a = line
      if a in latin_dict:
        a = latin_dict[a]
      b = '_latin'
      line = a+b
      out.write('"' + line + '"')
    ##HYPHENS
    elif any(x==line for x in measurement_marks) or line ==' - ' or line ==' -' or line=='- ' or line ==' — ' or line ==' —' or line=='— ' :
      
      if (any(x in prev_line for x in numbers) and any(y in next_line for y in numbers)) and ((len(prev_line.split(' '))<3) and ((len(prev_line.split(' '))==1 and prev_line[0].isdigit()) or (len(prev_line.split(' '))==2 and prev_line.split(' ')[0].isdigit())) and (next_line[0].isdigit() or next_line.split(' ')[0].replace('.','').replace(',','').isdigit())   and (prev_prev_line=='от')):
        
        line = 'до'
        do +=1
      else:
        line = line
      
      out.write('"' + line + '"')
    elif line.isdigit() and prev_line==':' and next_line==':':
      line = line
      out.write('"' + line + '"')
    elif line.isdigit() and len(line)!=4 and prev_prev_line.isdigit() and len(prev_prev_line)!=4 and any(x==prev_line for x in measurement_marks) and any(y==prev_prev_prev_line for y in genitive_prepositions):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      if prev_line =='—':
        if any(x==next_line for x in gen_months): 
          line = genitivise(line)
        elif next_line[-2:]=='ми':
          line = instrumentalise(line)
        else:
          line = accusativise(line)
      elif prev_line =='-':
        line = genitivise(line)
      out.write('"' + line + '"')
    ##minus numbers
    elif (line.count('-')==1 and line[0]=='-')and line.replace('-','').isdigit()  and (len(line.split(' '))==1 or len(line.split(' '))==2) and len(line.replace('-',''))>0:
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      minus = 'минус'
      number = line.replace('-','')
      
      if next_line.count('-')==1 and next_line[0]=='-' and next_line.replace('-','').isdigit():
        a=[]
        zzz=0
        while zzz<len(number):
          a.append(num2words((number[zzz]), lang='ru'))
          zzz+=1
        line = minus + ' ' + ' '.join(a)
      elif prev_line.count('-')==1 and prev_line[0]=='-' and prev_line.replace('-','').isdigit():
        a=[]
        zzz=0
        while zzz<len(number):
          a.append(num2words((number[zzz]), lang='ru'))
          zzz+=1
        line = minus + ' ' +' '.join(a)
      elif prev_line.isdigit():
        if any(x==prev_prev_line for x in prepositional_prepositions):
          line=prepi(number)
        elif any(x==prev_prev_line for x in genitive_prepositions):
          line=geni(number)
        elif any(x==prev_prev_line for x in dative_prepositions):
          line=dati(number)
        elif any(x==prev_prev_line for x in instrumental_prepositions):
          line=insti(number)
        elif any(x==prev_prev_line for x in accusative_prepositions):
          line=acci(number)
        elif any(x==prev_prev_line for x in nominative_prepositions):
          line=acci(number)
        elif next_line=='годов':
          line = geni(number)
        else:
          line = num2words(number,lang='ru')
          line = minus + ' ' +line
      else:
        in_words = num2words((number), lang='ru')
        if len(number)==4 and number[0]=='1':
          a=in_words.split(' ')
          a=a[1:]
          in_words = ' '.join(a)
        line = minus + ' ' + in_words
      out.write('"' + line + '"')
    elif (line.count('—')==1 and line[0]=='—')and line.replace('—','').isdigit()  and (len(line.split(' '))==1 or len(line.split(' '))==2) and len(line.replace('—',''))>0:
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      minus = 'минус'
      number = line.replace('—','')
      
      if next_line.count('—')==1 and next_line[0]=='—' and next_line.replace('—','').isdigit():
        a=[]
        zzz=0
        while zzz<len(number):
          a.append(num2words((number[zzz]), lang='ru'))
          zzz+=1
        line = minus + ' ' + ' '.join(a)
      elif prev_line.count('—')==1 and prev_line[0]=='—' and prev_line.replace('—','').isdigit():
        a=[]
        zzz=0
        while zzz<len(number):
          a.append(num2words((number[zzz]), lang='ru'))
          zzz+=1
        line = minus + ' ' +' '.join(a)
      elif prev_line.isdigit():
        if any(x==prev_prev_line for x in prepositional_prepositions):
          line=prepi(number)
        elif any(x==prev_prev_line for x in genitive_prepositions):
          line=geni(number)
        elif any(x==prev_prev_line for x in dative_prepositions):
          line=dati(number)
        elif any(x==prev_prev_line for x in instrumental_prepositions):
          line=insti(number)
        elif any(x==prev_prev_line for x in accusative_prepositions):
          line=acci(number)
        elif any(x==prev_prev_line for x in nominative_prepositions):
          line=acci(number)
        elif next_line=='годов':
          line = geni(number)
        else:
          line = accusativise(number)
          line = minus + ' ' +line
      else:
        in_words = num2words((number), lang='ru')
        if len(number)==4 and number[0]=='1':
          a=in_words.split(' ')
          a=a[1:]
          in_words = ' '.join(a)
        line = minus + ' ' + in_words
      out.write('"' + line + '"')
    elif (((line.count('-')==1 and line[0]=='-' and line.replace('-','').isdigit() and len(line.replace('-',''))>0) or ((line.count('—')==1 and line[0]=='—' and line.replace('—','').isdigit() and len(line.replace('—',''))>0) )) and (len(line.split(' '))==1 or len(line.split(' '))==2) and prev_line.isdigit() and (any(x==prev_prev_line for x in genitive_prepositions) or any(x==prev_prev_line for x in prepositional_prepositions) or any(x==prev_prev_line for x in dative_prepositions)or any(x==prev_prev_line for x in instrumental_prepositions) or any(x==prev_prev_line for x in accusative_prepositions) or any(x==prev_prev_line for x in nominative_prepositions))):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      line = line.replace('-','')
      line = line.replace('—','')
      if ((any(x == prev_prev_line for x in genitive_prepositions))and len(line)==4):
        line=geni(line)
        out.write('"' + line +'"')
      elif ((any(x == prev_prev_line for x in genitive_prepositions))):
        line=geni(line)
        out.write('"' + line +'"')
      elif ( (any(x == prev_prev_line for x in prepositional_prepositions)) and len(line)==4):
        line=prepi(line)
        out.write('"' + line +'"')  
      elif (any(x == prev_prev_line for x in prepositional_prepositions)):
        if any(y==next_line for y in years_abbreviations) or 'годах' in next_line:
          line = prepi(line)
        else:
          line=prepi(line)
     
        out.write('"' + line +'"')
      elif ( (any(x == prev_prev_line for x in dative_prepositions)) and len(line)==4):
        line=dati(line)
        out.write('"' + line +'"')
      
      elif (any(x == prev_prev_line for x in dative_prepositions)):
        line=dati(line)
      
        out.write('"' + line +'"')
      elif ((any(x == prev_prev_line for x in instrumental_prepositions)) and len(line)==4):  
        line=insti(line)
        out.write('"' + line +'"')
      elif  (any(x == prev_prev_line for x in instrumental_prepositions)):
        line=insti(line)
        out.write('"' + line +'"')
      elif ((any(x == prev_prev_line for x in accusative_prepositions)) and len(line)==4):
        line=acci(line)
        out.write('"' + line +'"')
      elif ( (any(x == prev_prev_line for x in accusative_prepositions))):
        #line=num2words(line,lang='ru')
        line=acci(line)
        out.write('"' + line +'"')
      elif ((any(x == prev_prev_line for x in nominative_prepositions)) and len(line)==4):
        line=acci(line)
        out.write('"' + line +'"')
      elif ((any(x == prev_prev_line for x in nominative_prepositions))):
        line=acci(line)
        
        out.write('"' + line +'"')
    elif (prev_line[-2:]=='ше' or prev_line[-4:]=='ание') and line.isdigit():
      line = genitivise(line)
      out.write('"' + line +'"')
    #elif line.count('-')==1 and line.replace('-','').isdigit() and len(line)==9 and line[4]=='-':
      #first = line[:4]
      #second = line[5:]
      #first = accusativise(first)
      #second = accusativise(second)
      #sil = 'sil'
      #line = first + ' ' + sil + ' ' + second
      #out.write('"' + line +'"')
    ##FRACTIONS##
    elif line.count('/')==1 and line.replace('/','').isdigit() and len(line.split(' '))==1 and (any(x==prev_line for x in genitive_prepositions)or any(x==prev_line for x in dative_prepositions)or any(x==prev_line for x in instrumental_prepositions)or any(x==prev_line for x in accusative_prepositions)or any(x==prev_line for x in prepositional_prepositions)or any(x==prev_line for x in nominative_prepositions)or any(x==prev_line for x in clause_marks)):
      
     
      place = line.find('/')
      before = line[:place]
      after = line[place+1:]
      before_inwords=num2words(int(before),lang='ru')
      after_inwords=num2words(int(after),lang='ru')
      
      if any(x==prev_line for x in prepositional_prepositions):
        before_inwords=prepositionalise(before)
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одной'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = oy_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
        after_inwords=' '.join(b)
      elif any(x==prev_line for x in genitive_prepositions) or (next_line[-1]=='ы' and len(next_line)>3) or (prev_line=='сезона') or prev_line=='примерно':
        before_inwords=genitivise(before)
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одной'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = oy_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
        after_inwords=' '.join(b)
      elif any(x==prev_line for x in instrumental_prepositions):
        before_inwords=instrumentalise(before)
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одной'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = oy_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
        after_inwords=' '.join(b)
      elif any(x==prev_line for x in dative_prepositions):
        before_inwords=dativise(before)
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одной'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = oy_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = dat_ord_dict[b[-1]]
        after_inwords=' '.join(b)
      elif any(x==prev_line for x in accusative_prepositions) or any(x==prev_line for x in nominative_prepositions):
        before_inwords=accusativise(before)
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одну'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = uyu_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
      elif any(x==prev_line for x in clause_marks):
        before_inwords=num2words(before,lang='ru')
        if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'одна'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = aya_ord_dict[b[-1]]
        elif before[-1]=='2' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'две'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
        else:
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
        after_inwords=' '.join(b)
        
      if len(before)==4 and before[0]=='1':
        a=before_inwords.split(' ')
        a=a[1:]
        before_inwords =' '.join(a)
      if len(after)==4 and after[0]=='1':
        b=after_inwords.split(' ')
        b=b[1:]
        after_inwords=' '.join(b)
      line = before_inwords + ' ' + after_inwords
      
      out.write('"' +line +'"')
    elif line.count('/')==1 and line.replace('/','').isdigit() and len(line.split(' '))==1:
      
     
      place = line.find('/')
      before = line[:place]
      after = line[place+1:]
      before_inwords=num2words(int(before),lang='ru')
      after_inwords=num2words(int(after),lang='ru')
      
     
        
      if before[-1]=='1' and (len(before)==1 or before[-2]!='1'):
        a=before_inwords.split(' ')
        a[-1] = 'одну'
        before_inwords=' '.join(a)
        b = after_inwords.split(' ')
        b[-1] = uyu_ord_dict[b[-1]]
      elif before[-1]=='2' and (len(before)==1 or before[-2]!='1'):
          a=before_inwords.split(' ')
          a[-1] = 'две'
          before_inwords=' '.join(a)
          b = after_inwords.split(' ')
          b[-1] = ukh_ord_dict[b[-1]]
      else:
        b = after_inwords.split(' ')
        b[-1] = ukh_ord_dict[b[-1]]
      after_inwords=' '.join(b)
      if len(before)==4 and before[0]=='1':
        a=before_inwords.split(' ')
        a=a[1:]
        before_inwords =' '.join(a)
      if len(after)==4 and after[0]=='1':
        b=after_inwords.split(' ')
        b=b[1:]
        after_inwords=' '.join(b)
      line = before_inwords + ' ' + after_inwords
      
      out.write('"' +line +'"')
    #elif line.isupper() and (not any(x==line for x in roman_numerals)) and not ('1' in line or '2' in line or '3' in line or '4' in line or '5' in line or '6' in line or '7' in line or '8' in line or '9' in line or '0' in line) and line in res and len(line)>1: #and '.' in line:
     # if len(sorted(res[line].items(), key=operator.itemgetter(1), reverse=True))>1: 
     #   
      #srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
      #line=srtd[0][0]
      #out.write('"' + line + '"')
      #print(line)
     
        
      #line=line.replace(' ','')
      #line=line.replace('.','')
      #a = []
      #i=0
      #while i <len(line):
      #  a.append(combined_dict[line[i]])#
      #  i+=1
      #line=' '.join(a)
      #print(line)
      #out.write('"' + line + '"')
    ##TRANSLITERATION##
    #elif any(x in line.split(' ') for x in latin) and (not any(y in line for y in punc_marks)) and (not any(z==line for z in roman_numerals)) and (any(w in line for w in lowercase_letters)):
    #  print(line)
    #  t = transify(line)
    #  print(t)
                  #val[i]=translit(val[i], 'ru')
                  #val[i] = transify(val[i])
    #elif ((len(line.split(' '))==3 ) and (line.split(' ')[2].isdigit()) and (line.split(' ')[0].isdigit()) and (any(x ==line.split(' ')[1] for x in prep_months))and(any(y == prev_line for y in #prepositional_prepositions))):
    #  number = line.split(' ')[0]
    ##  a=num2words(int(number), lang='ru')
    #  a = a.split(' ')
    #  a[-1]=prep_ord_dict[a[-1]]
    #  a=' '.join(a)
    #  a = genitivise(line.split(' ')[0])
    #  c = prepositionalise(line.split(' ')[2])
    #  b = line.split(' ')[1]
    # 
    #  d = 'году '
    #      #  #print('defo prep')
    #  out.write('"' + a +' '+ b +' '+ c + ' ' +'"')
     
    
    elif ((len(line.split(' '))==1) and line.count(':')==1  and (remove_colon.isdigit()) and ((len(remove_colon)==3) or (len(remove_colon)==4))):
      
      
        #a = line#.split(' ')
        
      remove_colon = line.replace(':', '')
      if (remove_colon.isdigit()) and ((len(remove_colon)==3) or (len(remove_colon)==4)):
      
        minutes = remove_colon[-2:]
        hours = remove_colon[:-2]
        hours_in_words=num2words(int(hours), lang='ru')
        minutes_in_words=num2words(int(minutes), lang='ru')
      #if (len(hours)==2) and (hours[0]=='0') and (hours[1]=='1'):
      # hours_in_words=hours_in_words.split(' ')
      # hours_in_words = hours_in_words[1:]
      # hours_in_words= ''.join(hours_in_words)
      if hours[-1]=='1' and (len(hours)==1 or hours[-2]!='1'):
        c = 'час'
        
        
      elif ((hours[-1]=='2') or (hours[-1]=='3') or (hours[-1]=='4')) and ((len(hours)==1) or (hours[-2]!='1')):
        c = 'часа'
     
      else:
        c = 'часов'
      if minutes[-1]=='1' and minutes[-2]!='1':
        minutes_in_words = minutes_in_words.split(' ')
        minutes_in_words[-1]='одна'
        minutes_in_words = ' '.join(minutes_in_words)
        e = 'минута'
      elif minutes[-1]=='2' and minutes[-2]!='1':
        minutes_in_words = minutes_in_words.split(' ')
        minutes_in_words[-1]='две'
        minutes_in_words = ' '.join(minutes_in_words)
        e = 'минуты'
      elif ((minutes[-1]=='3') or (minutes[-1]=='4')) and minutes[-2]!='1':
        e = 'минуты'
      elif (minutes =='00'):
        minutes_in_words=''
        e = ''
      else:
        e = 'минут'
        
      time = hours_in_words + ' '+c +' '+ minutes_in_words + ' '+ e
      if e=='':
        time=hours_in_words + ' '+c +' '+ minutes_in_words
      if (len(hours)==2) and (hours[0]=='0') and (hours[1]=='1'):
        time=c +' '+ minutes_in_words + ' '+ e
        if e=='':
          time=c +' '+ minutes_in_words
      elif hours[-1]=='1' and (len(hours)==1 or hours[-2]!='1'):
        time=c +' '+ minutes_in_words + ' '+ e
        if e=='':
          time=c +' '+ minutes_in_words  
      out.write('"' + time+'"')#
      
    elif ((len(line.split(' '))==2) and any(x==line.split(' ')[1] for x in time_zones) and line.count(':')==1  and line.split(' ')[0].replace(':','').isdigit() and ((len(line.split(' ')[0].replace(':',''))==3) or (len(line.split(' ')[0].replace(':',''))==4))):
      zone = line.split(' ')[1]
      lin = line.split(' ')[0].replace(':','')
      if zone == 'UTC':
        zone ='по часовому поясу u t c'
      elif zone=='по часовому поясу GMT':
        zone = 'g m t'
      elif zone =='PM' or zone=='p.m.':
        if (line[0]=='1') or (line[0]=='2') or (line[0]=='3') or (line[0]=='4'): 
          zone = 'дня'
        else:
          zone = 'вечера'
      elif zone =='AM' or zone=='a.m.':
        if (line[0]=='1' and len(lin)==3) or (line[0]=='2') or (line[0]=='3') or (line[0]=='4'): 
          zone = 'ночи'
        else:
          zone = 'утра'
      
        #a = line#.split(' ')
        
      remove_colon = line.split(' ')[0].replace(':', '')
      if (remove_colon.isdigit()) and ((len(remove_colon)==3) or (len(remove_colon)==4)):
      
        minutes = remove_colon[-2:]
        hours = remove_colon[:-2]
        hours_in_words=num2words(int(hours), lang='ru')
        minutes_in_words=num2words(int(minutes), lang='ru')
      #if (len(hours)==2) and (hours[0]=='0') and (hours[1]=='1'):
      # hours_in_words=hours_in_words.split(' ')
      # hours_in_words = hours_in_words[1:]
      # hours_in_words= ''.join(hours_in_words)
      if hours[-1]=='1' and (len(hours)==1 or hours[-2]!='1'):
        c = 'час'
      elif ((hours[-1]=='2') or (hours[-1]=='3') or (hours[-1]=='4')) and ((len(hours)==1) or (hours[-2]!='1')):
        c = 'часа'
     
      else:
        c = 'часов'
      if minutes[-1]=='1' and minutes[-2]!='1':
        minutes_in_words = minutes_in_words.split(' ')
        minutes_in_words[-1]='одна'
        minutes_in_words = ' '.join(minutes_in_words)
        e = 'минута'
      elif minutes[-1]=='2' and minutes[-2]!='1':
        minutes_in_words = minutes_in_words.split(' ')
        minutes_in_words[-1]='две'
        minutes_in_words = ' '.join(minutes_in_words)
        e = 'минуты'
      elif ((minutes[-1]=='3') or (minutes[-1]=='4')) and minutes[-2]!='1':
        e = 'минуты'
      elif (minutes =='00'):
        minutes_in_words=''
        e = ''
      else:
        e = 'минут'
      time = hours_in_words + ' '+c +' '+ minutes_in_words + ' '+ e
      if (len(hours)==2) and (hours[0]=='0') and (hours[1]=='1'):
        time=c +' '+ minutes_in_words + ' '+ e
      print('TIME AFTER TIME!')
      out.write('"' + time+' ' + zone+'"')#
    
    #elif line.isdigit() and prev_line=='—':
      #line=accusativise(line)
      #out.write('"' + line+'"')
    elif line.isdigit() and len(prev_line)==2 and prev_line[1]=='.' and any(x==prev_line[0] for x in combined_dict):
      line=accusativise(line)
      out.write('"' + line+'"')
    #elif ((len(line.split(' '))==1) and (any(x in line for x in time_marks)) and (any(y==prev_line for y in genitive_prepositions)) and (remove_colon.isdigit()) and ((len(remove_colon)==3) or (len(remove_colon)==4))):
    #  print('GENITIVE')
    #  print(prev_line)
    #  print(line)
    #    #a = line#.split(' ')
    #    
    #  remove_colon = line.replace(':', '')
    #  if (remove_colon.isdigit()) and ((len(remove_colon)==3) or (len(remove_colon)==4)):
    #  #print(line)
    #    minutes = remove_colon[-2:]
    #    hours = remove_colon[:-2]
    #    hours_in_words=num2words(int(hours), lang='ru')
    #    minutes_in_words=num2words(int(minutes), lang='ru')
    #  if (len(hours)==2) and (hours[0]=='0') and (hours[1]=='1'):
    #   hours_in_words=hours_in_words.split(' ')
    #   hours_in_words = hours_in_words[1:]
    #   hours_in_words= ' '.join(hours_in_words)
    #  if hours[-1]=='1' and (len(hours)==1 or (hours[-2]!='1')):
    #    c = 'час'
    #  elif ((hours[-1]=='2') or (hours[-1]=='3') or (hours[-1]=='4')) and ((len(hours)==1) or (hours[-2]!='1')):
    #    c = 'часа'
    # 
    #  else:
    #    c = 'часов'
    #  if minutes[-1]=='1' and minutes[-2]!='1':
    #    minutes_in_words = minutes_in_words.split(' ')
    #    minutes_in_words[-1]='одна'
    #    minutes_in_words = ' '.join(minutes_in_words)
    #    e = 'минута'
    #  elif minutes[-1]=='2' and minutes[-2]!='1':
    #    minutes_in_words = minutes_in_words.split(' ')
    #    minutes_in_words[-1]='две'
    #    minutes_in_words = ' '.join(minutes_in_words)
    #    e = 'минуты'
    #  elif ((minutes[-1]=='3') or (minutes[-1]=='4')) and minutes[-2]!='1':
    #    e = 'минуты'
    #  elif (minutes =='00'):
    #    minutes_in_words=''
    #    e = ''
    #  else:
    #    e = 'минут'
    #  
    #  if len(hours_in_words)>1:
    #    how_many_hours = hours_in_words.split(' ')
    #    i=0
    #    while i <len(how_many_hours):
    #      how_many_hours[i]=gen_car_dict[how_many_hours[i]]
    #      i+=1
    #    hours_in_words = ' '.join(how_many_hours)
    #  elif (len(minutes_in_words))>1:
    #    how_many_minutes = minutes_in_words.split(' ')
    #    j=0
    #    while j <len(how_many_minutes):
    #      how_many_minutes[j]=gen_car_dict[how_many_minutes[j]]
    #      j+=1
    #    
    #    minutes_in_words= ' '.join(how_many_minutes)
    #  time = hours_in_words + ' '+c +' '+ minutes_in_words + ' '+ e
    #  print(time)
    #  out.write('"' + hours_in_words + ' '+c +' '+ minutes_in_words + ' '+ e+'"')
    
    #elif any(x ==line for x in roman_numerals) and (any(y == prev_line for y in genitive_prepositions)):
    #  line=gen_ord_dict[line]
      #print(line)
    #  out.write('"' + line + '"')
    #elif prev_line=='('  and (line.isdigit()):
    #  number=int(line)
    #  line = num2words(number, lang='ru')
    #  a=line.split()
    #  length_of_number = len(str(number))
    #  if str(number)[0]=='1' and length_of_number==4:
    #    a = a[1:]
    #  line=' '.join(a)
    #  print(number)
    #  print(line)
    #  out.write('"' + line + '"')
    ##SUPERSCRIPTS
    elif any(y ==line for y in powers_dict):
      line = powers_dict[line]
      out.write('"' + line + '"')
    elif prev_line=='-' and line.isdigit() and len(next_line)>2 and (next_line[-2:]=='ых' or next_line[-2:]=='ый'):
      line = genitivise(line)
      out.write('"' + line + '"')
    
      
    elif (next_line+'BREAK'+prev_line + 'BREAK' + prev_prev_line  in cardinal5_dict) and line.isdigit() and len(line)<8:
      
      srtd = sorted(cardinal3_dict[next_line+'BREAK'+prev_line + 'BREAK' + prev_prev_line].items(), key=operator.itemgetter(1), reverse=True)
      srtd = srtd[0][0]
      length = len(srtd)
      srtd = srtd.split(' ')
     
      if srtd[-1] in gen_ord_dict.values():
        line = geni(line)
        #print('cheat: cardinal genord')
      elif srtd[-1] in dat_ord_dict.values():
        line = dati(line)
        #print('cheat: cardinal datord')
      elif srtd[-1] in inst_ord_dict.values():
        line = insti(line)
        #print('cheat: cardinal instord')
      elif srtd[-1] in prep_ord_dict.values():
        line = prepi(line)
        #print('cheat: cardinal prepord')
      elif srtd[-1] in acc_ord_dict.values():
        line = acci(line)
        #print('cheat: cardinal accord')
      elif srtd[-1] in inst_car_dict.values():
        line = instrumentalise(line)
        #print('cheat: cardinal instcar')
      elif 'стам' in srtd[-1] or  'одному' in srtd[-1] or 'двум' in srtd[-1] or 'рем' in srtd[-1] or 'нулю' in srtd[-1]:
        line = dativise(line)
        #print('cheat: cardinal datcar')
      elif 'тысячью' in srtd[-1] or 'ах' in srtd[-1] or 'ом' in srtd[-1]:
        line = prepositionalise(line)
        #print('cheat: cardinal prepcar')
      elif srtd[-1] in gen_car_dict.values():
        line=  genitivise(line)
        #print('cheat: cardinal gencar')
      else:
        line = accusativise(line)
        #print('cheat: cardinal acccar')
      out.write('"' + line +'"')
    elif '.com' in line and line.count('.')==1:
      line=website(line)
      out.write('"' + line + '"')
    elif any(f==prev_line for f in full_clause_marks) and line.isdigit():
      line = line.translate(SUB)
      line = line.translate(SUP)
      line = line.translate(OTH)
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
      next_line = next_line[1:-1]
      number = line
      in_words = num2words(number, lang='ru')
      
      if number[-1]=='1' and (len(number)==1 or number[-2]!='1'):
        if (any(x==next_line[-1] for x in feminine_endings) or any(x==next_line[-2:] for x in feminine_endings)):
         a = in_words.split(' ')
         a[-1]='одна'
         fem+=1
        elif any(x==next_line[-2:] for x in neutral_endings):
         a = in_words.split(' ')
         a[-1]='одно'
        else:
         a = in_words.split(' ')
         a[-1]='один'
        in_words = ' '.join(a)
      elif number[-1]=='2' and (len(number)==1 or number[-2]!='1'):
        if next_line[-1]=='ы' or next_line[-1]=='и' or (any(x==next_line[-2:] for x in feminine_endings)) or (any(y==next_line[-1] for y in feminine_endings)):
          a = in_words.split(' ')
          a[-1]='две'
          fem+=1
        else:
          a = in_words.split(' ')
          a[-1]='два'
        in_words = ' '.join(a)
      if len(number)==4 and number[0]=='1':
        a = in_words.split(' ')
        a = a[1:]
        in_words= ' '.join(a)
      line = in_words
      out.write('"' + line + '"')
      
    elif len(line.split(' '))==2 and (line.split(' ')[1]=='тыс.' or line.split(' ')[1]=='тыс' or line.split(' ')[1]=='тысяч') and(line.split(' ')[0].replace(',','').isdigit()) :
      line = thousandise(line)
      out.write('"' + line + '"')
    elif len(line.split(' '))==2 and (line.split(' ')[1]=='млн.' or line.split(' ')[1]=='млн') and(line.split(' ')[0].replace(',','').isdigit()) :
    
      line = millionise(line)
      out.write('"' + line + '"')
    elif len(line.split(' '))==2 and line.split(' ')[0].isdigit() and line.split(' ')[1].isdigit():
      
      first = line.split(' ')[0]
      second = line.split(' ')[1]
      number = first+second
      if second == '000' and prev_line==':':
        aa=0
        a=[]
        while aa<len(number):
          a.append(num2words(int(number[aa]),lang='ru'))
          aa=aa+1
        line = ' '.join(a)
      else:
        if any(x==prev_line for x in genitive_prepositions) or prev_line[-2:]=='ше':
          line = genitivise(number)
        else:
          line = accusativise(number)
      out.write('"' + line + '"')
    elif line.isdigit() and len(line)==1 and line[-1]=='1' and (next_line[-2:]=='во' or next_line[-2:]=='ое' or next_line[-2:]=='ро'):
      print(line)
      
      line = 'одно'
      
      out.write('"' + line + '"')
    elif line.isdigit() and str(line) =='1' and next_line[-2:]=='ья':
      line = 'одна'
      out.write('"' + line + '"')
    
    #elif any(x==prev_prev_line for x in lang) and prev_line=='.' and prev_prev_prev_line=='(' and line[0] in latin_dict and line[-1] in latin_dict:
      #print('LANG')
      #prev_prev_prev_prev_line = lines[i-4]
      #if prev_prev_prev_prev_line == '':
        #break

      #pos = prev_prev_prev_prev_line.find(',')
      #i1 = prev_prev_prev_prev_line[:pos]
      #prev_prev_prev_prev_line = prev_prev_prev_prev_line[pos + 1:]

      #pos = prev_prev_prev_prev_line.find(',')
      #i2 = prev_prev_prev_prev_line[:pos]
      #prev_prev_prev_prev_line = prev_prev_prev_prev_line[pos + 1:]
      #prev_prev_prev_prev_line = prev_prev_prev_prev_line[1:-1]
      #next_line = lines[i+1]
      #if next_line == '':
        #break

      #pos = next_line.find(',')
      #i1 = next_line[:pos]
      #next_line = next_line[pos + 1:]

      #pos = next_line.find(',')
      #i2 = next_line[:pos]
      #next_line = next_line[pos + 1:]
      #next_line = next_line[1:-1]
      
      #if next_line[0] in latin_dict and next_line[-1] in latin_dict:
        #prev_prev_prev_prev_prev_line = lines[i-5]
        #if prev_prev_prev_prev_prev_line == '':
          #break

        #pos = prev_prev_prev_prev_prev_line.find(',')
        #i1 = prev_prev_prev_prev_prev_line[:pos]
        #prev_prev_prev_prev_prev_line = prev_prev_prev_prev_prev_line[pos + 1:]

        #pos = prev_prev_prev_prev_prev_line.find(',')
        #i2 = prev_prev_prev_prev_prev_line[:pos]
        #prev_prev_prev_prev_prev_line = prev_prev_prev_prev_prev_line[pos + 1:]
        #prev_prev_prev_prev_prev_line = prev_prev_prev_prev_prev_line[1:-1]
        #print(line)
        #print(next_line)
        #print(prev_prev_prev_prev_prev_line)
        #print(prev_prev_prev_prev_line)
      #else:
        #print(line)
        #print(prev_prev_prev_prev_line)
    ## JUST DIGITS MAAAN!
    #elif any(x==prev_prev_line for x in prepositional_prepositions) and not any(y==prev_line for y in measurement_marks) and line.isdigit() and len(line)!=4:
      #line = prepositionalise(line)
      #print('DOUBLE PREP')
      #out.write('"' + line + '"')
      
    #elif any(x==prev_prev_line for x in genitive_prepositions) and not any(y==prev_line for y in measurement_marks) and line.isdigit() and len(line)!=4:
      #line = genitivise(line)
      #print('DOUBLE GEN')
      #out.write('"' + line + '"')
    elif (prev_line==';' or prev_line==':') and line.isdigit():
      linr = accusativise(line)
      out.write('"' + line + '"')
    ##THIS IS A STUPID BUG:
    elif line.isdigit() and prev_prev_line.isdigit() and (prev_line=='х' or prev_line=='x'):
      line = line[-1]
      line=accusativise(line)
      out.write('"' + line + '"')
    elif line.isdigit() and (any(x==prev_line for x in new_clause)):
      next_line = lines[i+1]
      if next_line == '':
        break

      pos = next_line.find(',')
      i1 = next_line[:pos]
      next_line = next_line[pos + 1:]

      pos = next_line.find(',')
      i2 = next_line[:pos]
      next_line = next_line[pos + 1:]
    
    
      next_line = next_line[1:-1]
      number = line
      in_words = num2words(number, lang='ru')
      if len(number)==4 and str(number)[0]=='1':
        a = in_words.split(' ')
        a = a[1:]
        in_words = ' '.join(a)
      
      if number[-1]=='1' and (len(number)==1 or number[-2]!='1'):
        a = in_words.split(' ')
        
        if any(x==line[-2:] for x in neutral_endings):
          a[-1] = 'одно'
        elif any(x==line[-2:] for x in feminine_endings) or any(x==line[-1] for x in feminine_endings):
          a[-1] = 'одна'
        else:
          a[-1] = 'один'
        in_words = ' '.join(a)
      line = in_words
      out.write('"' + line +'"')
    ##ADDITIONAL
    
    elif line in res:
        srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
        #print(srtd)
        if len(sorted(res[line].items(), key=operator.itemgetter(1), reverse=True))>1 and all(x!=line for x in full_clause_marks) and line not in po:
         dub.write('"' + str(srtd) + '"')
         dub.write('"' + '\n' + '"')
         dub.write('"' + 'prev' + '"')
         dub.write('"' + '\n' + '"')
         dub.write('"' + prev_line + '"')
         dub.write('"' + '\n' + '"')
         dub.write('"' + line + '"')
         dub.write('"' + '\n' + '"')
        out.write('"' + srtd[0][0] + '"')
        changes += 1
    elif (len(remove_hyphen)>8) and line.count('-')>1  and line.replace('-','').isdigit() :
    #  
      nu=telephonise(line)
      
      out.write('"' + nu + '"')   
    elif line.count('-')==1 and line.replace(' - ','').isdigit() and len(line.replace(' - ',''))==8:
        string_of_numbers = line.replace(' ','')
        first_hyphen_place=string_of_numbers.find('-')
        second_hyphen_place=len(string_of_numbers)
        first_string = string_of_numbers[:first_hyphen_place]
        second_string = string_of_numbers[first_hyphen_place+1:]
        if len(first_string)==1:
          first_string=num2words(int(first_string), lang='ru')
        elif len(first_string)==2:
          if first_string[0]=='0':
            first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')
          else:
            first_string = num2words(int(first_string), lang='ru')
        elif len(first_string)==3:
          if first_string[0]=='0':
            if first_string[1]=='0':
              first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')
            else:
              first_string = num2words(int(first_string[0]), lang='ru') +' ' + num2words(int(first_string[1:]), lang='ru')
          elif first_string[1]=='0':
            if first_string[2]=='0':
              first_string = num2words(int(first_string), lang='ru')
            else:
              first_string = num2words(int(first_string), lang='ru')
          else:
            first_string = num2words(int(first_string), lang='ru')
        elif len(first_string)==4:
          if first_string[0]=='0':
            if first_string[1]=='0':
              if first_string[2]=='0':
                first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')+ ' '+num2words(int(first_string[3]), lang='ru')
              else:
                first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2:]), lang='ru')
            else:
              first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1:]), lang='ru')
          else:
            first_string = num2words(int(first_string[:2]), lang='ru') + ' '+num2words(int(first_string[2:]), lang='ru')
        else:
          print('long string!')
  #print(first_string)
 ##SECOND STRING##
        if len(second_string)==1:
          second_string=num2words(int(second_string), lang='ru')
        elif len(second_string)==2:
          if second_string[0]=='0':
            second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')
          else:
            second_string = num2words(int(second_string), lang='ru')
        elif len(second_string)==3:
          if second_string[0]=='0':
            if second_string[1]=='0':
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') +' ' + num2words(int(second_string[1:]), lang='ru')
          elif second_string[1]=='0':
            if second_string[2]=='0':
             second_string = num2words(int(second_string), lang='ru')
            else:
             second_string = num2words(int(second_string), lang='ru')
          else:
            second_string = num2words(int(second_string), lang='ru')
        elif len(second_string)==4:
          if second_string[0]=='0':
            if second_string[1]=='0':
              if second_string[2]=='0':
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')
              else:
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:]), lang='ru')
          else:
            second_string = num2words(int(second_string[:2]), lang='ru') + ' '+num2words(int(second_string[2:]), lang='ru')
        elif len(second_string)==5:
          if second_string[0]=='0':
            if second_string[1]=='0':
              if second_string[2]=='0':
                if second_string[3]=='0':
                  second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')
                else:
                  second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3:]), lang='ru')
              else:
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:3]), lang='ru')+ ' ' + num2words(int(second_string[3:]), lang='ru')
          else:
            if second_string[3]=='0':
              second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
        else:
           second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3:]), lang='ru')
  
  
        s = 'sil'
        line = first_string + ' ' + s + ' '  + second_string
        
        out.write('"' + line + '"')
    elif line.count('-')==1 and line.replace('-','').isdigit() and len(line.replace('-',''))==8:
        string_of_numbers=line
        first_hyphen_place=string_of_numbers.find('-')
        second_hyphen_place=len(string_of_numbers)
        first_string = string_of_numbers[:first_hyphen_place]
        second_string = string_of_numbers[first_hyphen_place+1:second_hyphen_place]
        if len(first_string)==1:
          first_string=num2words(int(first_string), lang='ru')
        elif len(first_string)==2:
          if first_string[0]=='0':
            first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')
          else:
            first_string = num2words(int(first_string), lang='ru')
        elif len(first_string)==3:
          if first_string[0]=='0':
            if first_string[1]=='0':
              first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')
            else:
              first_string = num2words(int(first_string[0]), lang='ru') +' ' + num2words(int(first_string[1:]), lang='ru')
          elif first_string[1]=='0':
            if first_string[2]=='0':
              first_string = num2words(int(first_string), lang='ru')
            else:
              first_string = num2words(int(first_string), lang='ru')
          else:
            first_string = num2words(int(first_string), lang='ru')
        elif len(first_string)==4:
          if first_string[0]=='0':
            if first_string[1]=='0':
              if first_string[2]=='0':
                first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2]), lang='ru')+ ' '+num2words(int(first_string[3]), lang='ru')
              else:
                first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1]), lang='ru')+ ' ' + num2words(int(first_string[2:]), lang='ru')
            else:
              first_string = num2words(int(first_string[0]), lang='ru') + ' '+num2words(int(first_string[1:]), lang='ru')
          else:
            first_string = num2words(int(first_string[:2]), lang='ru') + ' '+num2words(int(first_string[2:]), lang='ru')
        else:
          print('long string!')
  #print(first_string)
 ##SECOND STRING##
        if len(second_string)==1:
          second_string=num2words(int(second_string), lang='ru')
        elif len(second_string)==2:
          if second_string[0]=='0':
            second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')
          else:
            second_string = num2words(int(second_string), lang='ru')
        elif len(second_string)==3:
          if second_string[0]=='0':
            if second_string[1]=='0':
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') +' ' + num2words(int(second_string[1:]), lang='ru')
          elif second_string[1]=='0':
            if second_string[2]=='0':
             second_string = num2words(int(second_string), lang='ru')
            else:
             second_string = num2words(int(second_string), lang='ru')
          else:
            second_string = num2words(int(second_string), lang='ru')
        elif len(second_string)==4:
          if second_string[0]=='0':
            if second_string[1]=='0':
              if second_string[2]=='0':
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')
              else:
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:]), lang='ru')
          else:
            second_string = num2words(int(second_string[:2]), lang='ru') + ' '+num2words(int(second_string[2:]), lang='ru')
        elif len(second_string)==5:
          if second_string[0]=='0':
            if second_string[1]=='0':
              if second_string[2]=='0':
                if second_string[3]=='0':
                  second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4]), lang='ru')
                else:
                  second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2]), lang='ru')+ ' '+num2words(int(second_string[3:]), lang='ru')
              else:
                second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1]), lang='ru')+ ' ' + num2words(int(second_string[2:]), lang='ru')
            else:
              second_string = num2words(int(second_string[0]), lang='ru') + ' '+num2words(int(second_string[1:3]), lang='ru')+ ' ' + num2words(int(second_string[3:]), lang='ru')
          else:
            if second_string[3]=='0':
              second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3]), lang='ru')+ ' ' + num2words(int(second_string[4:]), lang='ru')
        else:
           second_string = num2words(int(second_string[:3]), lang='ru') + ' '+num2words(int(second_string[3:]), lang='ru')
  
  
        s = 'sil'
        line = first_string + ' ' + s + ' '  + second_string
        out.write('"' + line + '"')
    elif line.isupper() or ('.' in line and line.replace('.','').replace(' ','').isupper()):
      line = line.replace('.','').replace(' ','')
      a=[]
      noletters = 0
      while noletters<len(line):
        a.append(line[noletters])
        noletters+=1
      a = ' '.join(a)
      line = a
      out.write('"' + line + '"')
    elif line.count(':')==2 and line.replace(':','').isdigit():
      line = line
      out.write('"' + line + '"')
    elif line.isdigit() and next_line[-3:]=='ами':
      line = instrumentalise(line)
      out.write('"' + line + '"')
    elif line.isdigit() and next_line[-2:]=='ью':
      line = instrumentalise(line)
      out.write('"' + line + '"')
    elif line.isdigit() and next_line=='году':
      line = prepositionalise(line)
      out.write('"' + line + '"')
    elif line.isdigit() and next_line[-1]=='у':
      line = dativise(line)
      out.write('"' + line + '"')
    elif line.isdigit() and (prev_line=='и') and len(line)==4 and len(prev_prev_line)==4 and prev_prev_line.isdigit() and any(x==prev_prev_prev_line for x in full_prepositions):
      if any(x==prev_prev_prev_line for x in genitive_prepositions):
        line=genitivise(line)
      elif any(x==prev_prev_prev_line for x in instrumental_prepositions):
        line=instrumentalise(line)
      elif any(x==prev_prev_prev_line for x in dative_prepositions):
        line=dativise(line)
      elif any(x==prev_prev_prev_line for x in accusative_prepositions):
        line=accusativise(line)
      elif any(x==prev_prev_prev_line for x in prepositional_prepositions):
        line=prepositionalise(line)
      elif any(x==prev_prev_prev_line for x in nominative_prepositions):
        line=accusativise(line)
      fem+=1
      out.write('"' + line +'"')
    elif len(line.split(' '))==1 and any(x in line for x in numbers) and line.replace('1','').replace('2','').replace('3','').replace('4','').replace('5','').replace('6','').replace('7','').replace('8','').replace('9','').replace('0','').isupper():
      line = line
      out.write('"' + line +'"')
    elif (prev_line==':' or prev_line==' :' or prev_line==': ') and (next_line=='.' or next_line==' .' or next_line=='. ' or next_line==',' or next_line==' ,' or next_line==', ' or next_line=='—' or next_line=='— ' or next_line==' —'):
      line = line
      out.write('"' + line +'"')
    elif '.' in line and line.replace('.','').replace(' ','').isupper():
      op=0
      a=[]
      line = line.replace('.','').replace(' ','')
      while op<len(line):
        a.append(line[op].lower())
        op+=1
      line = ' '.join(a)
      out.write('"' + line +'"')
    elif line.isdigit() and next_line=='лет':
      if prev_line=='возрасте':
       line = genitivise(line)
      else:
       line = accusativise(line)
      out.write('"' + line +'"')
    elif any(x in line for x in latin_letters) and not any(x in line for x in russian_dict) and not any(x in line for x in numbers) and not any(x in line for x in full_clause_marks) and len(line)>2:
      line = trans(line)
      out.write('"' + line +'"')
    elif (line.count('.')==1 or (line.count('.')==2 and 'www.' in line)) and (line[-4:] in web_endings3 or line[-5:] in web_endings4 or line[-6:] in web_endings5 or line[-7:] in web_endings6):
      line = webby_1(line)
      out.write('"' + line +'"')
    elif line.count('.')==2 and line.split('.')[0].isdigit() and (line[-4:] in web_endings3 or line[-5:] in web_endings4 or line[-6:] in web_endings5 or line[-7:] in web_endings6):
      number = line.split('.')[0]
      number = accusativise(number)
      rest = line.split('.')[1:]
      rest = ' '.join(rest)
      rest = webby_1(rest)
      line = number + ' ' +rest
      out.write('"' + line +'"')
    else:
        notin.write('"' + 'prev' + '"')
        notin.write('"' + '\n' + '"')
        notin.write('"' + prev_line + '"')
        notin.write('"' + '\n' + '"')
        notin.write('"' + line + '"')
        notin.write('"' + '\n' + '"')
        if line in res:
          srtd = sorted(res[line].items(), key=operator.itemgetter(1), reverse=True)
          
          
          
        if len(line) > 1:
            val = line.split(',')
            if len(val) == 2 and val[0].isdigit and val[1].isdigit:
                line = ''.join(val)

        if line.isdigit():
            
            
              #print(srtd)
              srtd = line.translate(SUB)
              srtd = srtd.translate(SUP)
              srtd = srtd.translate(OTH)
              out.write('"' + num2words(int(srtd), lang='ru') + '"')
              changes += 1
        elif len(line.split(' ')) > 1:
            val = line.split(' ')
            for i, v in enumerate(val):
                if v.isdigit():
                    srtd = v.translate(SUB)
                    srtd = srtd.translate(SUP)
                    srtd = srtd.translate(OTH)
                    val[i] = num2words(int(srtd), lang='ru')
                elif v in sdict:
                    val[i] = sdict[v]
                elif v in latin:
                  val[i]=translit(val[i], 'ru')
                  val[i] = transify(val[i])
                   # out.write('"'+letter + '_trans' + '"'+'LOLNOIDEA'+'"')
                   
                  

            out.write('"' + ' '.join(val) + '"')
            changes += 1
        else:
            out.write('"' + line + '"')
    
    out.write('\n')
    total += 1

print('Total: {} Changed: {}'.format(total, changes))
print('gen: {} acc: {} nom: {}'.format(gen,acc,nom))
print('digit{}'.format(digit))
print('yeppers{} noppers{}'.format(yes,no))
print('notin{}'.format(notin))
print(oyeeee)
print(roman)
test.close()

out.close()